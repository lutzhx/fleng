% mandelbrot
% https://gist.github.com/andrejbauer/7919569

-initialization(main).
-uses([fmt, mem]).

main :-
    command_line(Args),
    parse_args(Args, Xmin, Xmax, Ymin, Ymax, Maxiter, Xres, Outfile),
    Yres is integer((Xres * (Ymax - Ymin)) / (Xmax - Xmin)) / 3 * 3,
    image_depth(Maxiter, D),
    make_image(Xres, Yres, Img),
    Dx is (Xmax - Xmin) / Xres,
    Dy is (Ymax - Ymin) / Yres,
    TW is Xres / 3,
    TH is Yres / 3,
    start(0, 0, TW, TH, Dx, Dy, Ymax, Xmin, Maxiter, Img, [], R),
    dump_image(R, Img, Xres, Yres, D, Outfile, Ok),
    done(Ok).

done([]).

parse_args([], Xmin, Xmax, Ymin, Ymax, Maxiter, Xres, Outfile) :-
    parse_args([3000, 'pic.ppm'], Xmin, Xmax, Ymin, Ymax, Maxiter, Xres, Outfile).
parse_args([Xres1, Outfile1], Xmin, Xmax, Ymin, Ymax, Maxiter, Xres, Outfile) :-
    Xmin = 0.27085,
    Xmax = 0.27100,
    Ymin = 0.004640,
    Ymax = 0.004810,
    Maxiter = 1000,
    Xres is Xres1 / 3 * 3,
    Outfile = Outfile1.
parse_args([Xmin1, Xmax1, Ymin1, Ymax1, Maxiter1, Xres1, Outfile1],
    Xmin, Xmax, Ymin, Ymax, Maxiter, Xres, Outfile) :-
    string_to_real(Xmin1, Xmin),
    string_to_real(Xmax1, Xmax),
    string_to_real(Ymin1, Ymin),
    string_to_real(Ymax1, Ymax),
    string_to_integer(Maxiter1, Maxiter),
    string_to_integer(Xres1, Xres2),
    Xres is Xres2 / 3 * 3,
    Outfile = Outfile1.

image_depth(Maxiter, D) :- Maxiter < 256 | D = 256.
image_depth(Maxiter, D) :- otherwise | D = Maxiter.

make_image(W, H, M) :-
    N is W * H * 6,  % 6 bytes per pixel
    mem:allocate(N, M).

dump_image([], Img, W, H, D, Outfile, Ok) :-
    open_file(Outfile, w, File),
    N is W * H * 6,
    fmt:format(File, 'P6\n~d\n~d\n~d\n', [W, H, D], Ok1),
    when(Ok1, mem:write(Img, N, File, Ok2)),
    when(Ok2, close_file(File, Ok)).

start(_, PY, _, _, _, _, _, _, _, _, L, R) :- PY >= 3 | R = L.
start(PX, PY, W, H, Dx, Dy, Ymax, Xmin, Maxiter, Img, L, R) :-
    PY < 3, PX >= 3 |
    PY2 is PY + 1,
    start(0, PY2, W, H, Dx, Dy, Ymax, Xmin, Maxiter, Img, L, R).
start(PX, PY, W, H, Dx, Dy, Ymax, Xmin, Maxiter, Img, L, R) :-
    PY < 3, PX < 3 |
    TX is PX * W, 
    TY is PY * H,
    PX2 is PX + 1,
    iterate(TX, TY, W, H, Ymax, Xmin, Dx, Dy, Maxiter, Img, L, R2),
    next(PX2, PY, W, H, Dx, Dy, Ymax, Xmin, Maxiter, Img, R2, R).

% force arguments, so spawning takes place before iterate starts:
next(!PX2, !PY, !W, !H, !Dx, !Dy, !Ymax, !Xmin, !Maxiter, !Img, R2, R) :-
    start(PX2, PY, W, H, Dx, Dy, Ymax, Xmin, Maxiter, Img, R2, R)@fwd.

% wait until thread is idle, or this computation may block spawning
% the next thread:
iterate(!PX, !PY, !W, !H, !Ymax, !Xmin, !Dx, !Dy, !Maxiter, !Img, L, R2) :-
    idle |
    foreign_call(mb_iterate([PX, PY, W, H, Ymax, Xmin, Dx, Dy, Maxiter, Img, L, R2])).
