/* mandelbrot - C part 
   taken from: https://gist.github.com/andrejbauer/7919569
*/

#include "fleng.h"
#include "fleng-util.h"
#include <string.h>

/* terrible, terrible hack */
#if !defined(__LP64__) && !defined(_LP64)
#define fixaddr(x)  ((x) & 0x7fffffff)
#else
#define fixaddr(x)  (x)
#endif

void mb_iterate_1(FL_TCB *tcb, FL_VAL args) 
{
  LIST(args, px1, t1);
  LIST(t1, py1, t2);
  LIST(t2, w1, t3);
  LIST(t3, h1, t4);
  LIST(t4, ymax1, t5);
  LIST(t5, xmin1, t6);
  LIST(t6, dx1, t7);
  LIST(t7, dy1, t8);
  LIST(t8, maxiter1, t9);
  LIST(t9, img, t10);
  LIST(t10, L, t11);
  LIST(t11, R, _);
  FLOAT(dx1, dx);
  FLOAT(dy1, dy);
  FLOAT(ymax1, ymax);
  FLOAT(xmin1, xmin);
  unsigned char *buf = (unsigned char *)fixaddr(INT(img));
  double x, y;
  double u, v;
  int px = INT(px1), py = INT(py1);
  int maxiter = INT(maxiter1);
  int w = INT(w1), h = INT(h1);
  int i,j;
  int k;
  unsigned char *locp = buf + (py * w * 3 + px) * 6;
  for (j = 0; j < h; j++) {
    y = ymax - (j + py) * dy;
    for(i = 0; i < w; i++) {
      double u = 0.0;
      double v = 0.0;
      double u2 = u * u;
      double v2 = v * v;
      x = xmin + (i + px) * dx;
      /* iterate the point */
      for (k = 1; k < maxiter && (u2 + v2 < 4.0); k++) {
            v = 2 * u * v + y;
            u = u2 - v2 + x;
            u2 = u * u;
            v2 = v * v;
      };
      /* compute  pixel color and write it to file */
      if (k >= maxiter) {
        /* interior */
        const unsigned char black[] = {0, 0, 0, 0, 0, 0};
        memcpy(locp, black, 6);
      }
      else {
        /* exterior */
        unsigned char color[6];
        color[0] = k >> 8;
        color[1] = k & 255;
        color[2] = k >> 8;
        color[3] = k & 255;
        color[4] = k >> 8;
        color[5] = k & 255;
        memcpy(locp, color, 6);
      }
      locp += 6;
    }
    locp += w * 2 * 6;
  }
  fl_assign(tcb, L, R);
}
