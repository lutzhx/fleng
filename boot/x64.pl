% FLENG compiler - x86_64 backend
%
% register usage:
%
%   A: rax
%   T: rbx (callee save)
%   C: rdi
%   G: r12 (callee save)
%   N: r13 (callee save)
%   E0-E1: r14-r15 (callee save)
%   E<n>: (n >= 2) stack, F points to E2, ...
%   FP: rbp
%   P0-P3: rsi, rdx, rcx, r8 (tcb is passed in rdi)
%   SP: rsp, on entry set to rbp
%   X0, X1: r9, r10

:- dynamic([external/1]).
:- dynamic([floats/2]).

target_architecture(x64).

float_label(N, L) :- floats(N, L), !.
float_label(N, L) :-
    g_read(flabel, L1),
    !,
    L is L1 + 1,
    g_assign(flabel, L),
    assertz(floats(N, L)).
float_label(N, 1) :-
    assertz(flabel(1)),
    assertz(floats(N, 1)).

d_setup(_) :- g_assign(flabel, 1).

d_finalize(Out) :-
    emit('  .section .data\n  .balign 8\n', Out),
    floats(N, L),
    emit('f~d: .double ~f\n', [L, N], Out),
    fail.
d_finalize(_).

emit_external(_, _).

d_global(Name, Out) :- emit('  .global ~a\n', [Name], Out).
d_section(Name, Out) :- emit('  .section ~a\n', [Name], Out).
d_comment(Str, Out) :- emit('# ~a\n', [Str], Out).
d_wreserve(N, Out) :- emit('  .space ~d*8\n', [N], Out).

d_function_entry(Out) :- emit('  pushq %rbp\n', Out).
d_function_exit(Out) :- emit('  popq %rbp\n', Out).
d_endfunction(_).

c_prepare_foreign_call(_, _).
c_finalize_foreign_call(_, _).

d_align(code, Out) :- emit('  .balign 16\n', Out).
d_align(even, Out) :- emit('  .balign 2\n', Out).
d_align(halfword, Out) :- emit('  .balign 4\n', Out).
d_align(word, Out) :- emit('  .balign 8\n', Out).
d_align(float, Out) :- emit('  .balign 8\n', Out).

d_equ(S, X, Out) :- emit('  .equ ~a, ~d\n', [S, X], Out).
d_equ(S, Index, X, Out) :- emit('  .equ ~a~d, ~d\n', [S, Index, X], Out).

d_bdata([B], Out) :- !, emit('  .byte ~w\n', [B], Out).
d_bdata([B|L], Out) :- 
    emit('  .byte ~w', [B], Out),
    d_data1(L, Out).

d_wdata([B], Out) :- !, emit('  .quad ~w\n', [B], Out).
d_wdata([B|L], Out) :- 
    emit('  .quad ~w', [B], Out),
    d_data1(L, Out).

d_fdata([N], Out) :- !, emit('  .double ~f\n', [N], Out).
d_fdata([N|L], Out) :- 
    emit('  .double ~f, ', [N], Out),
    d_data1(L, Out).

d_data1([], Out) :- emit('\n', Out).
d_data1([B|L], Out) :- 
    emit(', ~w', [B], Out),
    d_data1(L, Out).

d_label(Str, Out) :- emit('~a:\n', [Str], Out).
d_label(Prefix, Index, Out) :- emit('~a~d:\n', [Prefix, Index], Out).

dg_label(Str, Out) :- d_label(Str, Out).

a_call(Name, Out) :- emit('  call ~a\n', [Name], Out).

r_reg(a, '%rax').
r_reg(c, '%rdi').
r_reg(t, '%rbx').
r_reg(g, '%r12').
r_reg(n, '%r13').
r_reg(fp, '%rbp').
r_reg(sp, '%rsp').
r_reg(e(N), R) :- e_reg(N, R).
r_reg(p(N), R) :- p_reg(N, R).
r_reg(x(N), R) :- x_reg(N, R).

p_reg(0, '%rsi').
p_reg(1, '%rdx').
p_reg(2, '%rcx').
p_reg(3, '%r8').

x_reg(0, '%r9').
x_reg(1, '%r10').

e_reg(0, '%r14').
e_reg(1, '%r15').

r_move_r(NR, R, Out) :-
    r_reg(R, RR),
    emit('  movq ~a, ~a\n', [NR, RR], Out).

r_move(Src, e(N), Out) :- 
    N > 1, 
    !,
    N2 is N - 2,
    r_store(Src, fp, N2, Out).
r_move(e(N), Dest, Out) :- 
    N > 1, 
    !,
    N2 is N - 2,
    r_load(fp, N2, Dest, Out).
r_move(Src, Dest, Out) :-
    r_reg(Src, SR),
    r_reg(Dest, DR),
    emit('  movq ~a, ~a\n', [SR, DR], Out).

r_load(RI, I, RD, Out) :-
    r_reg(RI, RIR),
    r_reg(RD, RDR),
    emit('  movq ~d*8(~a), ~a\n', [I, RIR, RDR], Out).

a_load(A, I, RD, Out) :-
    r_reg(RD, RDR),
    emit('  movq ~a+~d*8(~a), ~a\n', [A, I, '%rip', RDR], Out).

r_store(RS, RI, I, Out) :-
    r_reg(RI, RIR),
    r_reg(RS, RSR),
    emit('  movq ~a, ~d*8(~a)\n', [RSR, I, RIR], Out).

a_store(RS, A, I, Out) :-
    r_reg(RS, RSR),
    emit('  movq ~a, ~a+~d*8(~a)\n', [RSR, A, I, '%rip'], Out).

la_load(L, RD, Out) :-
    r_reg(RD, RDR),
    emit('  leaq ~a(~a), ~a\n', [L, '%rip', RDR], Out).
la_load(L, Index, RD, Out) :-
    r_reg(RD, RDR),
    emit('  leaq ~a~d(~a), ~a\n', [L, Index, '%rip', RDR], Out).

la_loadm(L, Index, RD, Out) :-
    r_reg(RD, RDR),
    emit('  leaq ~a~d(~a), ~a\n  orq $1, ~a\n', [L, Index, '%rip', RDR, RDR], Out).

lag_load(L, RD, Out) :-
    r_reg(RD, RDR),
    emit('  movq ~a@GOTPCREL(~a), ~a\n', [L, '%rip', RDR], Out).

i_load(I, RD, Out) :-
    r_reg(RD, RDR),
    emit('  movq $~w, ~a\n', [I, RDR], Out).

f_load(F, Out) :-
    float_label(F, L),
    emit('  movq f~d(~a), ~a\n', [L, '%rip', '%rdx'], Out). % gplc treats '%' as format char

r_push(R, Out) :-
    r_reg(R, RR),
    emit('  pushq ~a\n', [RR], Out).

r_pop(R, Out) :-
    r_reg(R, RR),
    emit('  popq ~a\n', [RR], Out).

g_return(Out) :- emit('  ret\n', Out).

ra_inc(R, Out) :-
    r_reg(R, RR),
    emit('  incl (~a)\n', [RR], Out).

la_inc(L, Out) :- emit('  incl ~a(~a)\n', [L, '%rip'], Out).

sp_align(Out) :- emit('  andq $-16, %rsp\n', Out).

ra_add(RS, N, RD, Out) :-
    r_reg(RS, RSR),
    r_reg(RD, RDR),
    emit('  leaq ~d*8(~a), ~a\n', [N, RSR, RDR], Out).

r_r_op(OP, RA, RB, RD, Out) :- 
    r_reg(RA, RAR),
    r_reg(RB, RBR),
    emit('  ~a ~a, ~a\n', [OP, RBR, RAR], Out),
    r_move(RA, RD, Out).

r_and(RA, RB, RD, Out) :- r_r_op(andq, RA, RB, RD, Out).
r_or(RA, RB, RD, Out) :- r_r_op(orq, RA, RB, RD, Out).
r_xor(RA, RB, RD, Out) :- 
    % special case
    r_reg(RA, RAR),
    r_reg(RB, RBR),
    r_reg(RD, RDR),
    emit('  xorq ~a, ~a\n  leaq 1(~a), ~a\n', [RBR, RAR, RAR, RDR], Out).

i_mask(R, M, Out) :-
    r_reg(R, RR),
    emit('  andq $~w, ~a\n', [M, RR], Out).

i_or(M, R, Out) :-
    r_reg(R, RR),
    emit('  orq $~w, ~a\n', [M, RR], Out).

j_if_carry(L, Out) :- emit('  jc ~a\n', [L], Out).
j_if_carry(L, Index, Out) :- emit('  jc ~a~d\n', [L, Index], Out).

j_compare_if_equal(RS, RI, I, L, Index, Out) :- 
    r_reg(RI, RIR),
    r_reg(RS, RSR),
    emit('  cmpq ~a, ~d*8(~a)\n  je ~a~d\n', [RSR, I, RIR, L, Index], Out).

j_r_compare_if_not_equal(RS, RI, L, Index, Out) :- 
    r_reg(RI, RIR),
    r_reg(RS, RSR),
    emit('  cmpq ~a, ~a\n  jne ~a~d\n', [RSR, RIR, L, Index], Out).

j_a_compare_if_not_equal(RS, A, L, Index, Out) :- 
    r_reg(RS, RSR),
    emit('  movq ~a@GOTPCREL(~a), ~a\n  cmpq (~a), ~a\n  jne ~a~d\n', 
        [A, '%rip', '%r10', '%r10', RSR, L, Index], Out).

j_compare_if_not_equal(RS, RI, I, L, Index, Out) :- 
    r_reg(RI, RIR),
    r_reg(RS, RSR),
    emit('  cmpq ~a, ~d*8(~a)\n  jne ~a~d\n', [RSR, I, RIR, L, Index], Out).

j_if_test(R, I, L, Index, Out) :-
    r_reg(R, RR),
    emit('  testq $~d, ~a\n  je ~a~d\n', [I, RR, L, Index], Out).
j_if_test(R, I, L, Out) :-
    r_reg(R, RR),
    emit('  testq $~d, ~a\n  je ~a\n', [I, RR, L], Out).

j_if_not_test(R, I, L, Index, Out) :-
    r_reg(R, RR),
    emit('  testq $~d, ~a\n  jne ~a~d\n', [I, RR, L, Index], Out).
j_if_not_test(R, I, L,  Out) :-
    r_reg(R, RR),
    emit('  testq $~d, ~a\n  jne ~a\n', [I, RR, L], Out).

j_always(L, Out) :- emit('  jmp ~a\n', [L], Out).
j_always(P, Index, Out) :- emit('  jmp ~a~d\n', [P, Index], Out).

sp_reset(_).
