/* FLENG - *BSD specific part of the runtime system */

#include "fleng.h"
#include "fleng-util.h"
#include <sys/types.h>
#include <sys/event.h>
#include <sys/time.h>
#include <signal.h>
#include <errno.h>
#include <sys/wait.h>
#include <string.h>
#include <unistd.h>

#define MAX_EVENTS  32

#ifndef NOTE_EOF
#define POLL_HACK
#include <poll.h>
/* doesn't seem to exist on Darwin and NetBSD */
#define NOTE_EOF 0
#endif

static struct { char *name; int sig; } signals[] = {
  { "SIGHUP", 1 },
  { "SIGINT", 2 },
  { "SIGQUIT", 3 },
  { "SIGILL", 4 },
  { "SIGTRAP", 5 },
  { "SIGABRT", 6 },
  { "SIGIOT", 6 },
  { "SIGFPE", 8 },
  { "SIGKILL", 9 },
  { "SIGSEGV", 11 },
  { "SIGPIPE", 13 },
  { "SIGALRM", 14 },
  { "SIGTERM", 15 },
  { "SIGTTIN", 21 },
  { "SIGTTOU", 22 },
  { "SIGXCPU", 24 },
  { "SIGXFSZ", 25 },
  { "SIGVTALRM", 26 },
  { "SIGPROF", 27 },
  { "SIGWINCH", 28 },
  { "SIGBUS", 10 },
  { "SIGUSR1", 30 },
  { "SIGCHLD", 20 },
  { "SIGCONT", 19 },
  { "SIGIO", 23 },
  { "SIGSTOP", 17 },
  { "SIGSYS", 12 },
  { "SIGTSTP", 18 },
  { "SIGURG", 0x10 },
  { "SIGUSR2", 31 },
  { "SIGSTKSZ", 0x8800 },
  { "SIGRTMAX", 126 },
  { "SIGRTMIN", 65 },
  { NULL, 0 }
};

static volatile int event_id;

int fl_resolve_signal(char *name) 
{
    for(int i = 0; signals[ i ].name != NULL; ++i) {
        if(!strcmp(signals[ i ].name, name)) 
            return(signals[ i ].sig);
    }
    return -1;
}

void fl_init_signals(void) {}

int fl_create_event_queue(FL_TCB *tcb)
{
    return tcb->event_queue = kqueue();
}

static void drop_listener(FL_TCB *tcb, FL_VAL id)
{
    FL_VAL *prev = &tcb->listening;
    FL_VAL ll = tcb->listening;
    while(1) {
        assert(ll != fl_nil);
        if(CAR(CAR(ll)) == id) {
            *prev = CDR(ll);
            CDR(ll) = fl_nil;
            unref(tcb, ll);
            return;
        } 
        prev = &CDR(ll);
        ll = CDR(ll);
    }
}

static void assign_tail(FL_TCB *tcb, FL_VAL lrec, FL_VAL val) 
{
    FL_VAL var = CDR(CDR(lrec));
    FL_VAL t = addref(fl_chase_tail(var));
    FL_VAL newt = mkvar(tcb);
    fl_assign(tcb, mklist(tcb, val, newt), t);
    unref(tcb, var);
    CDR(CDR(lrec)) = t;
}

static int exit_status(int status) 
{
    if(WIFSIGNALED(status)) return -WTERMSIG(status);
    if(WIFSTOPPED(status)) return -WSTOPSIG(status);
    return WEXITSTATUS(status);
}

static int update_events(FL_TCB *tcb, struct kevent *ev, int nchanges, long ms)
{
    struct kevent rev[ MAX_EVENTS ];
    struct timespec tmo;
    tmo.tv_sec = ms / 1000;
    tmo.tv_nsec = (ms % 1000) * 1000000;
    int r = kevent(tcb->event_queue, ev, nchanges, rev, MAX_EVENTS, 
        ms >= 0 ? &tmo : NULL);
    if(r == -1) return -1;
    for(int i = 0; i < r; ++i) {
        FL_VAL lrec = (FL_VAL)rev[ i ].udata;
        FL_VAL id = CAR(lrec);
        if((rev[ i ].flags & EV_ERROR) != 0) {
            if(rev[ i ].data == ESRCH) {
                // child already exited
                int status;
                FL_VAL var = CDR(CDR(lrec));
                int w = waitpid(INT(id), &status, WNOHANG);
                if(w != -1) w = exit_status(status);
                else w = 99;
                fl_assign(tcb, MKINT(w), var);
            } else {
                errno = rev[ i ].data;
                fl_rt_error(tcb, id, FL_IO_ERROR);
            }
            drop_listener(tcb, id);
            continue;
        }
        FL_VAL var = CDR(CDR(lrec));
        switch(rev[ i ].filter) {
        case EVFILT_READ: 
            fl_assign(tcb, fl_nil, var);
            drop_listener(tcb, id);
            break;
        case EVFILT_SIGNAL:
            assign_tail(tcb, lrec, MKINT(rev[ i ].data));
            break;
        case EVFILT_TIMER: {
            if(INT(CAR(CDR(lrec))) < 0) {   /* periodic? */
                assign_tail(tcb, lrec, MKINT(rev[ i ].data));
            } else {
                /* allow var being already assigned: */
                if(CAR(var) == var) fl_assign(tcb, fl_nil, var);
                drop_listener(tcb, id);
            }
            break; }
        case EVFILT_PROC: 
            fl_assign(tcb, MKINT(exit_status(rev[ i ].data)), var);
            drop_listener(tcb, id);
            break;
        }
    }
    return r;
}

int fl_wait_for_events(FL_TCB *tcb, long ms)
{
    return update_events(tcb, NULL, 0, ms);
}

int fl_add_event_to_queue(FL_TCB *tcb, int event, FL_VAL data, FL_VAL var, FL_VAL eid)
{
#ifdef POLL_HACK
    if(event == FL_INPUT) {
        /* On systems without NOTE_EOF for input-events, EOF will not be
           signalled by invocations of kevent(2), so we poll first to make
           sure we don't wait forever when at the end of file. */
        struct pollfd pfd;
        pfd.fd = INT(data);
        pfd.events = POLLIN;
        int r = poll(&pfd, 1, 0);
        if(r == -1 && errno != EINTR)
            fl_rt_error(tcb, data, FL_IO_ERROR);
        else if(r == 1) {
            if((pfd.revents & (POLLIN | POLLHUP)) != 0) {
                fl_assign(tcb, fl_nil, var);
                return 1;
            }
        }
    }
#endif
    /* listener record: [id, data|var] */
    struct kevent ev;
    int id = __sync_fetch_and_add(&event_id, 1);
    FL_VAL lrec = mklist(tcb, MKINT(id), mklist(tcb, data, var));
    FL_VAL n = mklist(tcb, lrec, tcb->listening);
    unref(tcb, tcb->listening);
    tcb->listening = addref(n);
    switch(event) {
    case FL_INPUT: /* data: fd */
        EV_SET(&ev, INT(data), EVFILT_READ, EV_ADD | EV_ONESHOT, NOTE_EOF, 0, lrec);
        break;
    case FL_SIGNAL: { /* data: signal (string) */
        int s = INT(data);
        signal(s, SIG_IGN);
        EV_SET(&ev, s, EVFILT_SIGNAL, EV_ADD, 0, 0, lrec);
        break; }
    case FL_TIMEOUT:  /* data: milliseconds */
        EV_SET(&ev, id, EVFILT_TIMER, EV_ADD | EV_ONESHOT, 0, INT(data), lrec);
        break;
    case FL_CLOCK:
        CAR(CDR(lrec)) = MKINT(-INT(data));
        EV_SET(&ev, id, EVFILT_TIMER, EV_ADD, 0, INT(data), lrec);
        break;
    case FL_CHILD:  /* data: pid */
        EV_SET(&ev, INT(data), EVFILT_PROC, EV_ADD | EV_ONESHOT, NOTE_EXIT, 0, lrec);
        break;
    }
    if(eid != NULL) fl_assign(tcb, MKINT(id), eid);
    return update_events(tcb, &ev, 1, 0);
}

static FL_VAL find_listener(FL_TCB *tcb, FL_VAL id, int remove)
{
    FL_VAL *prev = &tcb->listening;
    FL_VAL ll = tcb->listening;
    while(ll != fl_nil) {
        FL_VAL lrec = CAR(ll);
        if(CAR(lrec) == id) {
            if(remove) {
                addref(lrec);
                *prev = CDR(ll);
                CDR(ll) = fl_nil;
                unref(tcb, ll);
            }
            return lrec;
        }
        prev = &CDR(ll);
        ll = CDR(ll);
    }
    return fl_nil;
}

int fl_cancel_timer(FL_TCB *tcb, FL_VAL id)
{
    FL_VAL lrec = find_listener(tcb, id, 1);
    if(lrec == fl_nil) return 0;
    FL_VAL var = CDR(CDR(lrec));
    if(INT(CAR(CDR(lrec))) < 0) {   /* periodic? */
        FL_VAL st = fl_chase_tail(var);
        if(BITS(st) == 0 && TAG(st) == FL_VAR_TAG) 
            fl_assign(tcb, fl_nil, st);
    } else if(CAR(var) == var) fl_assign(tcb, fl_false, var);
    unref(tcb, lrec);
    struct kevent ev;
    EV_SET(&ev, INT(id), EVFILT_TIMER, EV_DELETE, 0, 0, 0);
    int r = kevent(tcb->event_queue, &ev, 1, NULL, 0, NULL);
    if(r == -1) fl_rt_error(tcb, id, FL_IO_ERROR);
    return 1;
}

int fl_restart_timer(FL_TCB *tcb, FL_VAL id, int ms)
{
    FL_VAL lrec = find_listener(tcb, id, 0);
    if(lrec == fl_nil) return 0;
    struct kevent ev[ 2 ];
    int n = 1;
    if(INT(CAR(CDR(lrec))) >= 0) { /* single shot */
        /* just overriding data doesn't seem to work with EV_ONESHOT timer
           so delete old timer and add new */
        EV_SET(&ev[ 0 ], INT(id), EVFILT_TIMER, EV_DELETE, 0, 0, 0);
        EV_SET(&ev[ 1 ], INT(id), EVFILT_TIMER, EV_ADD|EV_ONESHOT, 0, ms, lrec);
        n = 2;
    } else EV_SET(&ev[ 0 ], INT(id), EVFILT_TIMER, EV_ADD, 0, ms, lrec);
    int r = kevent(tcb->event_queue, ev, n, NULL, 0, NULL);
    if(r == -1) fl_rt_error(tcb, id, FL_IO_ERROR);
    return  1;
}
