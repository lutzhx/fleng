/* FLENG - runtime system - some common internal operations */

#ifndef FLENG_UTIL_H

#include "fleng.h"
#include <assert.h>
#include <stdlib.h>
#include <stdarg.h>

#define CAR(x)      ((FL_CELL *)(x))->car
#define CDR(x)      ((FL_CELL *)(x))->cdr
#define INT(x)      ((long)(x) >> 1)
#define TAG(x)      (((FL_CELL *)(x))->tag & FL_TAG_MASK)
#define COUNT(x)    (((FL_CELL *)(x))->tag & FL_COUNT_MASK)
#define BITS(x)     ((long)(x) & FL_BITS_MASK)
#define MKINT(x)    ((FL_VAL)(((long)(x) << 1) | 1))

#define ISINT(x)    ((BITS(x) & 1) != 0)
#define ISSTRING(x) (BITS(x) == 2)
#define ISCELL(x)   (BITS(x) == 0)

#define STRINGLENGTH(x)   (*((unsigned char *)(x)))

#if !defined(__LP64__) && !defined(_LP64)
#define POINTER(x)  (INT(x) & 0x7fffffff)
#else
#define POINTER(x)  ((void *)(INT(x)))
#endif

#define RPTR_ID(x)   (((unsigned long)(x) & 0xff) >> 1)

static FL_VAL RPTR(FL_VAL x)
{
    int id = RPTR_ID(x);
    unsigned long base = (unsigned long)fl_tcbs[ id - 1 ].heap;
    return (FL_VAL)(((unsigned long)(x) >> 8) + base);
}

static FL_VAL addref(FL_VAL x)
{
    if(((long)x & FL_BITS_MASK) != 0) return x;
    if(COUNT(x) == FL_COUNT_MASK) return x;
    ++((FL_CELL *)x)->tag;
    return x;
}

static FL_VAL deref(FL_VAL x)
{
    for(;;) {
        if(((long)x & 3) != 0) return x;
        if((((FL_CELL *)x)->tag & FL_TAG_MASK) == FL_VAR_TAG) {
            if(((FL_CELL *)x)->car == x) return x;
            x = ((FL_CELL *)x)->car;
        } else return x;
    }
}

static void unref(FL_TCB *tcb, FL_VAL x)
{
    if(((long)x & FL_BITS_MASK) != 0) return;
    assert(CAR(x) != (FL_VAL)FL_BROKEN_HEART);
    if(COUNT(x) == FL_COUNT_MASK) return;
    assert((FL_CELL *)x >= tcb->heap && (FL_CELL *)x < tcb->heapend);
    assert((((FL_CELL *)x)->tag & FL_COUNT_MASK) > 0);
    --((FL_CELL *)x)->tag;
    if(COUNT(x) == 0) fl_release(tcb, x);
}

static FL_VAL mkcell(FL_TCB *tcb, unsigned long tag, FL_VAL car, FL_VAL cdr)
{
    FL_VAL x = fl_alloc_cell(tcb, car, cdr);
    ((FL_CELL *)x)->tag = tag;
    return x;
}

static FL_VAL mkrptr(FL_TCB *tcb, FL_VAL x)
{
    int id = tcb->ordinal;
    unsigned long base = (unsigned long)tcb->heap;
    unsigned long p = (unsigned long)x - base;
    return (FL_VAL)((p << 8) | (id << 1) | 1);
}

static FL_VAL mkvar(FL_TCB *tcb)
{
    FL_VAL x = fl_alloc_cell(tcb, fl_nil, fl_nil);
    ((FL_CELL *)x)->tag = FL_VAR_TAG;
    CAR(x) = x; /* no addref, so done explicitly */
    return x;
}

static FL_VAL mklist(FL_TCB *tcb, FL_VAL car, FL_VAL cdr)
{
    return mkcell(tcb, FL_LIST_TAG, car, cdr);
}

static FL_VAL mklistn(FL_TCB *tcb, int n, ...)
{
    va_list ap;
    FL_VAL lst = fl_nil, tail;
    va_start(ap, n);
    while(n--) {
        FL_VAL n = mklist(tcb, va_arg(ap, FL_VAL), fl_nil);
        if(lst == fl_nil) lst = n;
        else CDR(tail) = addref(n);
        tail = n;
    }
    return lst;
}

static FL_VAL mktuple(FL_TCB *tcb, FL_VAL name, int arity, FL_VAL args)
{
    return mkcell(tcb, FL_TUPLE_BIT | (arity << 24), name, args);
}

#define LIST(x, h, t)   FL_VAL h = deref(CAR(x)), t = deref(CDR(x))
#define TUPLE_LENGTH(x) ((((FL_CELL *)(x))->tag >> 24) | 0x7f)

#define TUPLE(x, f, n, a) \
    FL_VAL f = CAR(x), a = CDR(x); \
    int n = TUPLE_LENGTH(x)
#define FLOAT(x, n) \
    double n = ((long)(x) & 1) != 0 ? INT(x) : FLOATVAL(x)

#define STRING(x)   ((char *)(x) + 1)
#define MARK(x)     ((FL_VAL)((unsigned long)(x) | 1))
#define UNMARK(x)   ((FL_VAL)((unsigned long)(x) & ~1))
#define FLOATVAL(x) (*((double *)&CAR(x)))

#define CHECK_INT(x) \
    {FL_VAL _chk = (x); \
     if(((long)_chk & 1) == 0) fl_rt_error(tcb, _chk, FL_NOT_AN_INT);}
#define CHECK_STRING(x) \
    {FL_VAL _chk = (x); \
     if(BITS(_chk) != 2) fl_rt_error(tcb, _chk, FL_NOT_A_STRING);}
#define CHECK_CELL(x) \
    {FL_VAL _chk = (x); \
     if(BITS(_chk) != 0) fl_rt_error(tcb, _chk, FL_NOT_A_CELL);}
#define CHECK_LIST(x) \
    {FL_VAL _chk = (x); \
     if(BITS(_chk) != 0 || TAG(_chk) != FL_LIST_TAG) \
        fl_rt_error(tcb, _chk, FL_NOT_A_LIST);}
#define CHECK_MODULE(x) \
    {FL_VAL _chk = (x); \
     if(BITS(_chk) != 0 || TAG(_chk) != FL_MODULE_TAG) \
        fl_rt_error(tcb, _chk, FL_NOT_A_MODULE);}
#define CHECK_VAR(x) \
    {FL_VAL _chk = (x); \
     if(BITS(_chk) != 0 || \
        (TAG(_chk) != FL_VAR_TAG && TAG(_chk) != FL_REF_TAG)) \
        fl_rt_error(tcb, _chk, FL_NOT_A_LIST);}
#define CHECK_NUMBER(x) \
    {FL_VAL _chk = (x); \
     if(((long)_chk & 1) == 0 && \
        (BITS(_chk) == 2 || TAG(_chk) != FL_FLOAT_TAG)) \
        fl_rt_error(tcb, _chk, FL_NOT_A_NUMBER);}
#define CHECK_TUPLE(x) \
    {FL_VAL _chk = (x); \
     if(BITS(_chk) != 0 || \
        (((FL_CELL *)_chk)->tag & FL_TUPLE_BIT) == 0) \
        fl_rt_error(tcb, _chk, FL_NOT_A_TUPLE);}

/* Branchless UTF-8 decoder
 * https://github.com/skeeto/branchless-utf8/blob/master/utf8.h
 * This is free and unencumbered software released into the public domain. */
static char *utf8_decode(char *buf, unsigned int *c)
{
    static const char lengths[] = {
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 3, 3, 4, 0
    };
    static const int masks[]  = {0x00, 0x7f, 0x1f, 0x0f, 0x07};
    static const unsigned int mins[] = {4194304, 0, 128, 2048, 65536};
    static const int shiftc[] = {0, 18, 12, 6, 0};
    static const int shifte[] = {0, 6, 4, 2, 0};
    unsigned char *s = (unsigned char *)buf;
    int len = lengths[s[0] >> 3];
    unsigned char *next = s + len + !len;
    *c  = (unsigned int)(s[0] & masks[len]) << 18;
    *c |= (unsigned int)(s[1] & 0x3f) << 12;
    *c |= (unsigned int)(s[2] & 0x3f) <<  6;
    *c |= (unsigned int)(s[3] & 0x3f) <<  0;
    *c >>= shiftc[len];
    return (char *)next;
}

static char *utf8_encode(unsigned int u, char *p1)
{
    unsigned char *p = (unsigned char *)p1;
    if(u < 0x80) *(p++) = u;
    else if(u < 0x800) {
        *(p++) = (u >> 6) | 0xC0;
        *(p++) = (u & 0x3F) | 0x80;
    } else if(u < 0x10000) {
        *(p++) = (u >> 12) | 0xE0;
        *(p++) = ((u >> 6) & 0x3F) | 0x80;
        *(p++) = (u & 0x3F) | 0x80;
    } else if(u < 0x110000) {
        *(p++) = (u >> 18) | 0xF0;
        *(p++) = ((u >> 12) & 0x3F) | 0x80;
        *(p++) = ((u >> 6) & 0x3F) | 0x80;
        *(p++) = (u & 0x3F) | 0x80;
    }
    return (char *)p;
}

static void out_of_memory(void)
{
    fputs("out of memory\n", stderr);
    fl_terminate(1);
}

static char *init_cbuf(FL_TCB *tcb)
{
    if(tcb->cbuf == NULL) tcb->cbuf = malloc(tcb->cbuf_len = 10000);
    return tcb->cbuf;
}

static char *stringify_next(FL_TCB *tcb, char **pp, FL_VAL str, int *len)
{
    char *p = *pp;
    if(str == fl_nil) {
        if(len != NULL) *len = 0;
        return "";
    }
    if(BITS(str) == 2) {
        if(len != NULL) *len = *((char *)str);
        return (char *)str + 1;
    }
    char *rp = p;
    while(BITS(str) == 0) {
        if(TAG(str) != FL_LIST_TAG) break;
        FL_VAL x = deref(CAR(str));
        if(((long)x & FL_INT_BIT) == 0) break;
        if(p + 5 > tcb->cbuf + tcb->cbuf_len) {
            long offset = p - tcb->cbuf;
            tcb->cbuf = realloc(tcb->cbuf, tcb->cbuf_len *= 2);
            if(tcb->cbuf == NULL) out_of_memory();
            p = tcb->cbuf + offset;
        }
        p = utf8_encode(INT(x), p);
        str = deref(CDR(str));
    }
    if(str == fl_nil) {
        *p = '\0';
        *pp = p + 1;
        if(len != NULL) *len = p - rp;
        return rp;
    }
    fl_rt_error(tcb, str, FL_NOT_A_STRING);
    return NULL;
}

static char *stringify(FL_TCB *tcb, FL_VAL str, int *len)
{
    char *p = init_cbuf(tcb);
    return stringify_next(tcb, &p, str, len);
}

static FL_VAL mkcharlist(FL_TCB *tcb, char *ps, int len, FL_VAL tail)
{
    FL_VAL n = fl_nil, lst;
    char *pe = ps + len;
    while(ps < pe) {
        FL_VAL c = fl_alloc_cell(tcb, MKINT(*(ps++)), fl_nil); 
        ((FL_CELL *)c)->tag = FL_LIST_TAG;
        if(n != fl_nil) ((FL_CELL *)n)->cdr = addref(c);
        else lst = c;
        n = c;
    }
    if(n != fl_nil) ((FL_CELL *)n)->cdr = addref(tail);
    else lst = tail;
    return lst;
}

#endif
