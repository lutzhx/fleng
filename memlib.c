/* FLENG - raw memory library - C part */

#include "fleng.h"
#include "fleng-util.h"
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>

void fl_mem_alloc_2(FL_TCB *tcb, FL_VAL size, FL_VAL var)
{
    CHECK_INT(size);
    fl_assign(tcb, MKINT(malloc(INT(size))), var);
}

void fl_mem_free_2(FL_TCB *tcb, FL_VAL adr, FL_VAL done)
{
    CHECK_INT(adr);
    free((void *)POINTER(adr));
    fl_assign(tcb, fl_nil, done);
}

void fl_mem_put_4(FL_TCB *tcb, FL_VAL adr, FL_VAL off, FL_VAL bytes, FL_VAL done)
{
    CHECK_INT(adr);
    CHECK_INT(off);
    char *p = (char *)POINTER(adr) + INT(off) - 1;
    if(((long)bytes & 1) == 1) *p = INT(bytes);
    else {
        int blen;
        char *b = stringify(tcb, bytes, &blen);
        memcpy(p, b, blen);
    }
    fl_assign(tcb, fl_nil, done);
}

void fl_mem_get_4(FL_TCB *tcb, FL_VAL adr, FL_VAL off, FL_VAL len, FL_VAL more)
{
    CHECK_INT(adr);
    CHECK_INT(len);
    LIST(more, bytes, tail);
    char *p = (char *)POINTER(adr) + INT(off) - 1;
    int n = INT(len);
    if(n < 1) {
        fl_unify_result(tcb, tail, bytes);
        return;
    }
    FL_VAL lst = tail;
    p += n;
    while(n-- > 0) lst = mklist(tcb, MKINT(*(--p)), lst);
    fl_unify_result(tcb, addref(lst), bytes);
    unref(tcb, lst);
}

void fl_mem_write_4(FL_TCB *tcb, FL_VAL adr, FL_VAL off, FL_VAL len, FL_VAL more)
{
    CHECK_INT(adr);
    CHECK_INT(len);
    LIST(more, fd, done);
    CHECK_INT(fd);
    char *p = (char *)POINTER(adr) + INT(off) - 1;
    int r = write(INT(fd), p, INT(len));
    fl_assign(tcb, MKINT(r), done);
}

void fl_mem_read_4(FL_TCB *tcb, FL_VAL adr, FL_VAL off, FL_VAL len, FL_VAL more)
{
    CHECK_INT(adr);
    CHECK_INT(len);
    LIST(more, fd, done);
    CHECK_INT(fd);
    char *p = (char *)POINTER(adr) + INT(off) - 1;
    int r = read(INT(fd), p, INT(len));
    fl_assign(tcb, MKINT(r), done);
}

void fl_mem_map_4(FL_TCB *tcb, FL_VAL fd, FL_VAL len, FL_VAL mode, FL_VAL adr)
{
    CHECK_STRING(mode);
    CHECK_INT(fd);
    CHECK_INT(len);
    int prot = 0;
    int flags = 0;
    for(char *p = STRING(mode); *p != '\0'; ++p) {
        switch(*p) {
        case 'p': flags |= MAP_PRIVATE; break;
        case 'a': flags |= MAP_ANON; break;
        case 's': flags |= MAP_SHARED; break;
        case 'r': prot |= PROT_READ; break;
        case 'w': prot |= PROT_WRITE; break;
        case 'x': prot |= PROT_EXEC; break;
        }
    }
    void *addr = mmap(NULL, INT(len), prot, MAP_PRIVATE, INT(fd), 0);
    if(addr == MAP_FAILED) fl_rt_error(tcb, fd, FL_IO_ERROR);
    fl_assign(tcb, MKINT(addr), adr);
}

void fl_mem_unmap_3(FL_TCB *tcb, FL_VAL adr, FL_VAL len, FL_VAL done)
{
    CHECK_INT(adr);
    CHECK_INT(len);
    munmap((void *)INT(adr), INT(len));
    fl_assign(tcb, fl_nil, done); 
}

void fl_mem_search_4(FL_TCB *tcb, FL_VAL addr, FL_VAL off, FL_VAL len, FL_VAL more)
{
    CHECK_INT(addr);
    CHECK_INT(len);
    LIST(more, what, var);
    char *a = (char *)POINTER(addr) + INT(off) - 1;
    int alen = INT(len);
    if((BITS(what) & 1) != 0) {
        char *w = memchr(a, INT(what), alen);
        fl_assign(tcb, w == NULL ? MKINT(0) : MKINT(w - a + 1), var);
        return;
    }
    int blen;    
    char *b = stringify(tcb, what, &blen);
    int p = 0;
    while(p + blen < alen) {
        if(memcmp(a + p, b, blen) == 0) {
            fl_assign(tcb, MKINT(p + 1), var);
            return;
        }
        ++p;
    }
    fl_assign(tcb, MKINT(0), var);
}

void fl_mem_char_offset_4(FL_TCB *tcb, FL_VAL addr, FL_VAL len, FL_VAL index, FL_VAL off)
{
    CHECK_INT(addr);
    CHECK_INT(len);
    CHECK_INT(index);
    char *a = (char *)POINTER(addr);
    int alen = INT(len);
    int i = INT(index);
    int o = 0;
    char *p = a;
    unsigned int d;
    while(i--) {
        if(p > a + alen) {
            fl_assign(tcb, fl_nil, off);
            return;
        }
        p = utf8_decode(p, &d);
    }
    fl_assign(tcb, MKINT(p - a), off);
}

void fl_mem_fill_4(FL_TCB *tcb, FL_VAL addr, FL_VAL off, FL_VAL len, FL_VAL more)
{
    CHECK_INT(addr);
    CHECK_INT(len);
    CHECK_INT(off);
    LIST(more, bytes, done);
    char *p = (char *)POINTER(addr) + INT(off) - 1;
    if(((long)bytes & 1) == 1) memset(p, INT(bytes), INT(len));
    else {
        int blen;
        int tlen = INT(len);
        char *b = stringify(tcb, bytes, &blen);
        while(tlen > 0) {
            if(blen > tlen) blen = tlen;
            for(int i = 0; i < blen; ++i)
                *(p++) = b[ i ];
            tlen -= blen;
        }
    }
    fl_assign(tcb, fl_nil, done);
}

void fl_mem_copy_4(FL_TCB *tcb, FL_VAL from, FL_VAL to, FL_VAL s1, FL_VAL more)
{
    CHECK_INT(from);
    CHECK_INT(to);
    CHECK_INT(s1);
    LIST(more, s2, m2);    
    LIST(m2, count, done);
    CHECK_INT(s2);
    CHECK_INT(count);
    memcpy((char *)POINTER(to) + INT(s2) - 1, (char *)POINTER(from) + INT(s1) - 1,
        INT(count));
    fl_assign(tcb, fl_nil, done);
}
