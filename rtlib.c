/* FLENG - runtime system - library */

#include "fleng.h"
#include "fleng-util.h"
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <math.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <time.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <dirent.h>

#define INIT_CBUF       10000
#define MAX_ARGS        256

static void io_error(FL_TCB *tcb, FL_VAL result)
{
    int err = errno;
    char *msg = strerror(err);
    FL_VAL str = fl_mkstring(msg, strlen(msg));
    FL_VAL e = mktuple(tcb, fl_error, 2, mklistn(tcb, 2, MKINT(err), str));
    fl_assign(tcb, e, result);
}

void fl_write_2(FL_TCB *tcb, FL_VAL x, FL_VAL done)
{
    fl_write(tcb, stdout, x);
    fflush(stdout);
    fl_assign(tcb, fl_nil, done);
}

void fl_writeln_2(FL_TCB *tcb, FL_VAL x, FL_VAL done)
{
    fl_write(tcb, stdout, x);
    fputc('\n', stdout);
    fflush(stdout);
    fl_assign(tcb, fl_nil, done);
}

void fl_mkmodule_2(FL_TCB *tcb, FL_VAL mod, FL_VAL var)
{
    FL_VAL m = fl_alloc_cell(tcb, mod, fl_nil);
    ((FL_CELL *)m)->tag = FL_MODULE_TAG;
    fl_assign(tcb, m, var);
}

void fl_find_module_2(FL_TCB *tcb, FL_VAL name, FL_VAL var)
{
    FL_MODULE *m = fl_find_module(tcb, name);
    if(m != NULL) {
        FL_VAL mod = MARK(m);
        fl_assign(tcb, mod, var);
        return;
    }
    fl_rt_error(tcb, name, FL_NO_MODULE);
}

void fl_all_modules_1(FL_TCB *tcb, FL_VAL var)
{
    fl_unify_result(tcb, fl_all_modules(tcb), var);
}

static FL_VAL module_ptr(FL_TCB *tcb, FL_VAL name)
{
    FL_VAL mod;
    if(ISSTRING(name)) {
        FL_MODULE *m = fl_find_module(tcb, name);
        if(m != NULL) mod = MARK(m);    
        else fl_rt_error(tcb, name, FL_NO_MODULE);
    } else if(ISCELL(name) && TAG(name) == FL_MODULE_TAG)
        mod = CAR(name);
    else fl_rt_error(tcb, name, FL_NOT_A_MODULE);
    return mod;
}

void fl_get_module_ptr_2(FL_TCB *tcb, FL_VAL name, FL_VAL var)
{
    fl_assign(tcb, module_ptr(tcb, name), var);
}

void fl_get_module_2(FL_TCB *tcb, FL_VAL name, FL_VAL var)
{
    FL_VAL mod = fl_alloc_cell(tcb, module_ptr(tcb, name), fl_nil);
    ((FL_CELL *)mod)->tag = FL_MODULE_TAG;
    fl_assign(tcb, mod, var);
}

void fl_module_exports_2(FL_TCB *tcb, FL_VAL mod, FL_VAL var)
{
    CHECK_MODULE(mod);
    FL_MODULE *m = (FL_MODULE *)UNMARK(CAR(mod));
    FL_VAL lst = fl_nil;
    for(int i = 0; i < m->npdefs; ++i) {
        if(m->pdefs[ i ].exported) {
            FL_VAL x = mklist(tcb, m->pdefs[ i ].name, 
                mklist(tcb, MKINT(m->pdefs[ i ].arity), fl_nil));
            lst = mklist(tcb, mktuple(tcb, fl_slash, 2, x), lst);
        }
    }
    fl_unify_result(tcb, lst, var);
}

void fl_module_name_2(FL_TCB *tcb, FL_VAL mod, FL_VAL var)
{
    CHECK_MODULE(mod);
    FL_MODULE *m = (FL_MODULE *)UNMARK(CAR(mod));
    fl_assign(tcb, m->name, var);
}

void fl_find_pdef_4(FL_TCB *tcb, FL_VAL module, FL_VAL name, FL_VAL arity, 
    FL_VAL var)
{
    FL_MODULE *mod = (FL_MODULE *)UNMARK(module);
    int a = INT(arity);
    for(int i = 0; i < mod->npdefs; ++i) {
        if(mod->pdefs[ i ].name == name && mod->pdefs[ i ].arity == a &&
            mod->pdefs[ i ].exported) {
            fl_assign(tcb, MARK(mod->pdefs[ i ].addr), var);
            return;
        }
    }
    fl_rt_error2(tcb, name, arity, FL_NO_PDEF);
}

void fl_arity_2(void *tcb, FL_VAL x, FL_VAL var)
{
    long n = (TAG(x) >> 24) & 0x7f;
    fl_assign(tcb, (FL_VAL)(((long)n << 1) | 1), var);
}

void fl_uncons_3(FL_TCB *tcb, FL_VAL lst, FL_VAL var1, FL_VAL var2)
{
    fl_assign(tcb, CAR(lst), var1);
    fl_assign(tcb, CDR(lst), var2);
}

void fl_set_cache_2(FL_TCB *tcb, FL_VAL cache, FL_VAL pdef)
{
    *((long *)((long)cache & ~1)) = (long)pdef & ~1;
}

void fl_string_length_2(FL_TCB *tcb, FL_VAL x, FL_VAL var)
{
    unsigned char *p = (unsigned char *)x;
    int len = 0, n = *(p++);
    unsigned char *pe = p + n;
    while(p < pe) {
        unsigned char c = *p;
        if((c & 0xf8) == 0xf0) p += 4;
        else if((c & 0xf0) == 0xe0) p += 3;
        else if((c & 0xe0) == 0xc0) p += 2;
        else ++p;
        ++len;
    }
    fl_assign(tcb, MKINT(len), var);
}

void fl_tuple_length_2(FL_TCB *tcb, FL_VAL x, FL_VAL var)
{
    fl_assign(tcb, MKINT(((TAG(deref(x)) >> 24) & 0x7f) + 1), var);
}

void fl_command_line_1(FL_TCB *tcb, FL_VAL v)
{
    FL_VAL lst = fl_nil;
    char **p = fl_argv;
    while(*p != NULL) ++p;
    while(p-- > fl_argv) {
        lst = fl_alloc_cell(tcb, fl_mkstring(*p, strlen(*p)), lst);
        ((FL_CELL *)lst)->tag = FL_LIST_TAG;
    }
    fl_unify_result(tcb, lst, v);
}

static void str_to_int(FL_TCB *tcb, char *str, int base, FL_VAL x, FL_VAL var)
{
    char *end;
    long n = strtol(str, &end, base);
    if(*end != '\0') goto fail;
    if((n == LONG_MIN || n == LONG_MAX) && errno == ERANGE) goto fail;
    if(n == 0 && errno == EINVAL) goto fail;
    fl_assign(tcb, (FL_VAL)((n << 1) | 1), var);
    return;
fail:
    fl_rt_error(tcb, x, FL_CANT_CONVERT);
}

void fl_string_to_integer_3(FL_TCB *tcb, FL_VAL str, FL_VAL base, FL_VAL var)
{
    if(((long)base & 1) != 1) fl_rt_error(tcb, base, FL_NOT_AN_INT);
    int b = INT(base);
    if(BITS(str) != 2) fl_rt_error(tcb, str, FL_NOT_AN_ATOM);
    str_to_int(tcb, str + 1, b, str, var);
}

void fl_string_to_integer_2(FL_TCB *tcb, FL_VAL str, FL_VAL var)
{
    fl_string_to_integer_3(tcb, str, (FL_VAL)((10 << 1) | 1), var);
}

static void str_to_float(FL_TCB *tcb, char *str, FL_VAL x, FL_VAL var) 
{
    char *end;
    double n = strtod(str, &end);
    if(*end != '\0') goto fail;
    if((n == HUGE_VAL || n == -HUGE_VAL || n == 0) && errno == ERANGE)
        goto fail;
    FL_VAL c = fl_alloc_float(tcb, n);
    fl_assign(tcb, c, var);
    return;
fail:
    fl_rt_error(tcb, x, FL_CANT_CONVERT);
}

void fl_string_to_float_2(FL_TCB *tcb, FL_VAL str, FL_VAL var)
{
    char *end;
    if(BITS(str) != 2) fl_rt_error(tcb, str, FL_NOT_AN_ATOM);
    str_to_float(tcb, (char *)str + 1, str, var);
}

static void str_to_list(FL_TCB *tcb, char *str, int len, FL_VAL var, 
    FL_VAL tail)
{
    FL_VAL n = fl_nil, lst;
    char *ps = str;
    char *pe = ps + len;
    uint32_t ch;
    while(ps < pe) {
        ps = utf8_decode(ps, &ch);
        FL_VAL c = fl_alloc_cell(tcb, MKINT(ch), fl_nil); 
        ((FL_CELL *)c)->tag = FL_LIST_TAG;
        if(n != fl_nil) ((FL_CELL *)n)->cdr = addref(c);
        else lst = c;
        n = c;
    }
    if(n != fl_nil) ((FL_CELL *)n)->cdr = addref(tail);
    else lst = tail;
    fl_unify_result(tcb, lst, var);    
}

void fl_string_to_list_3(FL_TCB *tcb, FL_VAL str, FL_VAL var, FL_VAL tail)
{
    if(BITS(str) != 2) fl_rt_error(tcb, str, FL_NOT_AN_ATOM);
    char *ps = (char *)str;
    str_to_list(tcb, ps + 1, STRINGLENGTH(ps), var, tail);
}

void fl_string_to_byte_list_3(FL_TCB *tcb, FL_VAL str, FL_VAL var, FL_VAL tail)
{
    if(BITS(str) != 2) fl_rt_error(tcb, str, FL_NOT_AN_ATOM);
    char *ps = (char *)str + 1;
    FL_VAL n = fl_nil, lst;
    char *pe = ps + STRINGLENGTH(str);
    while(ps < pe) {
        FL_VAL c = fl_alloc_cell(tcb, MKINT(*(ps++)), fl_nil); 
        ((FL_CELL *)c)->tag = FL_LIST_TAG;
        if(n != fl_nil) ((FL_CELL *)n)->cdr = addref(c);
        else lst = c;
        n = c;
    }
    if(n != fl_nil) ((FL_CELL *)n)->cdr = addref(tail);
    else lst = tail;
    fl_unify_result(tcb, lst, var);    
}

void fl_list_to_string_2(FL_TCB *tcb, FL_VAL lst, FL_VAL var)
{
    int len;
    char *s = stringify(tcb, lst, &len);
    fl_assign(tcb, fl_mkstring(s, len), var);
}

void fl_list_to_number_3(FL_TCB *tcb, FL_VAL lst, FL_VAL base, FL_VAL var)
{
    if(((long)base & 1) != 1) fl_rt_error(tcb, base, FL_NOT_AN_INT);
    int b = INT(base);
    char *p = init_cbuf(tcb);
    int f = 0;
    FL_VAL lst0 = lst;
    while(BITS(lst) == 0) {
        FL_VAL x = deref(CAR(lst));
        if(((long)x & FL_INT_BIT) == 0) fl_rt_error(tcb, x, FL_NOT_AN_INT);
        unsigned char c = INT(x);
        if(c == '.' || ((c == 'e' || c == 'E') && b == 10 && lst != lst0)) 
            f = 1;
        *(p++) = c;
        lst = deref(CDR(lst));
    }
    *p = '\0';
    if(f) str_to_float(tcb, tcb->cbuf, lst, var);
    else str_to_int(tcb, tcb->cbuf, b, lst, var);
}

void fl_list_to_number_2(FL_TCB *tcb, FL_VAL lst, FL_VAL var)
{
    fl_list_to_number_3(tcb, lst, (FL_VAL)((10 << 1) | 1), var);
}

void fl_number_to_list_4(FL_TCB *tcb, FL_VAL num, FL_VAL base, FL_VAL var, 
    FL_VAL tail)
{
    char *p;
    if(((long)num & FL_INT_BIT) != 0) {
        if(((long)base & 1) != 1) fl_rt_error(tcb, base, FL_NOT_AN_INT);
        int b = INT(base);
        p = init_cbuf(tcb) + 128;
        tcb->cbuf[ 128 ] = '\0';
        long n = labs(INT(num));
        if(n == 0) *(--p) = '0';
        while(n != 0) {
            char c = n % b + '0';
            *(--p) = c > '9' ? c - '9' + 'a' - 1 : c;
            n /= b;
        }
        if((long)num < 0) *(--p) = '-';
    } else if(BITS(num) != 0 || TAG(num) != FL_FLOAT_TAG)
        fl_rt_error(tcb, num, FL_NOT_A_NUMBER);
    else {
        double i, n = FLOATVAL(num);
        double f = modf(n, &i);
        p = init_cbuf(tcb);
        sprintf(p, "%.15g", n);
        if(strchr(p, '.') == NULL) strcat(p, ".0");
    }
    str_to_list(tcb, p, strlen(p), var, tail);
}

void fl_number_to_list_3(FL_TCB *tcb, FL_VAL num, FL_VAL var, FL_VAL tail)
{
    fl_number_to_list_4(tcb, num, (FL_VAL)((10 << 1) | 1), var, tail);
}

void fl_open_file_3(FL_TCB *tcb, FL_VAL name, FL_VAL mode, FL_VAL var)
{
    int len;
    char *fname = stringify(tcb, name, &len);
    CHECK_STRING(mode);
    int m = O_RDONLY;
    mode_t mo = 0644;
    switch(*((char *)mode + 1)) {
    case 'w': m = O_WRONLY | O_TRUNC | O_CREAT; break;
    case 'r': m = O_RDONLY; break;
    case 'a': m = O_WRONLY | O_APPEND | O_CREAT; break;
    }
    int fd = open(fname, m, mo);
    if(fd == -1) io_error(tcb, var);
    else fl_assign(tcb, MKINT(fd), var);
}

void fl_close_file_1(FL_TCB *tcb, FL_VAL fd)
{
    CHECK_INT(fd);
    close(INT(fd));
}

void fl_close_file_2(FL_TCB *tcb, FL_VAL fd, FL_VAL var)
{
    fl_close_file_1(tcb, fd);
    fl_assign(tcb, fl_nil, var);
}

static void bytes_to_list(FL_TCB *tcb, char *str, int len, FL_VAL var, 
    FL_VAL tail)
{
    FL_VAL lst = tail;
    for(char *p = str + len - 1; p >= str; --p) {
        lst = fl_alloc_cell(tcb, MKINT(*((unsigned char *)p)), lst);
        ((FL_CELL *)lst)->tag = FL_LIST_TAG;
    }
    fl_unify_result(tcb, lst, var);    
}

void fl_scan_block_2(FL_TCB *tcb, FL_VAL str, FL_VAL count)
{
    int n = 0;
    while(BITS(str) == 0 && TAG(str) == FL_LIST_TAG) {
        str = deref(CDR(str));
        ++n;
    }
    fl_assign(tcb, MKINT(n), count);
}

void fl_write_block_4(FL_TCB *tcb, FL_VAL fd, FL_VAL str, FL_VAL n, FL_VAL rest)
{
    CHECK_INT(fd);
    int len = 0;
    if(((long)n & FL_INT_BIT) != 0) len = INT(n); /* non-int means "all" */
    char *p;
    if(BITS(str) == 2) {
        int slen = STRINGLENGTH(str);
        p = (char *)str + 1;
        if(len == 0 || len > slen) len = slen;
        int w = write(INT(fd), p, len);
        if(w == -1) {
            if(errno == EINTR) fl_assign(tcb, fl_false, rest);
            else io_error(tcb, rest);
            return;
        }
        if(w < slen) bytes_to_list(tcb, p + w, slen - w, rest, fl_nil);
        else fl_assign(tcb, fl_nil, rest);
        return;
    }
    if(TAG(str) != FL_LIST_TAG) fl_rt_error(tcb, str, FL_NOT_A_STRING);
    p = init_cbuf(tcb);
    FL_VAL lst = str;
    int count = 0;
    while(BITS(lst) == 0) {
        if(p >= tcb->cbuf + tcb->cbuf_len) {
            int offset = p - tcb->cbuf;
            tcb->cbuf = realloc(tcb->cbuf, tcb->cbuf_len *= 2);
            if(tcb->cbuf == NULL) out_of_memory();
            p = tcb->cbuf + offset;
        }
        FL_VAL x = deref(CAR(lst));
        if(((long)x & FL_INT_BIT) == 0) fl_rt_error(tcb, lst, FL_NOT_A_STRING);
        *(p++) = INT(x);
        lst = deref(CDR(lst));
        ++count;
        if(len && count >= len) break;
    }
    int w = write(INT(fd), tcb->cbuf, len ? len : count);
    if(w == -1) {
        io_error(tcb, rest);
        return;
    }
    if(w < len) {
        while(w-- && BITS(str) == 0) str = deref(CDR(str));
        fl_unify_result(tcb, str, rest);
    } else fl_unify_result(tcb, lst, rest);
}

void fl_read_block_4(FL_TCB *tcb, FL_VAL fd, FL_VAL n, FL_VAL lst, FL_VAL tail)
{   
    CHECK_INT(fd);
    CHECK_INT(n);
    int len = INT(n);
    init_cbuf(tcb);
    if(len >= tcb->cbuf_len) len = tcb->cbuf_len;
    int r = read(INT(fd), tcb->cbuf, len);
    if(r == -1) {
        if(errno == EINTR) fl_assign(tcb, fl_false, lst);
        else io_error(tcb, lst);
    } else if(r == 0) fl_assign(tcb, fl_nil, lst);
    else bytes_to_list(tcb, tcb->cbuf, r, lst, tail);
}

void fl_utf_encode_3(FL_TCB *tcb, FL_VAL ch, FL_VAL lst, FL_VAL tail)
{
    if(((long)ch & FL_INT_BIT) == 0) fl_rt_error(tcb, ch, FL_NOT_AN_INT);
    char *p = utf8_encode(INT(ch), init_cbuf(tcb));
    bytes_to_list(tcb, tcb->cbuf, p - tcb->cbuf, lst, tail);
}

void fl_utf_decode_3(FL_TCB *tcb, FL_VAL lst, FL_VAL var, FL_VAL rest)
{
    int n = 1;
    FL_VAL ch = CAR(lst);
    if(((long)ch & FL_INT_BIT) == 0) fl_rt_error(tcb, ch, FL_NOT_AN_INT);
    unsigned char c = INT(ch);
    if((c & 0xf8) == 0xf0) n = 4;
    else if((c & 0xf0) == 0xe0) n = 3;
    else if((c & 0xe0) == 0xc0) n = 2;
    char *p = init_cbuf(tcb);
    *(p++) = c;
    lst = deref(CDR(lst));
    while(--n > 0) {
        if(BITS(lst) != 0) {
            while(n--) *(p++) = 0;
            break;
        }
        ch = CAR(lst);
        if(((long)ch & FL_INT_BIT) == 0) fl_rt_error(tcb, ch, FL_NOT_AN_INT);
        *(p++) = INT(ch);
        lst = deref(CDR(lst));
    }
    uint32_t uc;
    utf8_decode(init_cbuf(tcb), &uc);
    fl_assign(tcb, MKINT(uc), var);
    fl_unify_result(tcb, lst, rest);
}

void fl_send_2(FL_TCB *tcb, FL_VAL port, FL_VAL x)
{
    fl_send_to_port(tcb, port, x);
}

void fl_send_3(FL_TCB *tcb, FL_VAL port, FL_VAL x, FL_VAL done)
{
    fl_send_2(tcb, port, x);
    fl_assign(tcb, fl_nil, done);
}

void fl_open_port_2(FL_TCB *tcb, FL_VAL pv, FL_VAL sv)
{
    FL_VAL port = fl_alloc_cell(tcb, sv, fl_nil);
    ((FL_CELL *)port)->tag = FL_PORT_TAG;
    fl_assign(tcb, port, pv);
}

void fl_list_to_tuple_2(FL_TCB *tcb, FL_VAL lst, FL_VAL var)
{
    if(lst == fl_nil) {
        fl_assign(tcb, fl_nil, var);
        return;
    }
    int len = 0;
    FL_VAL n = deref(CDR(lst)), args = fl_nil, *prev = NULL;
    while(BITS(n) == 0) {
        FL_VAL arg = mklist(tcb, deref(CAR(n)), fl_nil);
        if(prev == NULL) args = arg;
        else *prev = addref(arg);
        prev = &CDR(arg);
        n = deref(CDR(n));
        ++len;
    }
    FL_VAL t = mktuple(tcb, deref(CAR(lst)), len, args);
    fl_unify_result(tcb, t, var);
}

void fl_tuple_to_list_3(FL_TCB *tcb, FL_VAL tup, FL_VAL var, FL_VAL tail)
{
    if(tup == fl_nil) fl_assign(tcb, var, tail);
    else if(BITS(tup) != 0 || (((FL_CELL *)tup)->tag & FL_TUPLE_BIT) == 0)
        fl_rt_error(tcb, tup, FL_NOT_A_TUPLE);
    else {
        FL_VAL n = mklist(tcb, CAR(tup), fl_nil);
        FL_VAL lst = n;
        tup = CDR(tup);
        if(tail == fl_nil) ((FL_CELL *)lst)->cdr = addref(tup);
        else {
            while(BITS(tup) == 0) {
                FL_VAL n2 = mklist(tcb, CAR(tup), fl_nil);
                ((FL_CELL *)n)->cdr = addref(n2);
                tup = deref(CDR(tup));
                n = n2;
            }
            ((FL_CELL *)n)->cdr = addref(tail);
        }
        fl_unify_result(tcb, lst, var);
    }
}

void fl_put_arg_3(FL_TCB *tcb, FL_VAL idx, FL_VAL tup, FL_VAL x)
{
    if(BITS(tup) != 0 || (((FL_CELL *)tup)->tag & FL_TUPLE_BIT) == 0)
        fl_rt_error(tcb, tup, FL_NOT_A_TUPLE);
    int len = (((FL_CELL *)tup)->tag & ~FL_TUPLE_BIT) >> 24;
    if(((long)idx & FL_INT_BIT) == 0) fl_rt_error(tcb, idx, FL_NOT_AN_INT);
    int i = INT(idx) - 1;
    if(i < 0 || i > len) fl_rt_error(tcb, idx, FL_BAD_INDEX);
    while(i--) tup = deref(CDR(tup));
    fl_assign(tcb, x, CAR(tup));
}

void fl_put_arg_4(FL_TCB *tcb, FL_VAL idx, FL_VAL tup, FL_VAL x, FL_VAL done)
{
    fl_put_arg_3(tcb, idx, tup, x);
    fl_assign(tcb, fl_nil, done);
}

void fl_get_arg_3(FL_TCB *tcb, FL_VAL idx, FL_VAL tup, FL_VAL var)
{
    if(BITS(tup) != 0 || (((FL_CELL *)tup)->tag & FL_TUPLE_BIT) == 0)
        fl_rt_error(tcb, tup, FL_NOT_A_TUPLE);
    int len = (((FL_CELL *)tup)->tag & ~FL_TUPLE_BIT) >> 24;
    if(((long)idx & FL_INT_BIT) == 0) fl_rt_error(tcb, idx, FL_NOT_AN_INT);
    int i = INT(idx) - 1;
    if(i < 0 || i > len) fl_rt_error(tcb, idx, FL_BAD_INDEX);
    while(i--) tup = deref(CDR(tup));
    fl_unify_result(tcb, CAR(tup), var);
}

void fl_get_arg_4(FL_TCB *tcb, FL_VAL idx, FL_VAL tup, FL_VAL var, FL_VAL done)
{
    fl_get_arg_3(tcb, idx, tup, var);
    fl_assign(tcb, fl_nil, done);
}

void fl_make_tuple_2(FL_TCB *tcb, FL_VAL n, FL_VAL var)
{
    if(((long)n & FL_INT_BIT) == 0) fl_rt_error(tcb, n, FL_NOT_AN_INT);
    int len = INT(n);
    if(len < 0 || len > 0x7f) fl_rt_error(tcb, n, FL_BAD_INDEX);
    if(len == 0) fl_assign(tcb, fl_nil, var);
    else {
        FL_VAL tup = fl_nil, prev;
        int i = len;
        while(i--) {
            FL_VAL v = fl_alloc_cell(tcb, fl_nil, fl_nil);
            ((FL_CELL *)v)->tag = FL_VAR_TAG;
            ((FL_CELL *)v)->car = v;
            FL_VAL n = fl_alloc_cell(tcb, v, fl_nil);
            if(tup == fl_nil) {
                tup = n;
                ((FL_CELL *)tup)->tag = FL_TUPLE_BIT | ((len - 1) << 24);
            } else {
                ((FL_CELL *)n)->tag = FL_LIST_TAG;
                ((FL_CELL *)prev)->cdr = addref(n);
            }
            prev = n;
        }
        fl_unify_result(tcb, tup, var);
    }
}

void fl_halt_1(FL_TCB *tcb, FL_VAL status)
{
    if(((long)status & FL_INT_BIT) == 0) 
        fl_rt_error(tcb, status, FL_NOT_AN_INT);
    fl_terminate(INT(status));
}

void fl_events_1(FL_TCB *tcb, FL_VAL stream)
{
    tcb->events = addref(stream);
}

void fl_forward_3(FL_TCB *tcb, FL_VAL id, FL_VAL msg, FL_VAL done)
{
    int peer = INT(id);
    if(peer < 0 && peer >= FL_WEST) peer = tcb->peers[ -peer - 1 ];
    else if(peer < 1 || peer > fl_nthreads()) fl_rt_error(tcb, id, FL_BAD_PEER);
    if(fl_forward(tcb, peer, msg)) fl_assign(tcb, fl_true, done);
    else fl_assign(tcb, fl_false, done);
}

void fl_transmit_3(FL_TCB *tcb, FL_VAL addr, FL_VAL msg, FL_VAL done)
{
    char *port = (char *)((unsigned long)addr & ~1);
    if(fl_forward_to_port(tcb, port, msg, FL_DETACHED))
        fl_assign(tcb, fl_true, done);
    else fl_assign(tcb, fl_false, done);
}

void fl_attach_3(FL_TCB *tcb, FL_VAL fname, FL_VAL fvar, FL_VAL avar)
{
    char *fn = stringify(tcb, fname, NULL);
    int fd = open(fn, O_RDWR);
    if(fd == -1) fl_rt_error(tcb, fname, FL_IO_ERROR);
    void *addr = mmap(NULL, FL_PORT_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
    if(addr == MAP_FAILED) fl_rt_error(tcb, fname, FL_IO_ERROR);
    fl_assign(tcb, MKINT(fd), fvar);
    fl_assign(tcb, MARK(addr), avar);
}

void fl_detach_3(FL_TCB *tcb, FL_VAL file, FL_VAL addr, FL_VAL done)
{
    munmap(UNMARK(addr), FL_PORT_SIZE);
    close(INT(file));
    fl_assign(tcb, fl_nil, done);
}

void fl_detach_message_port_2(FL_TCB *tcb, FL_VAL fname, FL_VAL done)
{
    char *fn = stringify(tcb, fname, NULL);
    int fd = open(fn, O_RDWR | O_CREAT, 0600);
    if(fd == -1) fl_rt_error(tcb, fname, FL_IO_ERROR);
    ftruncate(fd, FL_PORT_SIZE);
    void *addr = mmap(NULL, FL_PORT_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
    if(addr == MAP_FAILED) fl_rt_error(tcb, fname, FL_IO_ERROR);
    free(tcb->port);
    tcb->port = addr;
    tcb->mportfile = strdup(fn);
    memset(addr, 0, FL_PORT_SIZE);
    tcb->detached = 1;
    fl_assign(tcb, fl_nil, done);
}

// invoked by "addref" message:
void fl_local_addref_1(FL_TCB *tcb, FL_VAL x)
{
    x = RPTR(x);
    addref(x);
}

// invoked by "send" message:
void fl_local_port_2(FL_TCB *tcb, FL_VAL rptr, FL_VAL val)
{
    FL_VAL x = RPTR(rptr);
    assert(TAG(x) == FL_PORT_TAG);
    fl_assign(tcb, x, val);
}

// invoked by "read" message:
void fl_local_var_unref_2(FL_TCB *tcb, FL_VAL rptr, FL_VAL val)
{
    FL_VAL x = RPTR(rptr);
    assert(TAG(x) == FL_VAR_TAG);
//    fl_logfmt(tcb, "local_var_unref _%lu (%p:%ld)\n", (unsigned long)x - (unsigned long)tcb->heap, x, COUNT(x));
    FL_VAL y = deref(x);
    int bound = x != y;
    fl_assign(tcb, y, val);
    if(bound) unref(tcb, x);
}

// invoked by "dropref" message:
void fl_local_unref_1(FL_TCB *tcb, FL_VAL rptr)
{
    FL_VAL x = RPTR(rptr);
    unref(tcb, x);
}

void fl_remote_id_2(FL_TCB *tcb, FL_VAL rptr, FL_VAL id)
{
    fl_assign(tcb, MKINT(RPTR_ID(rptr)), id);
}

// invoked by "value" message:
void fl_local_assign_2(FL_TCB *tcb, FL_VAL val, FL_VAL rptr)
{
    extern void fl_watch();
    FL_VAL var = RPTR(rptr);
    assert(TAG(var) == FL_VAR_TAG);
//    fl_logfmt(tcb, "local_assign _%lu (%p:%ld)\n", (unsigned long)var - (unsigned long)tcb->heap, var, COUNT(var));
    /* Hairy: detect suspensions for "sys:watch_var/3" and mark 3rd argument
       to avoid sending value back to owner: */
    for(FL_VAL x = CDR(var); x != fl_nil; x = CDR(x)) {
        FL_VAL slst = CAR(x);
        if(CAR(slst) != fl_nil) { /* not already resumed? */
            FL_GOAL *g = (FL_GOAL *)UNMARK(CAR(slst));
            if(g->addr == fl_watch) {
                assert(g->args[ 2 ] == fl_false);
                g->args[ 2 ] = fl_true;
#ifdef XLOGGING
                if(tcb->logging)
                    fl_logfmt(tcb, "marking watch suspension on _%lu\n", 
                        (unsigned long)var - (unsigned long)tcb->heap);
#endif
            }
        }
    }    
    fl_unify_rec_checked(tcb, var, val);
    unref(tcb, var);
}

void fl_call_event_3(FL_TCB *tcb, FL_VAL peer, FL_VAL term, FL_VAL mod)
{
    FL_VAL ev = mklist(tcb, term, mod);
    FL_VAL task = tcb->goal->task;
    if(task != fl_nil) {
        ((FL_CELL *)task)->tag += peer == MKINT(FL_ALL) ? fl_nthreads() - 1 : 1;
        ev = mklist(tcb, task, ev);
        ev = mklist(tcb, peer, ev);
        ev = mklist(tcb, MKINT(7), ev); /* SEND_TCALL */
    } else {
        ev = mklist(tcb, peer, ev);
        ev = mklist(tcb, MKINT(6), ev); /* SEND_CALL */
    }
    fl_add_event(tcb, ev);
}

void fl_assign_2(FL_TCB *tcb, FL_VAL dest, FL_VAL src)
{
    fl_assign(tcb, src, dest);
}

void fl_assign_3(FL_TCB *tcb, FL_VAL dest, FL_VAL src, FL_VAL var)
{
    fl_assign_2(tcb, dest, src);
    fl_assign(tcb, fl_nil, var);
}

void fl_listen_2(FL_TCB *tcb, FL_VAL fd, FL_VAL var)
{
    CHECK_INT(fd);
    if(fl_add_event_to_queue(tcb, FL_INPUT, fd, var, NULL) == -1)
        io_error(tcb, var);
}

void fl_threads_1(FL_TCB *tcb, FL_VAL var)
{
    fl_unify_result(tcb, MKINT(fl_nthreads()), var);
}

void fl_execute_4(FL_TCB *tcb, FL_VAL args, FL_VAL fds, FL_VAL rpid, FL_VAL status)
{
    /* fds = [In, Out, Err, Closelist] */
    int fdi = INT(deref(CAR(fds)));
    fds = deref(CDR(fds));
    int fdo = INT(deref(CAR(fds)));
    fds = deref(CDR(fds));
    int fde = INT(deref(CAR(fds)));
    fds = deref(CDR(fds));
    int cfds[ 16 ], n = 0;
    for(fds = deref(CAR(fds)); BITS(fds) == 0; fds = deref(CDR(fds)))
        cfds[ n++ ] = INT(deref(CAR(fds)));
    char *argv[ MAX_ARGS ];
    int c = 0;
    FL_VAL ap = args;
    init_cbuf(tcb);
    sigset_t ss;
    while(BITS(args) == 0) {
        char *arg = stringify(tcb, deref(CAR(args)), NULL);
        argv[ c++ ] = strdup(arg);
        args = deref(CDR(args));
    }
    argv[ c ] = NULL;
    int pid = fork();
    if(pid == -1) fl_rt_error(tcb, fl_nil, FL_IO_ERROR);
    if(pid == 0) {
        if(fdi != STDIN_FILENO) {
            dup2(fdi, STDIN_FILENO);
            close(fdi);
        }
        if(fdo != STDOUT_FILENO) {
            dup2(fdo, STDOUT_FILENO);
            close(fdo);
        }
        if(fde != STDERR_FILENO) {
            dup2(fde, STDERR_FILENO);
            close(fde);
        }
        for(int i = 0; i < n; ++i) close(cfds[ i ]);
        pthread_sigmask(SIG_BLOCK, NULL, &ss);
        pthread_sigmask(SIG_UNBLOCK, &ss, NULL);
        execvp(argv[ 0 ], argv);
        fprintf(stderr, "execve failed: %s\n", strerror(errno));
        exit(1);
    } 
    /* parent */
    FL_VAL vpid = MKINT(pid);
    fl_assign(tcb, vpid, rpid);
    if(fl_add_event_to_queue(tcb, FL_CHILD, vpid, status, NULL) == -1)
        fl_rt_error(tcb, vpid, FL_IO_ERROR);
}

void fl_fdup_2(FL_TCB *tcb, FL_VAL from, FL_VAL to)
{
    CHECK_INT(from);
    CHECK_INT(to);
    if(dup2(INT(from), INT(to)) == -1) fl_rt_error(tcb, from, FL_IO_ERROR);
}

void fl_fdup_3(FL_TCB *tcb, FL_VAL from, FL_VAL to, FL_VAL done)
{
    fl_fdup_2(tcb, from, to);
    fl_assign(tcb, fl_nil, done);
}

void fl_open_pipe_2(FL_TCB *tcb, FL_VAL in, FL_VAL out)
{
    int fds[ 2 ];
    if(pipe(fds) == -1) fl_rt_error(tcb, fl_nil, FL_IO_ERROR);
    fl_assign(tcb, (FL_VAL)(((long)fds[ 0 ] << 1) | 1), in);
    fl_assign(tcb, (FL_VAL)(((long)fds[ 1 ] << 1) | 1), out);
}

void fl_getpid_1(FL_TCB *tcb, FL_VAL pid)
{
    fl_assign(tcb, MKINT(getpid()), pid);
}

void fl_self_1(FL_TCB *tcb, FL_VAL id)
{
    fl_assign(tcb, MKINT(tcb->ordinal), id);
}

void fl_current_seconds_1(FL_TCB *tcb, FL_VAL var)
{
    fl_assign(tcb, MKINT(time(NULL)), var);
}

void fl_timer_3(FL_TCB *tcb, FL_VAL ms, FL_VAL var, FL_VAL id)
{
    fl_add_event_to_queue(tcb, FL_TIMEOUT, ms, var, id);
}

void fl_clock_3(FL_TCB *tcb, FL_VAL ms, FL_VAL var, FL_VAL id)
{
    fl_add_event_to_queue(tcb, FL_CLOCK, ms, var, id);
}

void fl_restart_timer_2(FL_TCB *tcb, FL_VAL id, FL_VAL ms)
{
    int r = fl_restart_timer(tcb, id, INT(ms));
}

void fl_restart_timer_3(FL_TCB *tcb, FL_VAL id, FL_VAL ms, FL_VAL var)
{
    int r = fl_restart_timer(tcb, id, INT(ms));
    fl_assign(tcb, r ? fl_true : fl_false, var);
}

void fl_cancel_timer_1(FL_TCB *tcb, FL_VAL id)
{
    int r = fl_cancel_timer(tcb, id);
}

void fl_cancel_timer_2(FL_TCB *tcb, FL_VAL id, FL_VAL var)
{
    int r = fl_cancel_timer(tcb, id);
    fl_assign(tcb, r ? fl_true : fl_false , var);
}

void fl_ticks_1(FL_TCB *tcb, FL_VAL ticks)
{
    fl_assign(tcb, MKINT(tcb->ticks), ticks);
}
 
void fl_signal_2(FL_TCB *tcb, FL_VAL sig, FL_VAL var)
{
    int s;
    if(ISSTRING(sig)) {
        s = fl_resolve_signal(STRING(sig));
        if(s < 0) fl_rt_error(tcb, sig, FL_BAD_SIGNAL);
    } else if(ISINT(sig)) s = INT(sig);
    else fl_rt_error(tcb, sig, FL_BAD_SIGNAL);
    fl_add_event_to_queue(tcb, FL_SIGNAL, MKINT(s), var, NULL);   
}

void fl_signal_3(FL_TCB *tcb, FL_VAL sig, FL_VAL var, FL_VAL done)
{
    fl_signal_2(tcb, sig, var);
    fl_assign(tcb, fl_nil, done);
}

void fl_random_1(FL_TCB *tcb, FL_VAL var)
{
    fl_assign(tcb, fl_alloc_float(tcb, fl_random()), var);
}

void fl_set_random_seed_2(FL_TCB *tcb, FL_VAL seed, FL_VAL done)
{
    int len;
    char *ptr = stringify(tcb, seed, &len);
    fl_set_random_seed(ptr, len);
    fl_assign(tcb, fl_nil, done);
}

void fl_file_exists_2(FL_TCB *tcb, FL_VAL fname, FL_VAL flag)
{
    char *fn = stringify(tcb, fname, NULL);
    struct stat sb;
    if(stat(fn, &sb) == -1) {
        if(errno == ENOENT || errno == ENOTDIR) {
            fl_assign(tcb, fl_false, flag);
            return;
        }
        fl_rt_error(tcb, fname, FL_IO_ERROR);
    }
    fl_assign(tcb, fl_true, flag);
}

void fl_chdir_2(FL_TCB *tcb, FL_VAL dir, FL_VAL done)
{
    char *fn = stringify(tcb, dir, NULL);
    chdir(fn);
    fl_assign(tcb, fl_nil, done);
}

void fl_getcwd_1(FL_TCB *tcb, FL_VAL dir)
{   
    char *buf = init_cbuf(tcb);
    if(getcwd(buf, INIT_CBUF) == NULL) 
        fl_rt_error(tcb, fl_nil, FL_IO_ERROR);
    str_to_list(tcb, buf, strlen(buf), dir, fl_nil);
}

void fl_getenv_2(FL_TCB *tcb, FL_VAL name, FL_VAL var)
{
    char *str = stringify(tcb, name, NULL);
    char *e = getenv(str);
    if(e == NULL) fl_assign(tcb, fl_nil, var);
    else str_to_list(tcb, e, strlen(e), var, fl_nil);    
}

void fl_file_type_2(FL_TCB *tcb, FL_VAL name, FL_VAL var)
{
    struct stat sb;
    char *fn = stringify(tcb, name, NULL);
    if(lstat(fn, &sb) == -1) io_error(tcb, var);
    else {
        FL_VAL t = MKINT(1);
        if(S_ISDIR(sb.st_mode)) t = MKINT(2);
        else if(S_ISLNK(sb.st_mode)) t = MKINT(3);
        else if(S_ISSOCK(sb.st_mode)) t = MKINT(4);
        else if(S_ISFIFO(sb.st_mode)) t = MKINT(5);
        fl_assign(tcb, t, var);
    }
}

void fl_file_size_2(FL_TCB *tcb, FL_VAL name, FL_VAL var)
{
    struct stat sb;
    char *fn = stringify(tcb, name, NULL);
    if(stat(fn, &sb) == -1) io_error(tcb, var);
    else fl_assign(tcb, MKINT(sb.st_size), var);
}

void fl_readlink_2(FL_TCB *tcb, FL_VAL name, FL_VAL var)
{
    char *buf = init_cbuf(tcb);
    char *fn = stringify(tcb, name, NULL);
    int len = readlink(fn, buf, INIT_CBUF);
    if(len == -1) io_error(tcb, var);
    else str_to_list(tcb, buf, len, var, fl_nil);
}

static FL_HTNODE *lookup(FL_VAL name, FL_HTNODE *root) 
{
    for(FL_HTNODE *n = root; n != NULL; n = n->next) {
        if(n->key == name) return n;
    }
    return NULL;
}

static int hash(FL_VAL name)
{
    return (long)name ^ ((long)name >> 16);
}

void fl_get_global_2(FL_TCB *tcb, FL_VAL name, FL_VAL var)
{
    CHECK_STRING(name);
    FL_VAL x = fl_get_global(tcb, name, NULL);
    if(x != NULL) {
        fl_unify_result(tcb, x, var);
        return;
    }
    fl_rt_error(tcb, name, FL_NO_GLOBAL);
}

void fl_get_global_3(FL_TCB *tcb, FL_VAL name, FL_VAL var, FL_VAL def)
{
    CHECK_STRING(name);
    FL_VAL x = fl_get_global(tcb, name, def);
    fl_unify_result(tcb, x, var);
}

void fl_put_global_2(FL_TCB *tcb, FL_VAL name, FL_VAL val)
{
    CHECK_STRING(name);
    fl_set_global(tcb, name, val);
}

void fl_put_global_3(FL_TCB *tcb, FL_VAL name, FL_VAL val, FL_VAL done)
{
    fl_put_global_2(tcb, name, val);
    fl_assign(tcb, fl_nil, done);
}

void fl_global_2(FL_TCB *tcb, FL_VAL name, FL_VAL val)
{
    FL_VAL old = fl_get_global(tcb, name, NULL);
    if(old == NULL) {
        old = mkvar(tcb);
        fl_set_global(tcb, name, old);
    }
    fl_unify_result(tcb, old, val);
}

void fl_delete_file_2(FL_TCB *tcb, FL_VAL name, FL_VAL done)
{
    char *fn = stringify(tcb, name, NULL);
    unlink(fn);
    fl_assign(tcb, fl_nil, done);
}

void fl_rmdir_2(FL_TCB *tcb, FL_VAL name, FL_VAL done)
{
    char *fn = stringify(tcb, name, NULL);
    if(rmdir(fn) == -1) {
        if(errno != ENOENT) {
            io_error(tcb, done);
            return;
        }
    }
    fl_assign(tcb, fl_nil, done);
}

void fl_chmod_3(FL_TCB *tcb, FL_VAL name, FL_VAL mode, FL_VAL done)
{
    CHECK_INT(mode);
    char *fn = stringify(tcb, name, NULL);
    if(chmod(fn, INT(mode)) == -1) {
        if(errno != ENOENT) {
            io_error(tcb, done);
            return;
        }
    }
    fl_assign(tcb, fl_nil, done);
}

void fl_idle_peer_1(FL_TCB *tcb, FL_VAL peer)
{
    int n = fl_nthreads();
    unsigned int rnd = tcb->ticks + rand() & 0xffff;
    for(int i = 0; i < n; ++i) {
        int j = (i + rnd) % n;
        if(tcb->ordinal != fl_tcbs[ j ].ordinal && fl_tcbs[ j ].state == FL_IDLE) {
            fl_assign(tcb, MKINT(fl_tcbs[ j ].ordinal), peer);
            return;
        }
    }
    fl_assign(tcb, fl_false, peer);
}

void fl_new_task_3(FL_TCB *tcb, FL_VAL env, FL_VAL st, FL_VAL done)
{
    FL_VAL task = mkcell(tcb, FL_TASK_TAG | 1, mklist(tcb, env, tcb->goal->task), st);
    unref(tcb, tcb->goal->task);
    tcb->goal->task = task;
    fl_assign(tcb, fl_nil, done);
}

void fl_new_task_2(FL_TCB *tcb, FL_VAL st, FL_VAL done)
{
    FL_VAL task = tcb->goal->task;
    FL_VAL env = fl_nil;
    if(task != fl_nil) env = CAR(CAR(task));
    fl_new_task_3(tcb, env, st, done);
}

void fl_set_task_1(FL_TCB *tcb, FL_VAL task)
{
    assert(tcb->goal->task == fl_nil);
    if(TAG(task) == FL_RTASK_TAG) addref(task);
    tcb->goal->task = task;
}

void fl_environment_1(FL_TCB *tcb, FL_VAL var)
{
    FL_VAL task = tcb->goal->task;
    FL_VAL env = fl_nil;
    if(task != fl_nil) env = CAR(CAR(task));
    fl_unify_result(tcb, env, var);
}

void fl_task_info_3(FL_TCB *tcb, FL_VAL task, FL_VAL env, FL_VAL rptr)
{
    fl_assign(tcb, CAR(CAR(task)), env);
    fl_assign(tcb, mkrptr(tcb, task), rptr);
}

void fl_remote_task_3(FL_TCB *tcb, FL_VAL env, FL_VAL rptr, FL_VAL task)
{
    if(RPTR_ID(rptr) == tcb->ordinal) {
        fl_assign(tcb, RPTR(rptr), task);
        return;
    }
    fl_assign(tcb, mkcell(tcb, FL_RTASK_TAG, mklist(tcb, env, fl_nil), rptr), task);
}

static int kill_process(FL_TCB *tcb, FL_VAL sig, FL_VAL pid) 
{
    int s;
    CHECK_INT(pid);
    if(ISSTRING(sig)) {
        s = fl_resolve_signal(STRING(sig));
        if(s < 0) fl_rt_error(tcb, sig, FL_BAD_SIGNAL);
    } else if(ISINT(sig)) s = INT(sig);
    else fl_rt_error(tcb, sig, FL_BAD_SIGNAL);
    return kill(INT(pid), s);
}

void fl_kill_3(FL_TCB *tcb, FL_VAL sig, FL_VAL pid, FL_VAL done)
{
    int r = kill_process(tcb, sig, pid);
    if(r < 0) io_error(tcb, done);
    else fl_assign(tcb, fl_nil, done);
}

void fl_kill_2(FL_TCB *tcb, FL_VAL sig, FL_VAL pid)
{
    kill_process(tcb, sig, pid);
}

void fl_struct_ref_4(FL_TCB *tcb, FL_VAL na, FL_VAL i, FL_VAL s, FL_VAL var)
{
    CHECK_TUPLE(s);
    TUPLE(na, f1, l1, args);
    LIST(args, name, more);
    int len = INT(CAR(more));
    int p = INT(i) - 1;
    if(deref(CAR(s)) != name || p < 0 || p >= len) 
        fl_rt_error2(tcb, s, name, FL_BAD_STRUCT);
    do s = deref(CDR(s)); while(p--);
    fl_unify_result(tcb, CAR(s), var);
}

void fl_struct_modify_4(FL_TCB *tcb, FL_VAL na, FL_VAL i, FL_VAL x, FL_VAL more)
{
    LIST(more, s, var);
    CHECK_TUPLE(s);
    TUPLE(na, f1, l1, args);
    LIST(args, name, arity);
    int len = INT(CAR(deref(arity)));
    int p = INT(i) - 1;
    FL_VAL new, *prev = &new;
    if(deref(CAR(s)) != name || p < 0 || p >= len) 
        fl_rt_error2(tcb, s, name, FL_BAD_STRUCT);
    for(int j = 0; j < len; ++j) {
        s = deref(CDR(s));
        FL_VAL y = p == j ? x : deref(CAR(s));
        *prev = addref(mklist(tcb, y, fl_nil));
        prev = &CDR(*prev);
    }
    fl_unify_result(tcb, mktuple(tcb, deref(name), len, new), var);
    unref(tcb, new);
}

void fl_statistics_1(FL_TCB *tcb, FL_VAL lst) 
{
    FL_VAL l = mklist(tcb, MKINT(tcb->used), fl_nil);
    l = mklist(tcb, MKINT(tcb->suspended), l);
    l = mklist(tcb, MKINT(tcb->active), l);
    fl_unify_result(tcb, mktuple(tcb, MKINT(tcb->goals), 3, l), lst);
}

void fl_heap_statistics_1(FL_TCB *tcb, FL_VAL lst) 
{
    long data[ FL_HEAP_STAT_COUNT ];
    fl_heap_statistics(tcb, data);
    FL_VAL l = mklist(tcb, MKINT(data[ 4 ]), fl_nil);
    l = mklist(tcb, MKINT(data[ 3 ]), l);
    l = mklist(tcb, MKINT(data[ 2 ]), l);
    l = mklist(tcb, MKINT(data[ 1 ]), l);
    fl_unify_result(tcb, mktuple(tcb, MKINT(data[ 0 ]), 4, l), lst);
}

void fl_dump_heap_2(FL_TCB *tcb, FL_VAL fname, FL_VAL done)
{
    char *n = stringify(tcb, fname, NULL);
    fl_dump_heap_graph(n);
    fl_assign(tcb, fl_nil, done);
}

void fl_unify_with_occurs_check_3(FL_TCB *tcb, FL_VAL x, FL_VAL y, FL_VAL r)
{
    int oc = tcb->occurs_check;
    tcb->occurs_check = 1;
    FL_VAL z = fl_unify_safe1(tcb, x, y);
    tcb->occurs_check = oc;
    fl_assign(tcb, z, r);
}

void fl_copy_block_4(FL_TCB *tcb, FL_VAL from, FL_VAL to, FL_VAL n, FL_VAL w)
{
    CHECK_INT(from);
    CHECK_INT(to);
    CHECK_INT(n);
    char *buf = init_cbuf(tcb);
    for(;;) {
        int r = read(INT(from), buf, INT(n));
        if(r == -1) {
            if(errno != EINTR) 
                fl_rt_error(tcb, from, FL_IO_ERROR);
            continue;
        }
        int wr = r; 
        while(wr > 0) {
            int n = write(INT(to), buf, r);
            if(n == -1) {
                if(errno != EINTR)
                    fl_rt_error(tcb, to, FL_IO_ERROR);
                continue;
            }
            wr -= n;
        }
        fl_assign(tcb, MKINT(r), w);
        return;
    }
}

void fl_mkdir_2(FL_TCB *tcb, FL_VAL name, FL_VAL done)
{
    char *n = stringify(tcb, name, NULL);
    if(mkdir(n, 0755) == -1) {
        if(errno == EEXIST) {
            struct stat sb;
            if(stat(n, &sb) != -1 && S_ISDIR(sb.st_mode)) goto ok;
        }
        fl_rt_error(tcb, name, FL_IO_ERROR);
    }
ok:
    fl_assign(tcb, fl_nil, done);
}

void fl_opendir_2(FL_TCB *tcb, FL_VAL name, FL_VAL dir)
{
    char *n = stringify(tcb, name, NULL);
    DIR *d = opendir(n);
    if(d == NULL) io_error(tcb, dir);
    else fl_assign(tcb, MKINT(d), dir);
}

void fl_readdir_2(FL_TCB *tcb, FL_VAL dir, FL_VAL entry)
{
    struct dirent *de = readdir((DIR *)POINTER(dir));
    if(de == NULL) fl_assign(tcb, fl_nil, entry);
    else str_to_list(tcb, de->d_name, de->d_namlen, entry, fl_nil);
}

void fl_closedir_1(FL_TCB *tcb, FL_VAL dir)
{
    closedir((DIR *)POINTER(dir));
}
