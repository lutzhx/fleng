/* FLENG - Linux specific part of the runtime system */

#include "fleng.h"
#include "fleng-util.h"
#include <sys/types.h>
#include <sys/time.h>
#include <signal.h>
#include <sys/timerfd.h>
#include <sys/wait.h>
#include <string.h>
#include <stdint.h>
#include <pthread.h>
#include <poll.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>

#define MAX_EVENTS  32

static struct { char *name; int sig; } signals[] = {
  { "SIGHUP", 1 },
  { "SIGINT", 2 },
  { "SIGQUIT", 3 },
  { "SIGILL", 4 },
  { "SIGTRAP", 5 },
  { "SIGABRT", 6 },
  { "SIGIOT", 6 },
  { "SIGFPE", 8 },
  { "SIGKILL", 9 },
  { "SIGSEGV", 11 },
  { "SIGPIPE", 13 },
  { "SIGALRM", 14 },
  { "SIGTERM", 15 },
  { "SIGTTIN", 21 },
  { "SIGTTOU", 22 },
  { "SIGXCPU", 24 },
  { "SIGXFSZ", 25 },
  { "SIGVTALRM", 26 },
  { "SIGPROF", 27 },
  { "SIGWINCH", 28 },
  { "SIGBUS", 7 },
  { "SIGUSR1", 10 },
  { "SIGCHLD", 17 },
  { "SIGCONT", 18 },
  { "SIGIO", 29 },
  { "SIGSTOP", 19 },
  { "SIGSYS", 31 },
  { "SIGTSTP", 20 },
  { "SIGURG", 23 },
  { "SIGUSR2", 12 },
  { "SIGSTKSZ", 0x2000 },
  { "SIGPOLL", 29 },
  { "SIGPWR", 30 },
  { "SIGSTKFLT", 16 },
  { "SIGUNUSED", 31 },
  { "SIGRTMAX", 0 },
  { "SIGRTMIN", 0 },
  { NULL, 0 }
};

struct child {
    pid_t pid;
    FL_TCB *tcb;
    FL_VAL var;
    int running;
    int status;
    struct child *next;
};

struct handler {
    int sig;
    FL_TCB *tcb;
    FL_VAL stream;
    int count;
    struct handler *next;
};

static int signal_pipe[ 2 ];
static pthread_mutex_t child_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t handler_mutex = PTHREAD_MUTEX_INITIALIZER;
static struct child *children;
static struct handler *handlers;
static pthread_t handler_thread;

int fl_resolve_signal(char *name) 
{
    for(int i = 0; signals[ i ].name != NULL; ++i) {
        if(!strcmp(signals[ i ].name, name)) 
            return(signals[ i ].sig);
    }
    return -1;
}

static int exit_status(int status) 
{
    if(WIFSIGNALED(status)) return -WTERMSIG(status);
    if(WIFSTOPPED(status)) return -WSTOPSIG(status);
    return WEXITSTATUS(status);
}

static void wait_for_children(void) 
{
    for(;;) {
        int s;
        pid_t w = waitpid(-1, &s, WNOHANG);
        if(w == -1) {
            if(errno == ECHILD) {
                pthread_mutex_lock(&child_mutex);
                for(struct child *c = children; c != NULL; c = c->next)
                    c->running = 0;
                pthread_mutex_unlock(&child_mutex);
            }
            return;
        }
        if(w <= 0) break;
        pthread_mutex_lock(&child_mutex);
        for(struct child *c = children; c != NULL; c = c->next) {
            if(c->pid == w) {
                c->running = 0;
                c->status = exit_status(s);
            }
        }
        pthread_mutex_unlock(&child_mutex);
    }
}

static void *signal_handler(void *data)
{
    siginfo_t info;
    sigset_t ss;
    sigfillset(&ss);
    sigdelset(&ss, SIGPROF);
    for(;;) {
        int s = sigwaitinfo(&ss, &info);
        if(s > 0) {
            if(s == SIGCHLD) wait_for_children();
            else {
                int found = 0;
                pthread_mutex_lock(&handler_mutex);
                for(struct handler *h = handlers; h != NULL; h = h->next) {
                    if(h->sig == s) {
                        found = 1;
                        ++h->count;
                    }
                }
                pthread_mutex_unlock(&handler_mutex);
                if(!found) {
                    switch(s) {
                    case SIGINT: fl_terminate(2);
                    case SIGTERM: fl_terminate(3);
                    }
                }
            }
            write(signal_pipe[ 1 ], "x", 1);
        }
    }
}

void fl_init_signals(void)
{
    sigset_t ss;
    sigfillset(&ss);
    sigdelset(&ss, SIGPROF);
    pthread_sigmask(SIG_BLOCK, &ss, NULL);
    children = NULL;
    handlers = NULL;
    pipe(signal_pipe);
    fcntl(signal_pipe[ 0 ], F_SETFL, O_NONBLOCK);
    fcntl(signal_pipe[ 1 ], F_SETFL, O_NONBLOCK);
    pthread_create(&handler_thread, NULL, signal_handler, NULL);
}

int fl_create_event_queue(FL_TCB *tcb)
{
    tcb->pollfds = (struct pollfd *)malloc(sizeof(struct pollfd) * MAX_EVENTS);
    return 0;
}

static void wake_children(FL_TCB *tcb)
{
    pthread_mutex_lock(&child_mutex);
    struct child **prev = &children;
    for(struct child *c = children; c != NULL; c = c->next) {
        if(c->tcb == tcb && !c->running) {
            fl_assign(tcb, MKINT(c->status), c->var);
            /* var is kept in listeners, no need to unref */
            *prev = c->next;
            free(c);
        } else prev = &c->next;
    }
    pthread_mutex_unlock(&child_mutex);
}

static void assign_tail(FL_TCB *tcb, FL_VAL lrec, FL_VAL val) 
{
    FL_VAL var = CAR(CDR(lrec));
    FL_VAL t = addref(fl_chase_tail(var));
    FL_VAL newt = mkvar(tcb);
    fl_assign(tcb, mklist(tcb, val, newt), t);
    unref(tcb, var);
    CAR(CDR(lrec)) = t;
}

static void wake_handlers(FL_TCB *tcb)
{
    pthread_mutex_lock(&handler_mutex);
    for(struct handler *h = handlers; h != NULL; h = h->next) {
        if(h->tcb == tcb && h->count) {
            FL_VAL t = addref(fl_chase_tail(h->stream));
            FL_VAL newt = mkvar(tcb);
            fl_assign(tcb, mklist(tcb, MKINT(h->count), newt), t);
            unref(tcb, h->stream);
            h->stream = t;
        } 
    }
    pthread_mutex_unlock(&handler_mutex);
}

/* unused - for debugging */
static void listeners(FL_TCB *tcb, char *msg)
{
    fprintf(stderr, "%s: ", msg);
    for(FL_VAL ll = tcb->listening; ll != fl_nil; ll = CDR(ll)) {
        FL_VAL lrec = CAR(ll);
        fprintf(stderr, "fd=%ld, t=%ld; ", INT(CAR(lrec)), INT(CAR(CDR(CDR(lrec)))));
    }
    fputs("\n", stderr);
}

static int update_events(FL_TCB *tcb, long ms)
{
    int nt = 0;
    struct pollfd *pollfds = tcb->pollfds;
    /* XXX re-use pollfds if listeners didn't change */
    for(FL_VAL ll = tcb->listening; ll != fl_nil; ll = CDR(ll)) {
        assert(nt < MAX_EVENTS);
        FL_VAL litem = CAR(ll);
        int fd = INT(CAR(litem));
        int e = INT(CAR(CDR(CDR(litem))));
        if(e != FL_CHILD && e != FL_SIGNAL) {
            pollfds[ nt ].fd = fd;
            pollfds[ nt ].events = POLLIN;
            ++nt;
        }
    }
    pollfds[ nt ].fd = signal_pipe[ 0 ];
    pollfds[ nt ].events = POLLIN;
    ++nt;
    int r = poll(pollfds, nt, ms);
    int intr = 0;
    if(r == -1) {
        if(errno != EINTR) fl_rt_error(tcb, fl_nil, FL_IO_ERROR);
        else intr = 1;
    }
    FL_VAL ll = tcb->listening; 
    FL_VAL *prev = &tcb->listening;
    int wchildren = 0, whandlers = 0;
    if(!intr && (pollfds[ nt - 1 ].revents & (POLLIN | POLLHUP)) != 0) {
        char dummy;
        read(signal_pipe[ 0 ], &dummy, 1);
    }
    --nt;
    while(ll != fl_nil) {
        FL_VAL lrec = CAR(ll);
        FL_VAL next = CDR(ll);
        int fd = INT(CAR(lrec));
        int event = INT(CAR(CDR(CDR(lrec))));
        FL_VAL var = CAR(CDR(lrec));
        if(event == FL_CHILD) {
            if(!wchildren) {
                wake_children(tcb);
                wchildren = 1;
            }
            if(CAR(var) != var) { /* bound by wake_children() */
                *prev = next;
                CDR(ll) = fl_nil;
                unref(tcb, ll);
            } else prev = &CDR(ll);
        } else if(event == FL_SIGNAL) {
            if(!whandlers) {
                wake_handlers(tcb);
                whandlers = 1;
            }
            prev = &CDR(ll);
        } else if(!intr) {
            int found = 0;
            for(int i = 0; i < nt; ++i) {
                int drop = 1;
                if(fd == pollfds[ i ].fd && 
                   (pollfds[ i ].revents & (POLLIN | POLLHUP)) != 0) {
                    uint64_t n = 0;
                    found = 1;
                    switch(event) {
                    case FL_TIMEOUT:
                        read(fd, &n, sizeof(uint64_t));
                        if(CAR(var) == var) fl_assign(tcb, fl_nil, var);
                        close(fd);
                        break;
                    case FL_CLOCK:
                        read(fd, &n, sizeof(uint64_t));
                        assign_tail(tcb, lrec, MKINT(n));
                        drop = 0;
                        break;
                    case FL_INPUT:
                        fl_assign(tcb, fl_nil, var);
                        break;
                    }
                    if(drop) {
                        *prev = next;
                        CDR(ll) = fl_nil;
                        unref(tcb, ll);
                    } 
                }
            }
            if(!found) prev = &CDR(ll);
        } else prev = &CDR(ll);
        ll = next;
    }
    if(intr) return 0;
    return r;
}

int fl_wait_for_events(FL_TCB *tcb, long ms)
{
    return update_events(tcb, ms);
}

int fl_add_event_to_queue(FL_TCB *tcb, int event, FL_VAL data, FL_VAL var, FL_VAL eid)
{
    /* listener record: [fd, var, event|data] */
    FL_VAL n1 = mklist(tcb, MKINT(event), data);
    n1 = mklist(tcb, var, n1);
    n1 = mklist(tcb, fl_nil, n1);
    FL_VAL n = mklist(tcb, n1, tcb->listening);
    unref(tcb, tcb->listening);
    tcb->listening = addref(n);
    sigset_t sigs;
    int fd;
    switch(event) {
    case FL_INPUT:  /* data: fd */
        fd = INT(data);
        break;
    case FL_SIGNAL: { /* data: signal */
        struct handler *h = (struct handler *)malloc(sizeof(struct handler));
        h->count = 0;
        h->tcb = tcb;
        h->sig = INT(data);
        h->stream = var;
        pthread_mutex_lock(&handler_mutex);
        h->next = handlers;
        handlers = h;
        pthread_mutex_unlock(&handler_mutex);
        fd = 0;
        break; }
    case FL_TIMEOUT:    /* data: milliseconds */
    case FL_CLOCK: {
        struct itimerspec it;
        fd = timerfd_create(CLOCK_MONOTONIC, TFD_CLOEXEC);
        if(fd < 0) fl_rt_error(tcb, data, FL_IO_ERROR);
        int ms = INT(data);
        it.it_value.tv_sec = ms / 1000;
        it.it_value.tv_nsec = (ms % 1000) * 1000000;
        if(event == FL_CLOCK) it.it_interval = it.it_value; 
        else it.it_interval.tv_sec = it.it_interval.tv_nsec = 0;
        if(timerfd_settime(fd, 0, &it, NULL) < 0) 
            fl_rt_error(tcb, MKINT(fd), FL_IO_ERROR);
        break; }
    case FL_CHILD: { /* data: pid */
        struct child *c = (struct child *)malloc(sizeof(struct child));
        c->running = 1;
        c->tcb = tcb;
        c->pid = INT(data);
        c->var = var;
        c->status = 99; /* in case waitpid fails with ECHILD */
        pthread_mutex_lock(&child_mutex);
        c->next = children;
        children = c;
        pthread_mutex_unlock(&child_mutex);
        fd = 0;
        break; }
    }
    if(fd == - 1) {
        tcb->listening = CDR(n);
        CDR(n) = fl_nil;
        unref(tcb, n); 
        fl_rt_error(tcb, data, FL_IO_ERROR);
        return 0;
    }
    CAR(n1) = MKINT(fd);
    if(eid != NULL) fl_assign(tcb, MKINT(fd), eid);
    return update_events(tcb, 0);
}

static FL_VAL find_listener(FL_TCB *tcb, FL_VAL id, int remove)
{
    FL_VAL *prev = &tcb->listening;
    FL_VAL ll = tcb->listening;
    while(ll != fl_nil) {
        FL_VAL lrec = CAR(ll);
        if(CAR(lrec) == id) {
            if(remove) {
                addref(lrec);
                *prev = CDR(ll);
                CDR(ll) = fl_nil;
                unref(tcb, ll);
            }
            return lrec;
        }
        prev = &CDR(ll);
        ll = CDR(ll);
    }
    return fl_nil;
}

int fl_cancel_timer(FL_TCB *tcb, FL_VAL id)
{
    FL_VAL *prev;
    FL_VAL lrec = find_listener(tcb, id, 1);
    if(lrec == fl_nil) return 0;
    FL_VAL var = CAR(CDR(lrec));
    if(INT(CAR(CDR(CDR(lrec)))) == FL_CLOCK) {
        FL_VAL st = fl_chase_tail(var);
        if(BITS(st) == 0 && TAG(st) == FL_VAR_TAG) 
            fl_assign(tcb, fl_nil, st);
    } else if(CAR(var) == var) fl_assign(tcb, fl_false, var);
    unref(tcb, lrec);
    close(INT(CAR(lrec)));
    return 1;
}

int fl_restart_timer(FL_TCB *tcb, FL_VAL id, int ms)
{
    FL_VAL *prev;
    FL_VAL lrec = find_listener(tcb, id, 0);
    if(lrec == fl_nil) return 0;
    struct itimerspec it;
    it.it_value.tv_sec = ms / 1000;
    it.it_value.tv_nsec = (ms % 1000) * 1000000;
    if(INT(CAR(CDR(CDR(lrec)))) == FL_CLOCK)
        it.it_interval = it.it_value; 
    else it.it_interval.tv_sec = it.it_interval.tv_nsec = 0;
    int fd = INT(CAR(lrec));
    if(timerfd_settime(fd, 0, &it, NULL) < 0) 
        fl_rt_error(tcb, MKINT(fd), FL_IO_ERROR);
    return 1;
}
