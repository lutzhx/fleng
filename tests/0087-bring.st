% distributed communication stress test:
% produce stream of C numbers and consume in next peer
% in ring of N nodes, spinning for a random amount of time
% between sending a number.

-initialization(main).
-machine(ring).

main :- spawn(10, 1000)@fwd.

spawn(0, _).
spawn(N, C) :-
    N > 0 |
    p(C, S), c(S, C)@fwd, 
    N2 is N - 1, spawn(N2, C)@fwd.

p(C, S) :- p(C, [], S).

p(0, _, S) :- S := [].
p(N, [], S) :-
    N > 0 | 
    RND is rnd(100),
    spin(RND, OK),
    S := [N|S2], N2 is N - 1, p(N2, OK, S2).

spin(0, OK) :- OK := [].
spin(N, OK) :- N > 0 | N2 is N - 1, spin(N2, OK).

c([], 0).
c([N|S], N) :- N2 is N - 1, c(S, N2).
