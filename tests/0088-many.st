% multiple streams merged from many nodes

-initialization(main).
-machine(ring).

main :- spawn(10, [], R), chk(R).

chk(10000).

spawn(0, SS, R) :- 
    merger(SS, O), consume(O, 0, R).
spawn(N, SS, R) :-
    N > 0 | 
    produce(1000, I),
    N2 is N - 1, spawn(N2, [merge(I)|SS], R)@fwd.

produce(0, I) :- I := [].
produce(N, I) :-
    N > 0 | 
    I := [N|I2],
    N2 is N - 1, 
    produce(N2, I2).

consume([], C, R) :- R := C.
consume([_|S], C, R) :-
    C2 is C + 1, consume(S, C2, R).
