/* FLENG - runtime system, C part */

#include "fleng.h"
#include "fleng-util.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdarg.h>
#include <unistd.h>
#include <errno.h>
#include <math.h>
#include <stdint.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <signal.h>
#include <ctype.h>
#ifdef __linux__
#include <alloca.h>
#endif
#include "version.h"

#if defined(__LP64__) || defined(_LP64)
#define MAX_THREADS     64
#else
#define MAX_THREADS     32
#endif
#define MAX_SSTACK      1024
#define STACK_RESERVE   2048
#define MAX_ENV         64
#define TIMESLICE       10
#define STAT_TICKS      250
#define MAX_DELAY       (100 * 256)
#define LOG_WRITE_LIMIT 10
#define PROFILE_SAMPLE_TIME 10
#define MAX_DUMP_SUSPENDED 32

/* XXX */
#define FUDGE           10

/* [SEND_ADDREF, Rptr(Owner, x)] */
#define SEND_ADDREF     1
/* [SEND_DROPREF, Rptr(Owner, x)] */
#define SEND_DROPREF    2
/* [SEND_SENDPORT, Rptr(Owner, Port)|x] */
#define SEND_SENDPORT   3
/* [SEND_READ, Rptr(Owner, Var)|NewVar] */
#define SEND_READ       4
/* [SEND_VALUE, Rptr(Owner, Var)|Val] */
#define SEND_VALUE      5
/* [SEND_CALL, ID, Term|Mod] */
#define SEND_CALL       6
/* [SEND_TCALL, ID, Task, Term|Mod] */
#define SEND_TCALL      7

#define HEAP_SIZE       5000000
#define MAX_GOALS       100000
#define T_STACK_SIZE    500000
#define TRAIL_BUFFER    2000
#define MAX_ARGV        256

#define WATERMARK       (FL_PORT_SIZE / 10)

#define PORT_LOCK(port)     ((int *)port)
#define PORT_SIZE(port)     (((int *)port) + 1)
#define PORT_SOURCE(port)   (((int *)port) + 2)
#define PORT_MSGID(port)    (((int *)port) + 3)
/* slot #4 (data area): aligned to 16 bytes (32 bit system): */
#define PORT_DATA(port)     ((port) + 4 * sizeof(int))

typedef struct exit_handler {
    void (*proc)(int);
    struct exit_handler *next;
} EXIT_HANDLER;

FL_VAL fl_nil, fl_true, fl_false, fl_colon, fl_error, fl_slash, sys_name;
FL_TCB fl_tcbs[ MAX_THREADS ];
static int ntcbs = 0, logging = 0, loggingx = 0;
static char *strings = NULL;
char *fl_argv[ MAX_ARGV ];
static FILE *logfile;
static int stats = 0, heapstats = 0, stat_ticks = STAT_TICKS;
static int threads, active_threads, occurs_check, dump_on_error;
static volatile int idle_threads, msg_tally;
static FL_MODULE *last_module = NULL;
static int timeslice = TIMESLICE;
static EXIT_HANDLER *atexits = NULL;
static char *profile = NULL;
static long *yield_counters;

static pthread_mutex_t log_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t intern_mutex = PTHREAD_MUTEX_INITIALIZER;

#ifdef XLOGGING
#define XDBG 1
#else
#define XDBG 0
#endif

extern char fl_cmdline[];

void fl_atexit(void (*proc)(int))
{
    EXIT_HANDLER *h = (EXIT_HANDLER *)malloc(sizeof(EXIT_HANDLER));
    h->next = atexits;
    h->proc = proc;
    atexits = h;
}

#ifdef ENABLE_PROFILING
static void write_profile(char *fname)
{
    FILE *fp = fopen(fname, "w");
    if(fp == NULL) {
        fprintf(stderr, "unable to write %s\n", fname);
        exit(1);
    }
    long *totals = alloca(threads * sizeof(long));
    memset(totals, 0, sizeof(long) * threads);
    for(FL_MODULE *m = last_module; m != NULL; m = m->next) {
        for(FL_PROFINFO *p = m->profinfo; p != NULL; p = p->next)
            for(int i = 0; i < threads; ++i)
                totals[ i ] += p->counters[ i ];
    }
    for(int i = 0; i < threads; ++i)
        totals[ i ] += yield_counters[ i ];
    fprintf(fp, "%-32s", "");
    for(int i = 0; i< threads; ++i) fprintf(fp, "\t%17d     \t", i + 1);
    fputs("\n\n", fp);
    for(FL_MODULE *m = last_module; m != NULL; m = m->next) {
        for(FL_PROFINFO *p = m->profinfo; p != NULL; p = p->next) {
            int f = 0;
            for(int i = 0; i < threads; ++i) {
                long n = p->counters[ i ];
                if(n > 0) {
                    if(!f) {
                        fprintf(fp, "%-32s", p->goalname);
                        f = 1;
                    }
                    double t = n * PROFILE_SAMPLE_TIME / 1000.0;
                    fprintf(fp, "\t%8.3fs (%6ld)\t", t, n);
                    int p = (n * 100) / totals[ i ];
                    fprintf(fp, "%3d%%", p);
                }
            }
            if(f) fputc('\n', fp);
        }
    }
    fprintf(fp, "\n%-32s", "(yield/listen)");
    for(int i = 0; i < threads; ++i) {
        long n = yield_counters[ i ];
        double t = n * PROFILE_SAMPLE_TIME / 1000.0;
        fprintf(fp, "\t%8.3fs (%6ld)\t", t, n);
        int p = n > 0 ? (n * 100) / totals[ i ] : 0;
        fprintf(fp, "%3d%%", p);
    }
    fprintf(fp, "\n\n%-32s", "(total)");
    for(int i = 0; i < threads; ++i)
        fprintf(fp, "\t%8.3fs         \t    ", 
            totals[ i ] * PROFILE_SAMPLE_TIME / 1000.0);
    fputc('\n', fp);
    fclose(fp);
}
#endif

static void dump_heap_value(FILE *fp, FL_TCB *tcb, FL_VAL x, char *desc, char *str)
{
    char text[ 256 ];
    if(ISINT(x)) sprintf(text, "%s=%ld", desc, INT(x));
    else if(ISSTRING(x)) sprintf(text, "%s='%s'", desc, STRING(x));
    else sprintf(text, "%s=%p", desc, x);
    strcat(str, text);
}

static void dump_heap_element(FILE *fp, FL_TCB *tcb, FL_VAL x)
{
    char *c;
    char label[ 256 ];
    char t;
    *label = '\0';
    int fa = 0, fd = 0;
    switch(TAG(x) & FL_TAG_MASK) {
    case FL_REF_TAG:
        c = "Orange";
        t = 'r';
        sprintf(label, "ref@%ld", RPTR_ID(CAR(x)));
        fprintf(fp, "a%p -> a%p\n", x, RPTR(CAR(x)));
        break;
    case FL_VAR_TAG:
        c = "Goldenrod";            
        t = 'v';
        fa = fd = 1;
        break;
    case FL_LIST_TAG: 
        c = "SkyBlue";
        t = 'l';
        fa = fd = 1;
        break;
    case FL_FLOAT_TAG: 
        sprintf(label, "%g", FLOATVAL(x));
        c = "Aquamarine";
        t = 'f';
        break;
    case FL_PORT_TAG: 
        if(((long)CAR(x) & FL_INT_BIT) != 0) {
            sprintf(label, "@%ld", RPTR_ID(CAR(x)));
            fprintf(fp, "a%p -> a%p\n", x, RPTR(CAR(x)));
        } 
        t = 'p';
        c = "Salmon";
        break;
    default:
        if((TAG(x) & FL_TUPLE_BIT) != 0) {
            t = 't';
            c = "PaleGreen";
            sprintf(label, "#%ld", TUPLE_LENGTH(x));
            fa = fd = 1;
        } else {
            c = "white";
            t = '_';
        }
    }
    if(fa) {
        if(BITS(CAR(x)) == 0) fprintf(fp, "a%p -> a%p [label=car];\n", x, CAR(x));
        else dump_heap_value(fp, tcb, CAR(x), ",car", label);
    }
    if(fd) {
        if(BITS(CDR(x)) == 0) fprintf(fp, "a%p -> a%p [label=cdr];\n", x, CDR(x));
        else dump_heap_value(fp, tcb, CDR(x), ",cdr", label);
    }
    fprintf(fp, "a%p[style=filled, shape=box, color=%s, label=\"%c:(", x, c, t);
    if(COUNT(x) == FL_COUNT_MASK) fputs("_", fp);
    else fprintf(fp, "%ld", COUNT(x));
    fprintf(fp, ")%p@%ld:%s\"];\n", x, tcb->ordinal, label);
}

static void dump_tcb_ref(FILE *fp, FL_TCB *tcb, int ref, char *desc, FL_VAL x)
{
    if(BITS(x) == 0 && x != NULL && COUNT(x) != 0) {
        fprintf(fp, "t%d_%ld[label=\"@%ld:%s\"];", ref, tcb->ordinal, tcb->ordinal, desc);
        fprintf(fp, "t%d_%ld -> a%p;\n", ref, tcb->ordinal, x);
    }
}

void fl_dump_heap_graph(char *filename)
{
    FILE *fp = fopen(filename, "w");
    fprintf(fp, "digraph {\n");
    for(int i = 0; i < threads; ++i) {
        FL_TCB *tcb = fl_tcbs + i;
        for(FL_CELL *p = tcb->heap; p < tcb->heapend; ++p) {
            if((p->tag & FL_COUNT_MASK) != 0)
                dump_heap_element(fp, tcb, (FL_VAL)p);
        }
        dump_tcb_ref(fp, tcb, 1, "idle", tcb->idle);
        dump_tcb_ref(fp, tcb, 2, "events", tcb->events);
        dump_tcb_ref(fp, tcb, 3, "listening", tcb->listening);
    }
    fputs("}\n", fp);
    fclose(fp);
}

static void dump_suspended(FL_TCB *tcb) 
{
    int c = 0;
    for(int i = 1; i < tcb->max_goals; ++i) {
        if(c > MAX_DUMP_SUSPENDED) {
            fputs("  ...\n", stderr);
            break;
        }
        if(tcb->goal_buffer[ i ].info != NULL) {
            fprintf(stderr, "  %s %p\n",
                tcb->goal_buffer[ i ].info,
                tcb->goal_buffer[ i ].addr);
            ++c;
        }
    }
}

void fl_terminate(int code)
{
    for(EXIT_HANDLER *h = atexits; h != NULL; h = h->next)
        h->proc(code);
    for(int i = 0; i < threads; ++i) {
        if(fl_tcbs[ i ].detached) {
            munmap(fl_tcbs[ i ].port, FL_PORT_SIZE);
            unlink(fl_tcbs[ i ].mportfile);
        }
    }
    if(code != 0 && dump_on_error) {
        char buf[ 32 ];
        sprintf(buf, "heap.%d.dot", getpid()); 
        fl_dump_heap_graph(buf);
    }
#ifdef ENABLE_PROFILING
    if(code == 0 && profile != NULL) write_profile(profile);
#endif
    fflush(logfile);
    exit(code);
}

int fl_nthreads(void) { return threads; }

void fl_write(FL_TCB *tcb, FILE *fp, FL_VAL x)
{
    x = deref(x);
    if(((long)x & FL_INT_BIT) != 0)
        fprintf(fp, "%ld", (long)x >> 1);
    else if(((long)x & FL_ATOM_BIT) != 0)
        fputs((char *)x + 1, fp);
    else {
        FL_CELL *c = (FL_CELL *)x;
        unsigned long tag = c->tag;
        if(c->car == (FL_VAL)FL_BROKEN_HEART) {
            fprintf(fp, "<RECLAIMED %p>", x);
            return;
        }
        if((tag & FL_TUPLE_BIT) != 0) {
            FL_VAL x1 = deref(c->car);
            int a = (tag >> 24) & 0x7f;
            if(a > 0 && ((long)x1 & FL_BITS_MASK) == FL_ATOM_BIT &&
                x1 != fl_nil) {
                fl_write(tcb, fp, x1);
                fputc('(', fp);
                x = c->cdr;
                fl_write(tcb, fp, CAR(x));
                for(x1 = CDR(x); x1 != fl_nil; x1 = deref(CDR(x1))) {
                    fputs(", ", fp);
                    fl_write(tcb, fp, CAR(x1));
                    if(tcb->write_limit > -1) {
                        if(tcb->write_limit == 0) {
                            if(CDR(x1) != fl_nil) fputs("...", fp);
                            break;
                        } else --tcb->write_limit;
                    }
                }
                fputc(')', fp);
            } else {
                fputc('{', fp);
                fl_write(tcb, fp, x1);
                x1 = deref(c->cdr); 
                while(x1 != fl_nil) {
                    fputs(", ", fp);
                    fl_write(tcb, fp, CAR(x1));
                    x1 = deref(CDR(x1));
                    if(tcb->write_limit > -1) {
                        if(tcb->write_limit == 0) {
                            if(x1 != fl_nil) fputs("...", fp);
                            break;
                        } else --tcb->write_limit;
                    }
                }
                fputc('}', fp);
            }
        } else {
            switch(tag & FL_TAG_MASK) {
            case FL_FLOAT_TAG:
                fprintf(fp, "%.*g", 15, *((double *)(&c->car)));
                break;
            case FL_LIST_TAG:
                fputc('[', fp);
                fl_write(tcb, fp, c->car);
                x = deref(c->cdr); 
                while(((long)x & FL_BITS_MASK) == 0 && TAG(x) == FL_LIST_TAG) {
                    fputs(", ", fp);
                    fl_write(tcb, fp, CAR(x));
                    x = deref(CDR(x));
                    if(tcb->write_limit > -1) {
                        if(tcb->write_limit == 0) {
                            if(x != fl_nil) fputs("...", fp);
                            x = fl_nil;
                            break;
                        } else --tcb->write_limit;
                    }
                }
                if(x != fl_nil) {
                    fputc('|', fp);
                    fl_write(tcb, fp, x);
                }
                fputc(']', fp);
                break;
            case FL_VAR_TAG:                
                fprintf(fp, "_%lu", (unsigned long)x - (unsigned long)tcb->heap);
                break;
            case FL_PORT_TAG:
                if(((long)c->car & FL_INT_BIT) != 0)
                    fprintf(fp, "<port %p@%ld>", RPTR(c->car), RPTR_ID(c->car));
                else fprintf(fp, "<port %p>", c);
                break;
            case FL_MODULE_TAG:
                fprintf(fp, "<module '%s'>", 
                    (char *)((FL_MODULE *)UNMARK(c->car))->name + 1);
                break;
            case FL_TASK_TAG:
                fprintf(fp, "<task %p>", c);
                break;
            case FL_RTASK_TAG:
                fprintf(fp, "<task %p@%ld>", RPTR(c->cdr), RPTR_ID(c->cdr));
                break;
            case FL_REF_TAG: {
                x = CAR(x);
                int id = RPTR_ID(x);
                fprintf(fp, "_%lu_%d", 
                    (unsigned long)RPTR(x) - (unsigned long)fl_tcbs[ id - 1 ].heap, 
                    id);
                break; }
            default:
                fprintf(fp, "<unknown cell with tag 0x%08lx>", tag);
                break;
            }
        }
    }
}

static void write_limited(FL_TCB *tcb, FILE *fp, FL_VAL x)
{
    int lim = tcb->write_limit;
    tcb->write_limit = LOG_WRITE_LIMIT;
    fl_write(tcb, fp, x);
    tcb->write_limit = lim;
}

void fl_logfmt(FL_TCB *tcb, char *fstr, ...)
{
    va_list va;
    va_start(va, fstr);
    char *info = NULL;
    if(tcb->goal != NULL) info = tcb->goal->info;
    if(info == NULL) info = "<none>";
    pthread_mutex_lock(&log_mutex);
    fprintf(logfile, "<%ld> %ld: %s ", tcb->ordinal, tcb->ticks, info);
    vfprintf(logfile, fstr, va);
    fflush(logfile);
    pthread_mutex_unlock(&log_mutex);
    va_end(va);    
}

void fl_log_2(FL_TCB *tcb, FL_VAL msg, FL_VAL done)
{
    if(tcb->logging || tcb->loggingx) {
        pthread_mutex_lock(&log_mutex);
        FL_GOAL *g = tcb->goal;
        fprintf(logfile, "<%ld> %ld: ", tcb->ordinal, tcb->ticks);
        write_limited(tcb, logfile, msg);
        fputc('\n', logfile);
        fflush(logfile);
        pthread_mutex_unlock(&log_mutex);
    }
    fl_assign(tcb, fl_nil, done);
}

void fl_log_entry(FL_TCB *tcb, long arity)
{
    if(tcb->logging) {
        pthread_mutex_lock(&log_mutex);
        FL_GOAL *g = tcb->goal;
        fprintf(logfile, "<%ld> %ld: %s ", tcb->ordinal, tcb->ticks, g->info);
        int i;
        for(i = 0; i < arity; ++i) {
            write_limited(tcb, logfile, g->args[ i ]);
            fputc(' ', logfile);
            if(i == 2 && arity > 4) {
                for(FL_VAL a = g->args[ 3 ]; 
                    a != fl_nil; 
                    a = ((FL_CELL *)a)->cdr) {
                    write_limited(tcb, logfile, ((FL_CELL *)a)->car);
                    fputc(' ', logfile);
                }
                break;
            }
        }
        fputc('\n', logfile);
        fflush(logfile);
        pthread_mutex_unlock(&log_mutex);
    }
}

static void statistics(FL_TCB *tcb)
{
    ++tcb->scounts;
    tcb->average = (tcb->used + tcb->scounts * tcb->average) / tcb->scounts;
    if(tcb->used > tcb->peak) tcb->peak = tcb->used;
    pthread_mutex_lock(&log_mutex);
    fprintf(logfile, "## %ld %ld G %ld Q %ld S %ld C %ld A %ld P %ld\n", 
        tcb->ordinal, 
        tcb->ticks, tcb->goals, tcb->active, tcb->suspended, tcb->used,
        tcb->average, tcb->peak);
    fflush(logfile);
    pthread_mutex_unlock(&log_mutex);
}

void fl_heap_statistics(FL_TCB *tcb, long *data)
{
    for(int i = 0; i < FL_HEAP_STAT_COUNT; ++i) 
        data[ i ] = 0;
    for(FL_CELL *p = tcb->heap; p < tcb->heapend; ++p) {
        switch(p->tag & FL_TAG_MASK) {
        case FL_REF_TAG:
        case FL_VAR_TAG: ++data[ 0 ]; break;
        case FL_LIST_TAG: ++data[ 2 ]; break;
        case FL_FLOAT_TAG: ++data[ 3 ]; break;
        case FL_PORT_TAG: ++data[ 4 ]; break;
        default:
            if((p->tag & FL_TUPLE_BIT) != 0) ++data[ 1 ];
        }
    }
    for(int i = 0; i < FL_HEAP_STAT_COUNT; ++i) 
        data[ i ] *= sizeof(FL_CELL);
}

static void heap_statistics(FL_TCB *tcb) 
{
    long data[ FL_HEAP_STAT_COUNT ];
    fl_heap_statistics(tcb, data);
    pthread_mutex_lock(&log_mutex);
    fprintf(logfile, "#$ %ld %ld T %ld L %ld V %ld F %ld P %ld\n", 
        tcb->ordinal, tcb->ticks,
        data[ 1 ] / 1024, data[ 2 ] / 1024, data[ 0 ] / 1024, data[ 3 ] / 1024,
        data[ 4 ] / 1024);
    fflush(logfile);
    pthread_mutex_unlock(&log_mutex);
}

void fl_rt_error(FL_TCB *tcb, FL_VAL x, int error)
{
    fprintf(stderr, "\n*** ERROR: <%ld> %ld: %s - ", tcb->ordinal, tcb->ticks, 
        tcb->goal->info);
    switch(error) {
    case FL_NOT_AN_ATOM:
        fputs("expected atom but received: ", stderr);
        write_limited(tcb, stderr, x);
        break;
    case FL_NOT_A_CELL:
        fputs("expected cell but received: ", stderr);
        write_limited(tcb, stderr, x);
        break;
    case FL_NOT_AN_INT:
        fputs("expected integer but received: ", stderr);
        write_limited(tcb, stderr, x);
        break;
    case FL_NOT_A_LIST:
        fputs("expected list but received: ", stderr);
        write_limited(tcb, stderr, x);
        break;
    case FL_NOT_A_TUPLE:
        fputs("expected tuple but received: ", stderr);
        write_limited(tcb, stderr, x);
        break;
    case FL_NOT_A_VAR:
        fputs("expected variable but received: ", stderr);
        write_limited(tcb, stderr, x);
        break;
    case FL_NOT_A_NUMBER:
        fputs("expected number but received: ", stderr);
        write_limited(tcb, stderr, x);
        break;
    case FL_NOT_A_MODULE:
        fputs("expected module but received: ", stderr);
        write_limited(tcb, stderr, x);
        break;
    case FL_QUEUE_FULL:
        fputs("goal queue overflow", stderr);
        break;
    case FL_NO_MODULE:
        fputs("module not linked: ", stderr);
        write_limited(tcb, stderr, x);
        break;
    case FL_NO_CELLS:
        fputs("out of memory", stderr);
        break;
    case FL_DIV_BY_ZERO:
        fputs("division by zero", stderr);
        break;
    case FL_CANT_CONVERT:
        fputs("can not convert value: ", stderr);
        write_limited(tcb, stderr, x);
        break;
    case FL_NOT_A_STRING:
        fputs("expected string or character list: ", stderr);
        write_limited(tcb, stderr, x);
        break;
    case FL_IO_ERROR:
        fprintf(stderr, "I/O error - %s: ", strerror(errno));
        write_limited(tcb, stderr, x);
        break;
    case FL_NOT_A_PORT:
        fputs("expected port: ", stderr);
        write_limited(tcb, stderr, x);
        break;
    case FL_PORT_CLOSED:
        fputs("sent value to closed port: ", stderr);
        write_limited(tcb, stderr, x);
        break;
    case FL_BAD_INDEX:
        fputs("index out of range: ", stderr);
        write_limited(tcb, stderr, x);
        break;
    case FL_CANT_FORWARD:
        fputs("unable to forward object: ", stderr);
        write_limited(tcb, stderr, x);
        break;
    case FL_CANT_UNFORWARD:
        fputs("unable to unforward object", stderr);
        break;
    case FL_BAD_PEER:
        fputs("invalid peer: ", stderr);
        write_limited(tcb, stderr, x);
        break;
    case FL_OUT_OF_MEMORY:
        fputs("out of memory", stderr);
        break;
    case FL_BAD_SIGNAL:
        fputs("invalid signal: ", stderr);
        write_limited(tcb, stderr, x);
        break;
    case FL_TOO_MANY_GOALS:
        fputs("goal limit exceeded\n", stderr);
        break;
    case FL_NO_GLOBAL:
        fputs("unassigned global variable: ", stderr);
        write_limited(tcb, stderr, x);
        break;
    default:
        fprintf(stderr, "unknown error code %d", error);
    }
    fputc('\n', stderr);
    fl_terminate(1);
}

void fl_rt_error2(FL_TCB *tcb, FL_VAL x, FL_VAL y, int error)
{
    fprintf(stderr, "\n*** ERROR: <%ld> %ld: %s - ", tcb->ordinal, tcb->ticks, 
        tcb->goal->info);
    switch(error) {
    case FL_CANT_UNIFY:
        fputs("unification failed:", stderr);
        break;
    case FL_NO_PDEF:
        fputs("process definition not found: ", stderr);
        fl_write(tcb, stderr, x);
        fputc('/', stderr);
        fl_write(tcb, stderr, y);
        fputc('\n', stderr);
        fl_terminate(1);
    case FL_OCCURS:
        fputs("variable occurs in assigned value:", stderr);
        break;
    case FL_BAD_STRUCT:
        fputs("expected structure:", stderr);
        break;
    case FL_TRAIL_FULL:
        fputs("trail-buffer exceeded while unifying:", stderr);
        break;
    default:
        fprintf(stderr, "unknown error code %d", error);
    }
    fprintf(stderr, "\n<%ld> %ld: %s   ", tcb->ordinal, tcb->ticks, tcb->goal->info);
    write_limited(tcb, stderr, x);
    fprintf(stderr, "\n<%ld> %ld: %s   ", tcb->ordinal, tcb->ticks, tcb->goal->info);
    write_limited(tcb, stderr, y);
    fputc('\n', stderr);
    fl_terminate(1);
}

FL_VAL fl_alloc_cell(FL_TCB *tcb, FL_VAL car, FL_VAL cdr)
{
    /* Cell layout: 
        List = [Car Cdr]
        Var = [Val Suspensions(List)]   Val == Var, if unbound
        Float = [Num* Unused^]
        Port = [Stream|Source% Unused]
        Ref = [Id/Var% Unused]
        Module = [Module+ Unused]
        Task = [[Env|ParentTask]|StatusVar]
        Rtask = [[Env]|Parent%]
        *: in native format
        +: marked with lowest bit set
        %: RPtr 
        ^: only on 64 bit platform */
    if(tcb->freelist == NULL) fl_rt_error(tcb, 0, FL_NO_CELLS);
    FL_CELL *c = tcb->freelist;
    tcb->freelist = tcb->freelist->cdr;
    c->car = addref(car);
    c->cdr = addref(cdr);
    ++tcb->used;
    return (FL_VAL)c;
}

FL_VAL fl_alloc_float(FL_TCB *tcb, double n)
{
#if defined(__LP64__) || defined(_LP64)
    FL_VAL c = fl_alloc_cell(tcb, fl_nil, fl_nil);
#else    
    FL_VAL *prev = (FL_VAL *)&tcb->freelist;
    FL_VAL c = *prev;
    while(((long)c & 4) == 0) {
        if(c == NULL) fl_rt_error(tcb, 0, FL_NO_CELLS);
        prev = &((FL_CELL *)c)->cdr;
        c = *prev;
    }
    *prev = CDR(c);
    ++tcb->used;
#endif
    ((FL_CELL *)c)->tag = FL_FLOAT_TAG;
    *((double *)&((FL_CELL *)c)->car) = n;
    return c;
}

static void enqueue(FL_TCB *tcb, FL_GOAL *g)
{
    *(tcb->qend++) = g;
    if(tcb->qend == tcb->qstart)
        fl_rt_error(tcb, fl_nil, FL_QUEUE_FULL);
    if(tcb->qend >= &tcb->queue[ tcb->max_goals ])
        tcb->qend = tcb->queue;
    ++tcb->active;
}

static FL_GOAL *dequeue(FL_TCB *tcb)
{
    assert(tcb->qstart != tcb->qend);
    FL_GOAL *g = *(tcb->qstart++);
    if(tcb->qstart >= &tcb->queue[ tcb->max_goals ])
        tcb->qstart = tcb->queue;
    --tcb->active;
    return g;
}

FL_VAL fl_match_rec(FL_TCB *tcb, FL_VAL x, FL_VAL y)
{
restart:
    x = deref(x);
    y = deref(y);
    if(x == y) return fl_true;
    if(BITS(x) == 0) {
        if(TAG(x) == FL_VAR_TAG || TAG(x) == FL_REF_TAG) {
            *(tcb->sstackp++) = x;
            *(tcb->sstackp++) = tcb->goal->addr;
            return MKINT(0);
        }
    }
    if(BITS(y) == 0) {
        if(TAG(y) == FL_VAR_TAG || TAG(y) == FL_REF_TAG) {
            *(tcb->sstackp++) = y;
            *(tcb->sstackp++) = tcb->goal->addr;
            return MKINT(0);
        }
    }
    if(BITS(x) != 0) {
        if(BITS(y) != 0) return fl_false;
        if(ISINT(x) && TAG(y) == FL_FLOAT_TAG && INT(x) == FLOATVAL(y))
            return fl_true;
        return fl_false;
    }
    if(BITS(y) != 0) {
        if(ISINT(y) && TAG(x) == FL_FLOAT_TAG && INT(y) == FLOATVAL(x))
            return fl_true;
        return fl_false;
    }
    if(TAG(x) != TAG(y)) return fl_false;
    if(TAG(x) == FL_FLOAT_TAG) 
        return FLOATVAL(x) == FLOATVAL(y) ? fl_true : fl_false;
    FL_VAL r = fl_match_rec(tcb, CAR(x), CAR(y));
    if(r != fl_true) return r;
    x = CDR(x);
    y = CDR(y);
    goto restart;
}

FL_VAL fl_idle(FL_TCB *tcb, long *cache)
{
    if(tcb->epoch == *cache + 1) {
        *cache = 0;
        return fl_true;
    }
    *cache = tcb->epoch;
    *(tcb->sstackp++) = tcb->idle;
    *(tcb->sstackp++) = tcb->goal->addr;    
    return fl_false;
}

static void suspend(FL_TCB *tcb, int dbg)
{
    /* Suspension-list:
       [[GOAL1+, [Var1+, Addr1+], ...], ...]
       +: marked */
    FL_VAL slst = fl_nil;
    for(FL_VAL *p = tcb->sstack; p < tcb->sstackp; p += 2) {
        FL_VAL var = *p;
        FL_VAL addr = p[ 1 ];
        if(TAG(var) == FL_REF_TAG) {
            /* mutate REF into VAR and send a read-request to peer to set 
               the VAR: */
            FL_VAL remote = CAR(var);
#ifdef XLOGGING
            if(tcb->logging) {
                int id = RPTR_ID(remote);
                fl_logfmt(tcb, "ref->var _%lu_%d (%p) -> _%lu\n", 
                    (unsigned long)RPTR(remote) - (unsigned long)fl_tcbs[ id - 1 ].heap, 
                    id, RPTR(remote), 
                    (unsigned long)var - (unsigned long)tcb->heap);
            }
#endif
            ((FL_CELL *)var)->tag = FL_VAR_TAG | COUNT(var);
            ((FL_CELL *)var)->car = var;
            ((FL_CELL *)var)->cdr = fl_nil;            
            FL_VAL ev = mklist(tcb, MKINT(SEND_READ), mklist(tcb, remote, var));
            fl_add_event(tcb, ev);
        }
        FL_VAL x = mklist(tcb, MARK(var), MARK(addr));
        slst = mklist(tcb, x, slst);
    }
    slst = mklist(tcb, MARK(tcb->goal), slst);
    /* add suspension list to all vars on sstack: */
    for(FL_VAL *p = tcb->sstack; p < tcb->sstackp; p += 2) {
        FL_VAL var = *p;
        FL_VAL sl = CDR(var);
        CDR(var) = addref(mklist(tcb, slst, sl));
#ifdef LOGGING
        if(dbg && tcb->logging)
            fl_logfmt(tcb, "suspends on _%lu\n", (unsigned long)var - 
                (unsigned long)tcb->heap);
#endif
        unref(tcb, sl);
    }
    tcb->sstackp = tcb->sstack;
    ++tcb->suspended;
}

void fl_suspend(FL_TCB *tcb) 
{
    suspend(tcb, XDBG);
}

void fl_d_suspend(FL_TCB *tcb) 
{
    suspend(tcb, 1);
}

static void resume(FL_TCB *tcb, FL_VAL var, int dbg)
{
    for(FL_VAL x = CDR(var); x != fl_nil; x = CDR(x)) {
        FL_VAL slst = CAR(x);
        if(CAR(slst) != fl_nil) { /* not already resumed? */
            --tcb->suspended;
            FL_GOAL *g = (FL_GOAL *)UNMARK(CAR(slst));
#ifdef LOGGING
            if(dbg && tcb->logging)
                fl_logfmt(tcb, "resuming %s on _%lu\n", g->info, (unsigned long)var -
                    (unsigned long)tcb->heap);
#endif
            enqueue(tcb, g);
            /* clear goal entry in suspension list to avoid repeated resume
               of the same goal: */
            CAR(slst) = fl_nil;
            slst = CDR(slst);
            /* set goal-addr to some clause that suspends on this variable: */
            while(slst != fl_nil) {
                FL_VAL p1 = CAR(slst);
                FL_VAL v = UNMARK(CAR(p1));
                if(v == var) {
                    g->addr = (void *)UNMARK(CDR(p1));
                    break;
                }
                slst = CDR(slst);
            }
        }
    }
    unref(tcb, CDR(var));
    CDR(var) = fl_nil;
}

FL_VAL fl_chase_tail(FL_VAL st)
{
    st = deref(st);
    while(TAG(st) == FL_LIST_TAG) {
        st = deref(CDR(st));
        assert(((long)st & FL_BITS_MASK) == 0);
    }
    return st;
}

void fl_add_event(FL_TCB *tcb, FL_VAL event)
{
#ifdef LOGGING
    if(tcb->logging) {
        pthread_mutex_lock(&log_mutex);
        fprintf(logfile, "<%ld> %ld: %s event: ", tcb->ordinal, tcb->ticks, 
            tcb->goal->info);
        write_limited(tcb, logfile, event);
        fputc('\n', logfile);
        fprintf(logfile, "<%ld> %ld: %s   queue: ", tcb->ordinal, tcb->ticks, 
            tcb->goal->info);
        write_limited(tcb, logfile, tcb->events);
        fputc('\n', logfile);
        if(tcb->defer) {
            fprintf(logfile, "<%ld> %ld: %s   deferred: ", tcb->ordinal, tcb->ticks,
                tcb->goal->info);
            write_limited(tcb, logfile, tcb->deferred);
            fputc('\n', logfile);
        }
        pthread_mutex_unlock(&log_mutex);
    }
#endif
    assert(tcb->state != FL_FINISHING);
    FL_VAL var = mkvar(tcb);
    FL_VAL n = mklist(tcb, event, var);
    if(tcb->defer) {
        fl_assign(tcb, n, tcb->deferred_tail);
        tcb->deferred_tail = var;
        return;
    }
    FL_VAL s = addref(fl_chase_tail(tcb->events));
    assert(BITS(s) == 0 && TAG(s) == FL_VAR_TAG);
    int diddefer = 0;
    if(!tcb->defer) {
        tcb->defer = diddefer = 1;
        tcb->deferred = tcb->deferred_tail = var;
    }
    unref(tcb, tcb->events);
    if(diddefer) tcb->defer = 0;
    fl_assign(tcb, n, s);
    tcb->events = s;
}

static int search(FL_VAL haystack, FL_VAL needle)
{
    while(BITS(haystack) == 0) {
        if(needle == haystack) return 1;
        switch(TAG(haystack)) {
        case FL_VAR_TAG: {
            FL_VAL x = CAR(haystack);
            if(x == haystack) return 0;
            haystack = x;
            break; }
        case FL_REF_TAG:
        case FL_FLOAT_TAG:
            return 0;
        default:
            if(search(CAR(haystack), needle)) return 1;
            haystack = CDR(haystack);
            break;
        }
    }
    return 0;
}

static void assign(FL_TCB *tcb, FL_VAL x, FL_VAL var, int dbg)
{
    int isref = TAG(var) == FL_REF_TAG;
#ifdef LOGGING
    if(dbg && tcb->logging) {
        pthread_mutex_lock(&log_mutex);
        fprintf(logfile, "<%ld> %ld: %s assign ", tcb->ordinal, tcb->ticks, 
            tcb->goal->info);
        if(!isref)
            fprintf(logfile, "_%lu: ", (unsigned long)var - (unsigned long)tcb->heap);
        else {
            FL_VAL remote = CAR(var);
            int id = RPTR_ID(remote);
            fprintf(logfile, "_%lu_%d: ", 
                (unsigned long)RPTR(remote) - (unsigned long)fl_tcbs[ id - 1 ].heap, 
                id);
        }
        write_limited(tcb, logfile, x);
        fputc('\n', logfile);
        pthread_mutex_unlock(&log_mutex);
    }
#endif
    if(isref) {
        FL_VAL ev = mklist(tcb, MKINT(SEND_VALUE), mklist(tcb, CAR(var), addref(x)));
        fl_add_event(tcb, ev);
        // mutate REF into VAR:
        ((FL_CELL *)var)->tag = FL_VAR_TAG | COUNT(var);
        CAR(var) = x;
        CDR(var) = fl_nil;
        return;
    }
    if(CAR(var) == x) return;
    if(CAR(var) != var) {
        fprintf(stderr, "<%ld> %ld: %s - assignment to non-variable _%lu: ", tcb->ordinal,
            tcb->ticks, tcb->goal->info, (unsigned long)var - (unsigned long)tcb->heap);
        write_limited(tcb, stderr, x);
        fputs(" (is: ", stderr);
        write_limited(tcb, stderr, CAR(var));
        fputs(")\n", stderr);
        fl_terminate(1);
    }
    if(occurs_check && search(x, var))
        fl_rt_error2(tcb, var, x, FL_OCCURS);
    ((FL_CELL *)var)->car = addref(x);
    if(((FL_CELL *)var)->cdr != fl_nil) resume(tcb, var, dbg);
}

void fl_assign(FL_TCB *tcb, FL_VAL x, FL_VAL var)
{
    if(BITS(var) != 0) fl_rt_error(tcb, var, FL_NOT_A_VAR);
    assign(tcb, x, var, XDBG);
}

void fl_d_assign(FL_TCB *tcb, FL_VAL x, FL_VAL var)
{
    if(BITS(var) != 0) fl_rt_error(tcb, var, FL_NOT_A_VAR);
    assign(tcb, x, var, 1);
}

static void close_stream(FL_TCB *tcb, FL_VAL st)
{
#ifdef XLOGGING
    if(tcb->logging) {
        pthread_mutex_lock(&log_mutex);
        fprintf(logfile, "<%ld> %ld: %s closing stream: ", tcb->ordinal, tcb->ticks,
            tcb->goal->info);
        write_limited(tcb, logfile, st);
        fputc('\n', logfile);
        pthread_mutex_unlock(&log_mutex);
    }
#endif
    st = fl_chase_tail(st);
    if(BITS(st) == 0 && TAG(st) == FL_VAR_TAG) 
        fl_assign(tcb, fl_nil, st);
}

void fl_release(FL_TCB *tcb, FL_VAL x)
{
    FL_CELL *c;
    FL_VAL tl;
    void *addr;
restart:
#ifdef XXLOGGING
    if(tcb->logging) {
        pthread_mutex_lock(&log_mutex);
        fprintf(logfile, "<%ld> %ld: %s release %p (0x%08lx) [%ld]: ", tcb->ordinal, 
            tcb->ticks, tcb->goal->info, x, TAG(x), tcb->used);
        if(TAG(x) == FL_VAR_TAG)
            fprintf(logfile, "_%lu -> ", (unsigned long)x - (unsigned long)tcb->heap);
        write_limited(tcb, logfile, x);
        fputc('\n', logfile);
        pthread_mutex_unlock(&log_mutex);
    }
#endif
    c = (FL_CELL *)x;
    switch(c->tag & FL_TAG_MASK) {
    case FL_FLOAT_TAG:
        x = NULL;
        break;
    case FL_VAR_TAG:
        if(c->car != x) {
            if(BITS(c->cdr) != 0) {
                x = c->car;
                break;
            }
            unref(tcb, c->car);
        }
        x = c->cdr;
        break;
    case FL_PORT_TAG:
        if(((long)c->car & FL_INT_BIT) != 0) {
            if(tcb->state == FL_FINISHING) {
                int id = RPTR_ID(c->car);
                unref(&fl_tcbs[ id - 1 ], RPTR(c->car));
            } else {
                FL_VAL ev = mklist(tcb, MKINT(SEND_DROPREF), mklist(tcb, c->car, fl_nil));
                fl_add_event(tcb, ev);
            }
            x = NULL;
        } else {
            close_stream(tcb, c->car);
            x = c->car;
        }
        break;
    case FL_REF_TAG:
        x = NULL;
        if(tcb->state == FL_FINISHING) {
            int id = RPTR_ID(c->car);
            unref(&fl_tcbs[ id - 1 ], RPTR(c->car));
        } else {
            tl = mklist(tcb, MKINT(SEND_DROPREF), mklist(tcb, c->car, fl_nil));
            fl_add_event(tcb, tl);
        }
        break;
    case FL_RTASK_TAG:
        x = NULL;
        unref(tcb, c->car);
        if(tcb->state == FL_FINISHING) {
            int id = RPTR_ID(c->cdr);
            unref(&fl_tcbs[ id - 1 ], RPTR(c->cdr));
        } else {
            tl = mklist(tcb, MKINT(SEND_DROPREF), mklist(tcb, c->cdr, fl_nil));
            fl_add_event(tcb, tl);
        }
        break;        
    case FL_TASK_TAG:
#ifdef XLOGGING
        if(tcb->logging) fl_logfmt(tcb, "task %p finished\n", c);
#endif
        fl_assign(tcb, fl_nil, c->cdr);
        /* fall through ... */
    default:
        unref(tcb, c->car);
        x = c->cdr;
        break;
    }
    c->tag = 0;
    c->car = (FL_VAL)FL_BROKEN_HEART;
    c->cdr = tcb->freelist;
    //
    tcb->freelist = c;
    --tcb->used;
    if(x == NULL) return;
    /* hand-inline unref() to allow tail recursion: */
    if(BITS(x) != 0) return;
    assert(CAR(x) != (FL_VAL)FL_BROKEN_HEART);
    if(COUNT(x) == FL_COUNT_MASK) return;
    assert((FL_CELL *)x >= tcb->heap && (FL_CELL *)x < tcb->heapend);
    assert((((FL_CELL *)x)->tag & FL_COUNT_MASK) > 0);
    --((FL_CELL *)x)->tag;
    if(COUNT(x) == 0) goto restart;
}

void fl_abort(FL_TCB *tcb, FL_VAL err)
{
    write_limited(tcb, stderr, err);
    fputc('\n', stderr);
    fl_terminate(1);
}

void fl_fail(FL_TCB *tcb, int arity)
{
    fprintf(stderr, "<%ld> %ld: %s failed:\n", tcb->ordinal, tcb->ticks, tcb->goal->info);
    if(tcb->logging) fflush(logfile);
    tcb->logging = 1;
    logfile = stderr;
    fl_log_entry(tcb, arity);
    fl_terminate(1);
}

static char **next_string(char *p)
{
    // add length- and final 0-byte
    int wsize = sizeof(long);
    int len = STRINGLENGTH(p);
    return (char **)(((long)p + len + 2 + wsize - 1) & ~(wsize - 1));
}

FL_VAL fl_intern(char *np)
{
    pthread_mutex_lock(&intern_mutex);
    char *p = strings; 
    while(p != NULL) {
        if(*p == *np && !strncmp(np + 1, p + 1, *p)) {
            pthread_mutex_unlock(&intern_mutex);
            return p;
        }
        p = *next_string(p);
    }
    *next_string(np) = strings;
    strings = np;
    pthread_mutex_unlock(&intern_mutex);
    return np;
}

FL_VAL fl_mkstring(char *str, int len)
{
    assert(len < 256);
    int wsize = sizeof(long);
    /* len + '\0' + alignment padding: */
    char *buf = malloc(len + 2 + 2 + sizeof(char *) + wsize - 1);
    char *sp = buf + 2;
    *sp = len;
    memcpy(sp + 1, str, len);
    sp[ len + 1 ] = '\0';
    *((char **)(((long)sp + len + 2 + wsize - 1) & ~(wsize - 1))) = NULL;
    FL_VAL s = fl_intern(sp);
    if(s != sp) free(buf);
    return s;
}

void fl_send_to_port(FL_TCB *tcb, FL_VAL port, FL_VAL x)
{
    if(BITS(port) != 0 || TAG(port) != FL_PORT_TAG)
        fl_rt_error(tcb, port, FL_NOT_A_PORT);
    if(((long)CAR(port) & FL_INT_BIT) != 0) {
        FL_VAL ev = mklist(tcb, MKINT(SEND_SENDPORT), mklist(tcb, CAR(port), x));
        fl_add_event(tcb, ev);
        return;
    }
    FL_VAL head = CAR(port);
    FL_VAL st = fl_chase_tail(head);
    FL_VAL e = fl_alloc_cell(tcb, fl_nil, fl_nil);
    ((FL_CELL *)e)->tag = FL_VAR_TAG;
    ((FL_CELL *)e)->car = e;
    ((FL_CELL *)port)->car = addref(e);
    FL_VAL n = fl_alloc_cell(tcb, x, e);
    ((FL_CELL *)n)->tag = FL_LIST_TAG;
    fl_assign(tcb, n, st);
    unref(tcb, head);
}

char *fl_forward_rec(FL_TCB *tcb, FL_VAL x, int dest, char *ptr, char *limit)
{
    for(;;) {
        assert(((unsigned long)ptr & 3) == 0);
        x = deref(x);
        assert(ptr < limit);
        if(BITS(x) == FL_ATOM_BIT && dest == FL_DETACHED) {
            *((unsigned long *)ptr) = FL_INTERNED;
            ptr += sizeof(FL_VAL);
            int len = *((char *)x) + 1;
            memcpy(ptr, x, len);
            int wsize = sizeof(long);
            return ptr + ((len + wsize - 1) & ~(wsize - 1));
        }
        if(BITS(x) != 0) {
            *((FL_VAL *)ptr) = x;
            return ptr + sizeof(FL_VAL);
        }
        if(COUNT(x) == FL_COUNT_MASK && dest != FL_DETACHED) {
            *((unsigned long *)ptr) = FL_CONSTANT;
            ptr += sizeof(long);
            *((FL_VAL *)ptr) = x;
            return ptr + sizeof(FL_VAL);            
        }
        FL_VAL ev;
        switch(TAG(x)) {
        case FL_FLOAT_TAG:
            *((unsigned long *)ptr) = FL_FLOAT_TAG;
            ptr += sizeof(long);
#if !defined(__LP64__) && !defined(_LP64)
            if(((long)ptr & 8) != 0) ptr += sizeof(long);
#endif
            *((double *)ptr) = *((double *)&CAR(x));
            return ptr + sizeof(double);
        case FL_PORT_TAG:
            assert(dest != FL_DETACHED);
            *((unsigned long *)ptr) = FL_PORT_TAG;
            ptr += sizeof(long);
            if(((long)CAR(x) & FL_INT_BIT) != 0) {
                *((FL_VAL *)ptr) = CAR(x);
                if(RPTR_ID(CAR(x)) != dest) {
                    ev = mklist(tcb, MKINT(SEND_ADDREF), mklist(tcb, CAR(x), fl_nil));
                    fl_add_event(tcb, ev);
                } else addref(RPTR(CAR(x)));
            } else *((FL_VAL *)ptr) = mkrptr(tcb, addref(x));
            return ptr + sizeof(FL_VAL);
            // used below in several places:
        fwd_as_var: {
            FL_VAL v = mkvar(tcb);
            CAR(v) = addref(x);
            x = v; }
            // falls through to ...
        case FL_VAR_TAG:
            assert(dest != FL_DETACHED);
            if(dest == tcb->ordinal) {
                *((unsigned long *)ptr) = FL_VAR_TAG;
                ptr += sizeof(long);
                *((FL_VAL *)ptr) = addref(x);
            } else {
                *((unsigned long *)ptr) = FL_REF_TAG;
                ptr += sizeof(long);
                FL_VAL rptr = mkrptr(tcb, addref(x));
                *((FL_VAL *)ptr) = rptr;
            }
            return ptr + sizeof(FL_VAL);
        case FL_REF_TAG:
            assert(dest != FL_DETACHED);
            *((unsigned long *)ptr) = FL_REF_TAG;
            ptr += sizeof(long);
            *((FL_VAL *)ptr) = CAR(x);
            if(RPTR_ID(CAR(x)) != dest) {
                ev = mklist(tcb, MKINT(SEND_ADDREF), mklist(tcb, CAR(x), fl_nil));
                fl_add_event(tcb, ev);
            }
            return ptr + sizeof(FL_VAL);
        case FL_LIST_TAG:
            if(ptr + WATERMARK >= limit) goto fwd_as_var;
            *((unsigned long *)ptr) = FL_LIST_TAG;
            ptr += sizeof(long);
            ptr = fl_forward_rec(tcb, CAR(x), dest, ptr, limit);
            x = CDR(x);
            break;
        default:
            if(ptr + WATERMARK >= limit) goto fwd_as_var;
            if((TAG(x) & FL_TUPLE_BIT) != 0) {
                *((unsigned long *)ptr) = TAG(x);
                ptr += sizeof(long);
                ptr = fl_forward_rec(tcb, CAR(x), dest, ptr, limit);
                x = CDR(x);
            } else fl_rt_error(tcb, x, FL_CANT_FORWARD);
            break;
        }
    }
}

int fl_forward(FL_TCB *tcb, int id, FL_VAL x)
{
    FL_TCB *dest = &fl_tcbs[ id - 1 ];
    assert(dest->state != FL_FINISHING);
    int r = fl_forward_to_port(tcb, dest->port, x, id);
    if(r) {
        __sync_add_and_fetch(&msg_tally, 1);
        if(dest->wakeup_hook != NULL)
            dest->wakeup_hook(dest);
    }
    return r;
}
    
int fl_forward_to_port(FL_TCB *tcb, char *port, FL_VAL x, int dest)
{
    int *port_lock = PORT_LOCK(port);
    int *port_size = PORT_SIZE(port);
    int *port_msgid = PORT_MSGID(port);
    int *port_source = PORT_SOURCE(port);
    char *port_data = PORT_DATA(port);
    while(!__sync_bool_compare_and_swap(port_lock, 0, 1));
    if(*port_size) {
        /* port blocked */
        *port_lock = 0;
        __sync_synchronize();
        return 0;
    }
#ifdef LOGGING
    ++(*port_msgid);
    if(tcb->logging) {
        pthread_mutex_lock(&log_mutex);
        fprintf(logfile, "<%ld> %ld: %s send -> %d <%d>: ", tcb->ordinal, tcb->ticks, 
            tcb->goal->info, *port_msgid, dest);
        write_limited(tcb, logfile, x);
        fputc('\n', logfile);
        pthread_mutex_unlock(&log_mutex);
    }
#endif   
    char *p = fl_forward_rec(tcb, x, dest, port_data, port + FL_PORT_SIZE);
#ifdef XXLOGGING
    if(tcb->logging) {
        pthread_mutex_lock(&log_mutex);
        for(char *pp = port_data; pp < p; pp += sizeof(long))
            fprintf(logfile, "<%ld>    0x%08lx\n", tcb->ordinal,
                *((unsigned long *)pp));
        pthread_mutex_unlock(&log_mutex);
    }        
#endif
    *port_size = p - port_data;
    *port_source = (dest == FL_DETACHED ? FL_DETACHED : tcb->ordinal);
    __sync_synchronize();
    *port_lock = 0;
    return 1;
}

char *fl_unforward_rec(FL_TCB *tcb, char *ptr, FL_VAL *result)
{
    for(;;) {
        assert(BITS(ptr) == 0);
        FL_VAL x = *((FL_VAL *)ptr);
        ptr += sizeof(FL_VAL);
        if(BITS(x) != 0) {
            *result = x;
            return ptr;
        }
        if((unsigned long)x == FL_INTERNED) {
            int len = *((char *)ptr);
            *result = fl_mkstring(ptr + 1, len);
            int wsize = sizeof(long);
            return ptr + ((len + wsize) & ~(wsize - 1));
        }
        if((unsigned long)x == FL_CONSTANT) {
            *result = *((FL_VAL *)ptr);
            return ptr + sizeof(FL_VAL);
        }
        switch((unsigned long)x) {
        case FL_PORT_TAG: {
            FL_VAL h = *((FL_VAL *)ptr);
            ptr += sizeof(FL_VAL);
            if(RPTR_ID(h) == tcb->ordinal) *result = RPTR(h);
            else {
                *result = addref(mkcell(tcb, FL_PORT_TAG, fl_nil, fl_nil));
                CAR(*result) = h; /* no implicit addref */
            }
            return ptr; }
        case FL_VAR_TAG:    /* locally sent to self */
            *result = *((FL_VAL *)ptr);
            ptr += sizeof(FL_VAL);
            return ptr;
        case FL_REF_TAG: {
            FL_VAL h = *((FL_VAL *)ptr);
            ptr += sizeof(FL_VAL);
            if(RPTR_ID(h) == tcb->ordinal) {
                /* rptr in REF originated from this thread: */
                *result = addref(RPTR(h));
                return ptr;
            } 
            *result = addref(mkcell(tcb, FL_REF_TAG, h, fl_nil));
            return ptr; }
        case FL_FLOAT_TAG:
#if !defined(__LP64__) && !defined(_LP64)
            if(((long)ptr & 8) != 0) ptr += sizeof(long);
#endif
            *result = addref(fl_alloc_float(tcb, *((double *)ptr)));
            return ptr + sizeof(double);
        case FL_LIST_TAG:
            *result = addref(mklist(tcb, fl_nil, fl_nil));
            ptr = fl_unforward_rec(tcb, ptr, &CAR(*result));
            result = &CDR(*result);
            break; /* loop */
        default:
            if(((unsigned long)x & FL_TUPLE_BIT) != 0) {
                FL_VAL h;
                ptr = fl_unforward_rec(tcb, ptr, &h);
                *result = addref(mkcell(tcb, (unsigned long)x, fl_nil, fl_nil));
                CAR(*result) = h;
                result = &CDR(*result);
                break; /* loop */
            } else fl_rt_error(tcb, fl_nil, FL_CANT_UNFORWARD);
        }
    }
}

FL_VAL fl_unforward(FL_TCB *tcb)
{
    int *port_lock = PORT_LOCK(tcb->port);
    int *port_size = PORT_SIZE(tcb->port);
    int *port_msgid = PORT_MSGID(tcb->port);
    int *port_source = PORT_SOURCE(tcb->port);
    char *port_data = PORT_DATA(tcb->port);
    while(!__sync_bool_compare_and_swap(port_lock, 0, 1));
    FL_VAL x;
    char *p = port_data + *port_size;
    fl_unforward_rec(tcb, port_data, &x);
    *port_size = 0;
    int src = *port_source;
    int id = *port_msgid;
    __sync_synchronize();
    *port_lock = 0;
#ifdef LOGGING
    if(tcb->logging) {
        pthread_mutex_lock(&log_mutex);
        fprintf(logfile, "<%ld> %ld: %s receive <- %d <%d>: ",
             tcb->ordinal, tcb->ticks, tcb->goal->info, id, src);
        write_limited(tcb, logfile, x);
        fputc('\n', logfile);
#ifdef XXLOGGING
        for(char *pp = port_data; pp < p; pp += sizeof(long))
            fprintf(logfile, "<%ld>    0x%08lx\n", tcb->ordinal,
                *((unsigned long *)pp));
#endif
        pthread_mutex_unlock(&log_mutex);
    }
#endif
    return x;
}

static FL_HTNODE *lookup(FL_TCB *tcb, FL_VAL key, FL_HTNODE ***root) 
{
    if(tcb->globals == NULL)
        tcb->globals = calloc(FL_GLOBAL_TABLE, sizeof(FL_HTNODE *));
    int k = ((long)key ^ ((long)key >> 16)) & 0x7fffffff;
    *root = tcb->globals + k % FL_GLOBAL_TABLE;
    for(FL_HTNODE *n = **root; n != NULL; n = n->next) {
        if(n->key == key) return n;
    }
    return NULL;
}

FL_VAL fl_get_global(FL_TCB *tcb, FL_VAL key, FL_VAL def) 
{
    FL_HTNODE **root;
    FL_HTNODE *n = lookup(tcb, key, &root);
    if(n != NULL) return n->val;
    return def;
}

void fl_set_global(FL_TCB *tcb, FL_VAL key, FL_VAL val) 
{
    FL_HTNODE **root;
    FL_HTNODE *n = lookup(tcb, key, &root);
    if(n != NULL) {
        unref(tcb, n->val);
        n->val = addref(val);
    } else {
        n = calloc(1, sizeof(FL_HTNODE));
        n->key = key;
        n->val = addref(val);
        n->next = *root;
        *root = n;
    }
}

void fl_init_module(FL_MODULE *mod)
{
    mod->next = last_module;
    last_module = mod;
    int i = 0;
    char *np = mod->names;
    while(np != NULL) {
        char *np2 = *next_string(np);
        mod->atoms[ i++ ] = fl_intern(np);
        np = np2;
    }
    for(FL_VAL **rap = mod->ratoms; *rap != NULL; ++rap) {
        FL_VAL *ap = *rap;
        *ap = mod->atoms[ (long)(*ap) ];
    }
    mod->name = mod->atoms[ (long)mod->name ];
    for(i = 0; i < mod->npdefs; ++i)
        mod->pdefs[ i ].name = mod->atoms[ (long)mod->pdefs[ i ].name ];
}

static void init_thread(FL_TCB *tcb, long heapsz, int maxgoals)
{
    tcb->goal = NULL;
    tcb->sstack = (FL_VAL *)malloc(MAX_SSTACK * sizeof(FL_VAL) * 2);
    tcb->sstackp = tcb->sstack;
    tcb->tailcalls = 0;
    tcb->timeslice = timeslice;
    tcb->ticks = 0;
    tcb->stackbase = NULL;  /* set on thread-start */
    long cells = heapsz / sizeof(FL_CELL);
    tcb->heap = (FL_CELL *)malloc(heapsz);
    tcb->heapend = tcb->heap + cells;
    tcb->heap[ 0 ].car = (FL_VAL)FL_BROKEN_HEART;
    tcb->heap[ 0 ].tag = 0;
    tcb->heap[ 0 ].cdr = NULL;
    for(int i = 1; i < cells; ++i) {
        tcb->heap[ i ].tag = 0;
        tcb->heap[ i ].car = (FL_VAL)FL_BROKEN_HEART;
        tcb->heap[ i ].cdr = (FL_VAL)(&tcb->heap[ i - 1 ]);
    }
    tcb->freelist = &tcb->heap[ cells - 1 ];
    tcb->suspended = 0;
    tcb->active = 0;
    tcb->goal_buffer = (FL_GOAL *)malloc(maxgoals * sizeof(FL_GOAL));
    tcb->max_goals = maxgoals;
    tcb->queue = (FL_GOAL **)malloc(maxgoals * sizeof(FL_GOAL *));
    tcb->qstart = tcb->qend = tcb->queue;
    tcb->goal_buffer[ 0 ].addr = NULL;
    for(int i = 1; i < maxgoals; ++i) {
        tcb->goal_buffer[ i ].addr = (void *)(&tcb->goal_buffer[ i - 1 ]);
        tcb->goal_buffer[ i ].task = fl_nil;
    }
    tcb->freegoals = &tcb->goal_buffer[ maxgoals - 1 ];
    tcb->delay = 0;
    tcb->events = fl_nil;
    tcb->listening = fl_nil;
    tcb->state = FL_BUSY;
    tcb->detached = 0;
    tcb->mportfile = NULL;
    tcb->port = malloc(FL_PORT_SIZE);
    memset(tcb->port, 0, FL_PORT_SIZE);
    tcb->defer = 0;
    tcb->epoch = 2;
    tcb->globals = NULL;
    tcb->write_limit = -1;
    tcb->cbuf = NULL;
    tcb->counters = NULL;
    tcb->wakeup_hook = NULL;
    tcb->occurs_check = occurs_check;
    tcb->average = tcb->peak = tcb->scounts = 0;
    tcb->idle = fl_alloc_cell(tcb, fl_nil, fl_nil);
    ((FL_CELL *)tcb->idle)->tag = FL_VAR_TAG | 1;
    ((FL_CELL *)tcb->idle)->car = tcb->idle;
    if(fl_create_event_queue(tcb) == -1) {
        fprintf(stderr, "\n*** ERROR: <%ld> unable to create event queue: %s\n",
            tcb->ordinal, strerror(errno));
        exit(1);
    }
}

static void init_goal(FL_TCB *tcb, FL_PDEF *pdef)
{
#ifdef LOGGING
    if(tcb->logging) 
        fl_logfmt(tcb, "initializing goal %s/%ld @ %p\n", (char *)pdef->name + 1, 
            pdef->arity, pdef->addr);
#endif
    FL_GOAL *g = tcb->freegoals;
    tcb->freegoals = (FL_GOAL *)g->addr;
    g->addr = pdef->addr;
    g->info = "<init>";
    ++tcb->goals;
    if(tcb->goal != NULL) enqueue(tcb, g);
    else tcb->goal = g;
}

void fl_init_goal(FL_PDEF *pdef)
{
    init_goal(&fl_tcbs[ 0 ], pdef);
}

void fl_resolve_pdef(FL_TCB *tcb, long arity, FL_VAL mod, FL_VAL name, void *cache, FL_GOAL *ng)
{
#ifdef XXLOGGING
    if(tcb->logging)
        fl_logfmt(tcb, "resolve %s/%ld, cache=%p\n", (char *)name + 1, arity, cache);
#endif
    FL_VAL lst;
    int many = 0;
    int ar = arity;
    if(arity > FL_MAX_ARGS) {
        lst = ng->args[ FL_MAX_ARGS - 1 ];
        ar = 3;
        many = 1;
    } else lst = fl_nil;
    for(int i = ar - 1; i >= 0; --i) {
        FL_VAL lst2 = mklist(tcb, ng->args[ i ], lst);
        unref(tcb, ng->args[ i ]);
        lst = lst2;
    }
    if(many) unref(tcb, ng->args[ FL_MAX_ARGS - 1 ]);
    lst = mklist(tcb, mod, mklist(tcb, mktuple(tcb, name, arity, lst), fl_nil));
    ng->args[ 0 ] = addref(mktuple(tcb, fl_colon, 2, lst));
    ng->args[ 1 ] = MARK(cache);
}

static int type_order(FL_VAL x)
{
    if(((long)x & FL_INT_BIT) != 0) return 1;
    if(((long)x & FL_ATOM_BIT) != 0) return 3;
    if((TAG(x) & FL_TUPLE_BIT) != 0) return 5;
    switch(TAG(x)) {
    case FL_REF_TAG:
    case FL_VAR_TAG: return 0;
    case FL_FLOAT_TAG: return 2;
    case FL_LIST_TAG: return 4;
    case FL_PORT_TAG: return 6;
    case FL_MODULE_TAG: return 7;
    }
    return 9; /* ??? */
}

static int signum(long n)
{   
    if(n < 0) return -1;
    return n > 0 ? 1 : 0; 
}

FL_VAL fl_random_int(FL_VAL n) 
{
    return MKINT(fl_random_integer(INT(n)));
}

int fl_ordering(FL_TCB *tcb, FL_VAL x, FL_VAL y, long *suspend)
{
    x = deref(x);
    y = deref(y);
    int o1 = type_order(x), o2 = type_order(y);
    if(o1 == 0) {
        *(tcb->sstackp++) = x;
        *(tcb->sstackp++) = tcb->goal->addr;
        *suspend = 1;
        return 0;
    }
    if(o2 == 0) {
        *(tcb->sstackp++) = y;
        *(tcb->sstackp++) = tcb->goal->addr;
        *suspend = 1;
        return 0;
    }
    *suspend = 0;
    if(o1 < o2) return -1;
    if(o1 > o2) return 1;
    switch(o1) {
    case 2: /* float */ {
        double f1 = *((double *)&CAR(x));
        double f2 = *((double *)&CAR(y));
        double r = f1 - f2;
        if(r < 0) return -1;
        else if(r > 0) return 1;
        return 0; }
    case 3: /* string */ {
        int l1 = STRINGLENGTH(x);
        int l2 = STRINGLENGTH(y);
        char *p1 = (char *)x + 1;
        char *p2 = (char *)y + 1;
        for(int i = 0; i < l1; ++i) {
            if(i >= l2) return 1;
            if(*p1 < *p2) return -1;
            if(*p1 > *p2) return 1;
            ++p1;
            ++p2;
        }
        return l1 < l2 ? -1 : 0; }
    case 4: /* list */ {
        int o = fl_ordering(tcb, CAR(x), CAR(y), suspend);
        if(*suspend) return 0;
        if(o < 0) return -1;
        if(o > 0) return 1;
        return fl_ordering(tcb, CDR(x), CDR(y), suspend); }
    case 5: /* tuple */ {
        int l1 = (((FL_CELL *)x)->tag & ~FL_TUPLE_BIT) >> 24;
        int l2 = (((FL_CELL *)y)->tag & ~FL_TUPLE_BIT) >> 24;
        if(l1 < l2) return -1;
        if(l1 > l2) return 1;
        int o = fl_ordering(tcb, CAR(x), CAR(y), suspend);
        return (o == 0 && *suspend == 0) ? 
            fl_ordering(tcb, CDR(x), CDR(y), suspend) : o; }
    case 7: /* module */
        return (int)(CAR(x) - CAR(y));
    default:
        return signum((long)x - (long)y);
    }
}

static FL_VAL unify_rec(FL_TCB *tcb, FL_VAL x, FL_VAL y, int dbg)
{
    for(;;) {
        if(x == y) return fl_true;
        if(((long)x & FL_BITS_MASK) == 0) {
            if(TAG(x) == FL_VAR_TAG) {
                if(CAR(x) != x) {
                    x = CAR(x);
                    continue;
                }
            }
        }
        if(((long)y & FL_BITS_MASK) == 0) {
            if(TAG(y) == FL_VAR_TAG) {
                if(CAR(y) != y) {
                    y = CAR(y);
                    continue;
                }
            }
        }
        if(((long)x & FL_BITS_MASK) == 0) {
            if((TAG(x) == FL_VAR_TAG && CAR(x) == x) || 
                TAG(x) == FL_REF_TAG) {
                assign(tcb, y, x, dbg);
                return fl_true;
            }
        } 
        if(((long)y & FL_BITS_MASK) != 0) 
            return fl_false;
        if((TAG(y) == FL_VAR_TAG && CAR(y) == y) || 
            TAG(y) == FL_REF_TAG) {
            assign(tcb, x, y, dbg);
            return fl_true;
        }
        if(((long)x & FL_BITS_MASK) != 0) 
            return fl_false;
        long tx = TAG(x);
        if(tx != TAG(y)) 
            return fl_false;
        if(tx == FL_FLOAT_TAG)
            return *((double *)&CAR(x)) == *((double *)&CAR(y)) ? fl_true : fl_false;
        if(tx == FL_PORT_TAG)
            return CAR(x) == CAR(y) ? fl_true : fl_false;
        if(unify_rec(tcb, CAR(x), CAR(y), dbg) != fl_true)
            return fl_false;
        x = CDR(x);
        y = CDR(y);
    }
}

FL_VAL fl_unify_rec(FL_TCB *tcb, FL_VAL x, FL_VAL y)
{
    return unify_rec(tcb, x, y, XDBG);
}

FL_VAL fl_d_unify_rec(FL_TCB *tcb, FL_VAL x, FL_VAL y)
{
    return unify_rec(tcb, x, y, 1);
}

void fl_unify_rec_checked(FL_TCB *tcb, FL_VAL x, FL_VAL y)
{
    if(unify_rec(tcb, x, y, XDBG) == fl_false)
        fl_rt_error2(tcb, x, y, FL_CANT_UNIFY);
}

void fl_d_unify_rec_checked(FL_TCB *tcb, FL_VAL x, FL_VAL y)
{
    if(unify_rec(tcb, x, y, 1) == fl_false)
        fl_rt_error2(tcb, x, y, FL_CANT_UNIFY);
}

static int unify_safe_rec(FL_TCB *tcb, FL_VAL x, FL_VAL y, FL_VAL **trail)
{
    FL_VAL *tp = *trail;
    if(tp >= tcb->trailbuf + TRAIL_BUFFER) {
        fl_rt_error2(tcb, x, y, FL_TRAIL_FULL);
    }
    for(;;) {
        if(x == y) return 1;
        if(((long)x & FL_BITS_MASK) == 0) {
            if(TAG(x) == FL_VAR_TAG) {
                if(CAR(x) != x) {
                    x = CAR(x);
                    continue;
                }
            }
        }
        if(((long)y & FL_BITS_MASK) == 0) {
            if(TAG(y) == FL_VAR_TAG) {
                if(CAR(y) != y) {
                    y = CAR(y);
                    continue;
                }
            }
        }
        if(((long)x & FL_BITS_MASK) == 0) {
            if(TAG(x) == FL_VAR_TAG && CAR(x) == x) {
                *(tp++) = x;
                *(tp++) = CAR(x) = y;
                *trail = tp;
                return 1;
            }
            if(TAG(x) == FL_REF_TAG) {
                assign(tcb, y, x, XDBG);
                return 1;
            }
        } 
        if(((long)y & FL_BITS_MASK) != 0) 
            return 0;
        if(TAG(y) == FL_VAR_TAG && CAR(y) == y) {
            *(tp++) = y;
            *(tp++) = CAR(y) = x;
            *trail = tp;
            return 1;
        }
        if(TAG(y) == FL_REF_TAG) {
            assign(tcb, x, y, XDBG);
            return 1;
        }
        if(((long)x & FL_BITS_MASK) != 0) 
            return 0;
        long tx = TAG(x);
        if(tx != TAG(y)) 
            return 0;
        if(tx == FL_FLOAT_TAG)
            return *((double *)&CAR(x)) == *((double *)&CAR(y));
        if(tx == FL_PORT_TAG)
            return CAR(x) == CAR(y);
        *trail = tp;
        if(!unify_safe_rec(tcb, CAR(x), CAR(y), trail))
            return 0;
        tp = *trail;
        x = CDR(x);
        y = CDR(y);
    }
}

FL_VAL fl_unify_safe1(FL_TCB *tcb, FL_VAL x, FL_VAL y)
{
    if(tcb->trailbuf == NULL) 
        tcb->trailbuf = malloc(TRAIL_BUFFER * sizeof(FL_VAL *));
    FL_VAL *tp = tcb->trailbuf;
    int r = unify_safe_rec(tcb, x, y, &tp);
    if(r && tcb->occurs_check) {
        for(FL_VAL *tpp = tcb->trailbuf; tpp < tp; tpp += 2) {
            FL_VAL var = *tpp, x = tpp[ 1 ];
            if(search(x, var)) {
                r = 0;
                break;
            }
        }
    }
    for(FL_VAL *tpp = tcb->trailbuf; tpp < tp; tpp += 2) {
        FL_VAL var = *tpp, x = tpp[ 1 ];
        CAR(var) = var; /* undo binding */
        if(r) assign(tcb, tpp[ 1 ], *tpp, XDBG);
    }
    return r ? fl_true : fl_false;
}

void fl_unify_result(FL_TCB *tcb, FL_VAL val, FL_VAL dest)
{
    FL_VAL r = fl_unify_rec(tcb, addref(val), dest);
    unref(tcb, val);
    if(r == fl_false)
        fl_rt_error2(tcb, val, dest, FL_CANT_UNIFY);
}

static int check_heap(FL_TCB *tcb)
{
    if(!tcb->used) return 1;
    fprintf(stderr, "\n*** ERROR: <%ld> memory leak of %ld cells:\n", tcb->ordinal, 
        tcb->used);
    for(FL_CELL *p = tcb->heap; p < tcb->heapend; ++p) {
        if((p->tag & FL_COUNT_MASK) != 0) {
            fprintf(stderr, "<%ld>   %p 0x%08x ", tcb->ordinal, p, (int)p->tag);
            write_limited(tcb, stderr, (FL_VAL)p);
            fputc('\n', stderr);
        }
    }
    return 0;
}

static void finalize_heaps()
{
    int i;
#ifdef XLOGGING
    if(fl_tcbs[ 0 ].logging) 
        fl_logfmt(&fl_tcbs[ 0 ], "waiting for threads\n");
#endif
    for(i = 1; i < threads; ++i)
        pthread_join(fl_tcbs[ i ].thread, NULL);
    for(i = 0; i < threads; ++i) {
        unref(&fl_tcbs[ i ], fl_tcbs[ i ].listening);
        unref(&fl_tcbs[ i ], fl_tcbs[ i ].idle);
        if(fl_tcbs[ i ].globals != NULL)
            for(int j = 0; j < FL_GLOBAL_TABLE; ++j)
                for(FL_HTNODE *n = fl_tcbs[ i ].globals[ j ]; n != NULL; n = n->next)
                    unref(&fl_tcbs[ i ], n->val);
    }
    int failed = 0;
    for(i = 0; i < threads; ++i) {
        failed = !check_heap(&fl_tcbs[ i ]) || failed;
        assert(fl_tcbs[ i ].goals == 0);
    }
    if(failed) fl_terminate(1);
}

static int has_active_suspensions(FL_VAL var) 
{
    for(FL_VAL slsts = CDR(var); slsts != fl_nil; slsts = CDR(slsts)) {
        FL_VAL slst = CAR(slsts);
        if(CAR(slst) != fl_nil) return 1;
    }
    return 0;
}

void fl_yield(FL_TCB *tcb)
{
#ifdef ENABLE_PROFILING
    long *counters = tcb->counters;
    tcb->counters = yield_counters;
#endif
    ++tcb->ticks;
    if((tcb->ticks % stat_ticks) == 0) {
        if((stats & (1 << (tcb->ordinal - 1))) != 0) 
            statistics(tcb);
        if((heapstats & (1 << (tcb->ordinal - 1))) != 0) 
            heap_statistics(tcb);
    }
listen:
    if(*(((int *)tcb->port) + 1) > 0) {
        FL_VAL ev = fl_unforward(tcb);
        fl_add_event(tcb, ev);
        tcb->delay = 0;
        unref(tcb, ev); /* unforward adds one implicit ref */
        if(tcb->state != FL_BUSY) {
            assert(tcb->state == FL_IDLE);
            tcb->state = FL_BUSY;
#ifdef XLOGGING
            if(tcb->logging) fl_logfmt(tcb, "IDLE -> BUSY (message)\n");
#endif
            __sync_sub_and_fetch(&idle_threads, 1);
        }
        __sync_sub_and_fetch(&msg_tally, 1);
    }
    if(tcb->listening != fl_nil) {
        int ms = tcb->delay >> 8;
        if(tcb->qstart == tcb->qend) {
            if(threads == 1 && !tcb->detached) {
#ifdef XLOGGING
                if(tcb->logging) fl_logfmt(tcb, "waiting for event\n");
#endif
                ms = -1;
            }
        } else ms = 0;
#ifdef XLOGGING
        if(tcb->logging) {
            pthread_mutex_lock(&log_mutex);
            fprintf(logfile, "<%ld> %ld: %s listen (%dms): ", tcb->ordinal, tcb->ticks,
                tcb->goal->info, ms);
            write_limited(tcb, logfile, tcb->listening);
            fputc('\n', logfile);
            pthread_mutex_unlock(&log_mutex);
        }
#endif        
        int r = fl_wait_for_events(tcb, ms);
        if(r == -1) fl_rt_error(tcb, fl_nil, FL_IO_ERROR);
        else if(r > 0) tcb->delay = 0;
        else if(ms != -1) {
            tcb->delay += FUDGE;
            if(tcb->delay > MAX_DELAY) tcb->delay = MAX_DELAY;
        }
    }
    if(tcb->qstart == tcb->qend) {
        if(has_active_suspensions(tcb->idle)) {
            fl_assign(tcb, fl_nil, tcb->idle);
            unref(tcb, tcb->idle);
            ++tcb->epoch;
            tcb->idle = fl_alloc_cell(tcb, fl_nil, fl_nil);
            ((FL_CELL *)tcb->idle)->tag = FL_VAR_TAG | 1;
            ((FL_CELL *)tcb->idle)->car = tcb->idle;
            goto next;
        }
        if(tcb->listening != fl_nil) goto listen;
        if(tcb->state == FL_BUSY) {
            int n = __sync_add_and_fetch(&idle_threads, 1);
            tcb->state = FL_IDLE;
#ifdef XLOGGING
            if(tcb->logging) fl_logfmt(tcb, "BUSY -> IDLE (%d)\n", n);
#endif
            /* give other threads a chance to pick up any pending messages
               in case they are sleeping: */
            tcb->delay = MAX_DELAY * 2;
        } else {
            __sync_synchronize();
            if(!tcb->detached && idle_threads == threads && msg_tally == 0) {
                if(tcb->state == FL_FINISHING) {
                    if(tcb->suspended != 0) {
                        fprintf(stderr, "<%ld> deadlock with %ld suspended goal(s):\n", 
                            tcb->ordinal, tcb->suspended);
                        dump_suspended(tcb);
                        fl_terminate(1);
                    }
#ifdef LOGGING
                    if(tcb->logging) fl_logfmt(tcb, "terminates\n");
#endif
                    if(tcb->ordinal == 1) finalize_heaps();
#ifdef LOGGING
                    if(tcb->logging && tcb->ordinal == 1) 
                        fl_logfmt(tcb, "exit\n");
#endif
                    fflush(logfile);
                    if(--active_threads == 0) fl_terminate(0);
                    else pthread_exit(NULL);
                    /* never reached */
                } else { /* IDLE */
#ifdef XLOGGING
                    if(tcb->logging) fl_logfmt(tcb, "shutting down\n");
#endif
                    close_stream(tcb, tcb->events);
#ifdef XLOGGING
                    if(tcb->logging) fl_logfmt(tcb, "IDLE -> FINISHING\n");
#endif
                    tcb->state = FL_FINISHING;
                    unref(tcb, tcb->events);
                }
            }
        }
        if(usleep(tcb->delay >> 8) == -1) {  /* likely EINTR */
            tcb->delay = 0;
        }
        else {
            tcb->delay += FUDGE;
            if(tcb->delay > MAX_DELAY) tcb->delay = MAX_DELAY;
        }
        goto listen;
    }
next:
    tcb->goal = dequeue(tcb);
#ifdef ENABLE_PROFILING
    tcb->counters = counters;
#endif
    tcb->sstackp = tcb->sstack;
    fl_enter(tcb);
}

static void *enter_thread(void *tcb0) 
{
    FL_TCB *tcb = (FL_TCB *)tcb0;
    /* align stackbase to 16 byte boundary - at least on Darwin this doesn't seem to
       be guaranteed... */
#if defined(__aarch64__) || defined(__riscv)
    /* on architectures with special stack alignment requirements we use our 
       own intermediate stack in x28(a64)/s11(r64), which is part of the thread's 
       stack */
    int ssize = STACK_RESERVE + MAX_ENV * sizeof(FL_VAL);
    char *p = alloca(ssize) + 8;
    tcb->sp0 = (void *)((unsigned long)p & -16);
    p = tcb->sp0 + STACK_RESERVE;
#else
    int ssize = MAX_ENV * sizeof(FL_VAL);
    char *p = alloca(ssize) + 8;
#endif
    tcb->stackbase = (void *)((unsigned long)p & -16);
    fl_enter(tcb);
    return NULL;    /* never reached */
}

static void build_topology(void)
{
    int h = round(sqrt(threads));
    int w = h + (threads - h * h) / h;
    for(int i = 0; i < threads; ++i) {
        int right = (i + 1) % threads;
        fl_tcbs[ i ].peers[ -FL_FWD - 1 ] = right + 1;
        fl_tcbs[ right ].peers[ -FL_BWD - 1 ] = i + 1;
    }
    for(int r = 0; r < h; ++r) {
        for(int c = 0; c < w; ++c) {
            int here = (r * w) + c;
            int below = ((r + 1) % h) * w + c;
            fl_tcbs[ here ].peers[ -FL_SOUTH - 1 ] = below + 1;
            fl_tcbs[ below ].peers[ -FL_NORTH - 1 ] = here + 1;
            int right = (r * w) + (c + 1) % w;
            fl_tcbs[ here ].peers[ -FL_EAST - 1 ] = right + 1;
            fl_tcbs[ right ].peers[ -FL_WEST - 1 ] = here + 1;
        }
    }
}

FL_MODULE *fl_find_module(FL_TCB *tcb, FL_VAL name)
{
    for(FL_MODULE *m = last_module; m != NULL; m = m->next) {
        if(m->name == name) return m;
    }
    return NULL;
}

FL_VAL fl_all_modules(FL_TCB *tcb) 
{
    FL_VAL lst = fl_nil;
    for(FL_MODULE *m = last_module; m != NULL; m = m->next) {
        FL_VAL mod = fl_alloc_cell(tcb, MARK(m), fl_nil);
        ((FL_CELL *)mod)->tag = FL_MODULE_TAG;
        lst = mklist(tcb, mod, lst);
    }
    return lst;
}

static void profile_sample(int sig)
{
    for(int i = 0; i < threads; ++i) {
        long *counters = fl_tcbs[ i ].counters;
        if(counters != NULL) ++counters[ i ];
    }
}

void fl_start(int nthreads, long heapsz, int maxgoals)
{
    fl_nil = fl_mkstring("[]", 2);
    fl_colon = fl_mkstring(":", 1);
    fl_true = fl_mkstring("true", 4);
    fl_false = fl_mkstring("false", 5);
    fl_error = fl_mkstring("error", 5);
    fl_slash = fl_mkstring("/", 1);
    sys_name = fl_mkstring("sys", 3);
    fl_tcbs[ 0 ].ordinal = 1;
    fl_tcbs[ 0 ].logging = (logging & 1) != 0;
    fl_tcbs[ 0 ].loggingx = (loggingx & 1) != 0;
    init_thread(&fl_tcbs[ 0 ], heapsz, maxgoals);
    threads = nthreads;
    fl__init();
    assert(threads >= 1 && threads <= MAX_THREADS);
    for(int i = 1; i < threads; ++i) {
        fl_tcbs[ i ].ordinal = i + 1;
        fl_tcbs[ i ].logging = (logging & (1 << i)) != 0;
        fl_tcbs[ i ].loggingx = (loggingx & (1 << i)) != 0;
        init_thread(&fl_tcbs[ i ], heapsz, maxgoals);
    }
    build_topology();
#ifdef ENABLE_PROFILING
    for(FL_MODULE *m = last_module; m != NULL; m = m->next) {
        for(FL_PROFINFO *p = m->profinfo; p != NULL; p = p->next)
            p->counters = (long *)calloc(threads, sizeof(long));
    }
    yield_counters = (long *)calloc(threads, sizeof(long));
    if(profile != NULL) {
        struct sigaction sa;
        sa.sa_handler = profile_sample;
        sigemptyset(&sa.sa_mask);
        sa.sa_flags = SA_RESTART;
        sigaction(SIGPROF, &sa, NULL);
        struct itimerval pt;
        pt.it_interval.tv_sec = 0;
        pt.it_interval.tv_usec = PROFILE_SAMPLE_TIME * 1000;
        pt.it_value = pt.it_interval;
        setitimer(ITIMER_PROF, &pt, NULL);
    }
#endif
    fl_init_signals();
    FL_MODULE *sysmod = fl_find_module(&fl_tcbs[ 0 ], sys_name);
    assert(threads == 1 || sysmod != NULL);
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setstacksize(&attr, T_STACK_SIZE);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    for(int i = 1; i < threads; ++i) {
        init_goal(&fl_tcbs[ i ], sysmod->pdefs);
        pthread_create(&fl_tcbs[ i ].thread, &attr, enter_thread, (void *)&fl_tcbs[ i ]);
    }
    enter_thread(&fl_tcbs[ 0 ]);
}

static char *prgname;

static void usage(int code)
{
    fprintf(stderr, "usage: %s [+HELP] [+VERSION] [+LOG <mask>] [+LOGX <mask>] [+HEAP <heapsize>] [+GOALS <maxgoals>] [+TIMESLICE <ticks>] [+LOGFILE <filename>] [+THREADS <number>] [+STATS <mask>] [+STAT_TICKS <number>] [+HEAPSTATS <mask>] [+PROFILE <filename>] [+OCCURS_CHECK] ... [--] ...\n", prgname);
    exit(code);
}

static long arg(char **argv, int i, int suffix)
{
    char *arg = argv[ i ];
    if(arg == NULL) usage(1);
    char *endp;
    long x = strtol(arg, &endp, 0);
    if(suffix) {
        switch(*endp) {
        case 'k':
        case 'K': x *= 1024; ++endp; break;
        case 'm':
        case 'M': x *= 1024 * 1024; ++endp; break;
        case 'g':
        case 'G': x *= 1024 * 1024 * 1024; ++endp; break;
        }
    }
    if(*endp != '\0') usage(1);
    return x;
}

int main(int argc, char *argv[])
{
    int heap_size = HEAP_SIZE;
    int max_goals = MAX_GOALS;
    char *argv2[ MAX_ARGV ];
    logfile = stderr;
    int args = 0, argc2 = 0;
    int i;
    prgname = argv[ 0 ];
    active_threads = threads = 1;
    idle_threads = msg_tally = 0;
    occurs_check = dump_on_error = 0;
    struct timeval tv;
    gettimeofday(&tv, NULL);
    fl_set_random_seed((char *)&tv, sizeof(struct timeval));
    char *cmd = fl_cmdline;
    while(*cmd != '\0') {
        while(isspace(*cmd)) ++cmd;
        if(*cmd == '\0') break;
        char *p1 = cmd;
        while(*cmd != '\0' && !isspace(*cmd)) ++cmd;
        if(argc2 >= MAX_ARGV) {
            fprintf(stderr, "too many arguments: %s\n", fl_cmdline);
            exit(1);
        }
        argv2[ argc2++ ] = strndup(p1, cmd - p1);
    }
    for(i = 1; i < argc; ++i) argv2[ argc2++ ] = argv[ i ];
    argv2[ argc2 ] = NULL;
    for(i = 0; i < argc2; ++i) {
        if(!strcmp("--", argv2[ i ])) {
            ++i;
            break;
        }
        if(*argv2[ i ] == '+') {
            if(!strcmp("+LOG", argv2[ i ]))
                logging = arg(argv2, ++i, 0);
            else if(!strcmp("+LOGX", argv2[ i ]))
                loggingx = arg(argv2, ++i, 0);
            else if(!strcmp("+STATS", argv2[ i ]))
                stats = arg(argv2, ++i, 0);
            else if(!strcmp("+STAT_TICKS", argv2[ i ]))
                stat_ticks = arg(argv2, ++i, 0);
            else if(!strcmp("+HEAPSTATS", argv2[ i ]))
                heapstats = arg(argv2, ++i, 0); 
            else if(!strcmp("+HEAP", argv2[ i ]))
                heap_size = arg(argv2, ++i, 1);
            else if(!strcmp("+GOALS", argv2[ i ]))
                max_goals = arg(argv2, ++i, 1);
            else if(!strcmp("+THREADS", argv2[ i ]))
                threads = arg(argv2, ++i, 0);
            else if(!strcmp("+TIMESLICE", argv2[ i ]))
                timeslice = arg(argv2, ++i, 0);
            else if(!strcmp("+OCCURS_CHECK", argv2[ i ]))
                occurs_check = 1;
            else if(!strcmp("+DUMP_HEAP_ON_ERROR", argv2[ i ]))
                dump_on_error = 1;
            else if(!strcmp("+VERSION", argv2[ i ])) {
                fputs(version, stdout);
                exit(0);
            }
            else if(!strcmp("+LOGFILE", argv2[ i ])) {
                FILE *fp;
                char *fname = argv2[ ++i ];
                char *mode = "w";
                if(*fname == '+') {
                    ++fname;
                    mode = "a";
                }
                if((fp = fopen(fname, mode)) == NULL) {
                    fprintf(stderr, "can not open log file %s\n", fname);
                    exit(1);
                }
                logfile = fp;
            }
            else if(!strcmp("+PROFILE", argv2[ i ]))
                profile = argv2[ ++i ];
            else if(!strcmp("+HELP", argv2[ i ])) usage(0);
            else usage(1);
        } else fl_argv[ args++ ] = argv2[ i ];
    }
    while(i < argc2) fl_argv[ args++ ] = argv2[ i++ ];
    fl_argv[ args ] = NULL;
    fl_start(threads, heap_size, max_goals);
}
