% term manipulation

-module(term).

% read terms from file

read_terms(Name, Errs, Forms, File) :-
    u:open_input_file(Name, File),
    read_terms2(File, Name, Forms, Errs).

read_terms2(error(_, Str), Name, _, Errs) :-
    send(Errs, error('can not open file: ~s - ~a', [Name, Str])).
read_terms2(File, _, Forms, Errs) :-
    otherwise |
    parse:parse_terms(File, Forms, Errs).


% extract singleton variables as a list of "Name = ID" tuples

singletons(X, S) :- 
    extract_variables(X, S1),
    singletons2(S1, [], [], S).

singletons2([], _, S1, S) :- S1 = S.
singletons2([V|S1], Seen, Ss, S) :-
    list:member(V, Seen, F),
    (F == true  ->
        singletons2(S1, Seen, Ss, S)
    ;
        list:member(V, S1, F2),
        (F2 == true  ->
            singletons2(S1, [V|Seen], Ss, S)
        ;
            singletons2(S1, [V|Seen], [V|Ss], S)
        )
    ).


% extract variables from a term and return a list of "Name = ID" tuples

extract_variables(X, Vs) :- extract_variables(X, Vs, []).

extract_variables([X|Y], Vs, T) :-
    extract_variables(X, Vs, Vs2),
    extract_variables(Y, Vs2, T).
extract_variables(X, Vs, T) :-
    tuple(X) |
    tuple_to_list(X, L, []),
    extract_variables_tuple(L, Vs, T).
extract_variables(_, Vs, T) :- otherwise | Vs = T.

extract_variables_tuple(['$VAR', _, V, N], Vs, T) :- 
    integer(V) |
    Vs = [N = V|T].
extract_variables_tuple(L, Vs, T) :- 
    otherwise | extract_variables(L, Vs, T).


% convert extracted variable list into sorted list of IDs

environment(Vars, Env) :-
    app:maplist(get_arg(3), Vars, E1),
    sort:sort(E1, Env).

extract_environment(X, Env) :-
    extract_variables(X, Vars),
    environment(Vars, Env).


% create fresh variables not contained in environment
% (assumes Vars is sorted environment)

fresh_variable(Env1, V, Env) :- fresh_variables(Env1, [V], Env).

fresh_variables(Env1, Vs, Env) :-
    copy_env(Env1, 0, N, Env, Tail),
    fresh_variables2(N, Vs, Tail).

copy_env([], N, M, Env, Tail) :-
    Env = Tail,
    M is N + 1.
copy_env([V|Env1], _, M, Env, Tail) :-
    Env = [V|Env2],
    copy_env(Env1, V, M, Env2, Tail).

fresh_variables2(_, [], Env) :- Env = [].
fresh_variables2(N, [V|L], Env) :- 
    V = N,
    N2 is N + 1,
    Env = [V|Env2],
    fresh_variables2(N2, L, Env2).


% test whether a term is ground

ground(X, F) :- atomic(X) | F = true.
ground([X|Y], F) :- 
    ground(X, F1), 
    (F1 == true -> ground(Y, F); F = false).
ground(T, F) :- 
    tuple(T) |
    tuple_to_list(T, Lst, []),
    ground2(Lst, F).
ground(_, F) :- otherwise | F = false.

ground2(['$VAR', _, _, _], F) :- F = false.
ground2(L, F) :- otherwise | ground(L, F).


% test whether two terms are structurally equivalent

structurally_equal(X, X, F) :- F = true.
structurally_equal(X, Y, F) :- atomic(Y), X =\= Y | F = false.
structurally_equal(X, Y, F) :- atomic(X), X =\= Y | F = false.
structurally_equal([X1|X2], [Y1|Y2], F) :-
    structurally_equal(X1, Y1, F1),
    (F1 == true -> structurally_equal(X2, Y2, F); F = false).
structurally_equal(X, Y, F) :-
    tuple(X), tuple(Y) |
    tuple_to_list(X, T1, []),
    tuple_to_list(Y, T2, []),
    structurally_equal2(T1, T2, F).
structurally_equal(_, _, F) :- otherwise | F = false.

structurally_equal2(['$VAR'|_], _, F) :- F = true.
structurally_equal2(_, ['$VAR'|_], F) :- F = true.
structurally_equal2(L1, L2, F) :-
    otherwise | structurally_equal(L1, L2, F).


% basic term destructuring

functor_parts(F, Name, Arity, Args) :- 
    string(F) |
    Name = F,
    Arity = 0,
    Args = [].
functor_parts(F, Name, Arity, Args) :- 
    tuple(F) |
    tuple_to_list(F, [Name|Args], []),
    length(Args, Arity).

make_functor([X], F) :- string(X) | F = X.
make_functor(X, F) :- otherwise | list_to_tuple(X, F).

functor_arity(F, A) :- tuple(F) | length(F, A1), A is A1 - 1.
functor_arity(_, A) :- otherwise | A = 0.

% extract last argument

split_result([X], L, R) :- L = [], R = X.
split_result([X|L], L2, R) :-
    L2 = [X|L3],
    split_result(L, L3, R).


% variable construction

make_vars([], Vs) :- Vs = [].
make_vars([V|Is], Vs) :-
    Vs = ['$VAR'(0, V, 0)|Vs2],
    make_vars(Is, Vs2).

pseudo_var(Name, V) :- V = '$VAR'(0, Name, 0).
