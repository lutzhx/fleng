#!/bin/sh
# FLENG general build script

. ./config.sh

GHCLIBS="lib fmt sort set map lex parse sys proc mem app list io scan match"
FCOBJS="asm ep ffi inline index lfa u term"
RTOBJS="rt1 prng memlib $OS rtlib"
CONFFLAGS="-DLOGGING"
STAGE=
LSTAGE=
DCOMP=
SPREFIX=.
FCPREFIX=.

v () {
    echo "  " "$@"
    "$@"
}

sources () {
    for o in $GHCLIBS; do
        ./cdo "$o.fl" : "$SRCDIR/lib/$o.ghc" $DCOMP config.sh : \
            "$FCPREFIX/fghc2fl$STAGE" -I "$SRCDIR" "$SRCDIR/lib/$o.ghc" \
            "$o.fl" || exit 1
    done
    ./cdo rt.s : "$SRCDIR/lib/rt.fl" $DCOMP config.sh : \
            "$FCPREFIX/fl2$ARCH$STAGE" $FLFLAGS -I "$SRCDIR" \
            "$SRCDIR/lib/rt.fl" rt.s || exit 1        
    for o in $GHCLIBS; do
        ./cdo "$o.s" : "$o.fl" config.sh : \
            "$FCPREFIX/fl2$ARCH$STAGE" -w $FLFLAGS "$o.fl" \
            "$o.s" || exit 1
    done
}

runtime () {
    for o in $RTOBJS; do
        ./cdo "$o.o" : "$SRCDIR/$o.c" "$SRCDIR/fleng.h" \
            "$SRCDIR/fleng-util.h" version.h config.sh : \
            $CC -c -I. -I"$SRCDIR" $CONFFLAGS $CFLAGS "$SRCDIR/$o.c" -o \
                "$o.o" || exit 1
    done
    ./cdo "rt0$ARCH.o" : "$SRCDIR/rt0$ARCH.s" config.sh : \
        $AS $ASFLAGS "$SRCDIR/rt0$ARCH.s" -o "rt0$ARCH.o" \
            || exit 1
}

compiler_sources () {
    for o in $FCOBJS $ARCH fghc2fl compiler; do
        ./cdo "$o.fl" : "$SRCDIR/compiler/$o.ghc" $DCOMP \
            config.sh version.ghc config.ghc : \
            "$FCPREFIX/fghc2fl$STAGE" -I "$SRCDIR" "$SRCDIR/compiler/$o.ghc" \
            "$o.fl" || exit 1
    done
    for o in $FCOBJS $ARCH fghc2fl compiler; do
        ./cdo "$o.s" : "$o.fl" config.sh version.ghc config.ghc : \
            "$FCPREFIX/fl2$ARCH$STAGE" -w $FLFLAGS "$o.fl" \
            "$o.s" || exit 1
    done
}

base () {
    for o in $GHCLIBS rt; do
        ./cdo "$o.o" : "$SPREFIX/$o.s" config.sh : \
            $AS $ASFLAGS "$SPREFIX/$o.s" -o "$o.o" || exit 1
    done
    ./cdo "libfleng$LSTAGE.a" : \
        "rt0$ARCH.o" rt1.o prng.o memlib.o "$OS.o" rtlib.o \
        lib.o fmt.o sort.o set.o map.o lex.o parse.o sys.o proc.o \
        mem.o app.o list.o io.o scan.o match.o rt.o config.sh : \
        $AR cr "libfleng$LSTAGE.a" "rt0$ARCH.o" rt1.o prng.o \
            memlib.o $OS.o rtlib.o lib.o fmt.o sort.o \
            set.o map.o lex.o parse.o sys.o proc.o mem.o app.o list.o \
            io.o scan.o match.o rt.o || exit 1
    for o in fghc2fl compiler $FCOBJS $ARCH $GHCLIBS; do
        ./cdo "$o.o" : "$SPREFIX/$o.s" config.sh : \
            $AS $ASFLAGS "$SPREFIX/$o.s" -o "$o.o" || exit 1
    done
    ./cdo "fghc2fl$STAGE" : fghc2fl.o term.o \
        u.o "libfleng$LSTAGE.a" config.sh : \
        $CC fghc2fl.o term.o u.o -o "fghc2fl$STAGE" \
            "libfleng$LSTAGE.a" $LDFLAGS || exit 1
    ./cdo "fl2$ARCH$STAGE" : compiler.o term.o \
        u.o asm.o ep.o ffi.o inline.o index.o lfa.o "$ARCH.o" \
        "libfleng$LSTAGE.a" config.sh : \
        $CC compiler.o term.o u.o asm.o ep.o ffi.o inline.o index.o lfa.o \
            "$ARCH.o" -o "fl2$ARCH$STAGE" "libfleng$LSTAGE.a" $LDFLAGS \
            || exit 1
}

doctools () {
    ./cdo flengdoc.fl : "$SRCDIR/flengdoc.ghc" config.sh \
        config.ghc version.ghc : \
        "./fghc2fl$STAGE" -I "$SRCDIR" "$SRCDIR/flengdoc.ghc" \
            flengdoc.fl || exit 1
    ./cdo flengdoc.s : flengdoc.fl config.sh : \
        "./fl2$ARCH$STAGE" -d -w $FLFLAGS flengdoc.fl flengdoc.s \
            || exit 1
    ./cdo flengdoc.o : flengdoc.s config.sh : \
        $AS $ASFLAGS flengdoc.s -o flengdoc.o || exit 1
    ./cdo flengdoc : flengdoc.o "libfleng$LSTAGE.a" : \
        $CC flengdoc.o -o flengdoc "libfleng$LSTAGE.a" $LDFLAGS || exit 1
}

boot_compiler () {
    mkdir -p boot
    cd boot
    if ! [ -f config.sh ]; then
        "$SRCDIR/boot/configure" || exit 1
    fi
    ../cdo fl2$ARCH : *.pl : ./build || exit 1
    cd ..
}

stage1 () {
    boot_compiler
    FCPREFIX=boot
    DCOMP="boot/fl2$ARCH"
    sources
    compiler_sources
    runtime
    STAGE=-stage1
    LSTAGE=-stage1
    base
}

stage2 () {
    FCPREFIX=.
    STAGE=-stage1
    LSTAGE=-stage1
    DCOMP="fl2$ARCH$STAGE"
    sources
    compiler_sources
    STAGE=
    LSTAGE=
    runtime
    base
}

from_dist () {
    SPREFIX="$SRCDIR/asm/$ARCH"
    runtime
    base
    v cp fghc2fl fghc2fl-stage1
    v cp fl2$ARCH fl2$ARCH-stage1
}

documentation () {
    doctools
    ./cdo LIBRARY : "$SRCDIR/SYNTAX" "$SRCDIR/DECLARATIONS" \
        "$SRCDIR/GUARDS" "$SRCDIR/PRIMITIVES" \
        "$SRCDIR/lib/rt.fl" "$SRCDIR/lib/lib.ghc" "$SRCDIR/lib/sys.ghc" \
        "$SRCDIR/lib/fmt.ghc" "$SRCDIR/lib/sort.ghc" "$SRCDIR/lib/map.ghc" \
        "$SRCDIR/lib/list.ghc" "$SRCDIR/lib/set.ghc" "$SRCDIR/lib/proc.ghc" \
        "$SRCDIR/lib/lex.ghc" "$SRCDIR/lib/parse.ghc" "$SRCDIR/lib/mem.ghc" \
        "$SRCDIR/lib/app.ghc" "$SRCDIR/lib/io.ghc" "$SRCDIR/lib/scan.ghc" \
        "$SRCDIR/lib/match.ghc" flengdoc : \
        ./flengdoc -x "$SRCDIR/SYNTAX" \
            "$SRCDIR/DECLARATIONS" "$SRCDIR/GUARDS" "$SRCDIR/PRIMITIVES" \
            "$SRCDIR/lib/rt.fl" "$SRCDIR/lib/lib.ghc" "$SRCDIR/lib/sys.ghc" \
            "$SRCDIR/lib/fmt.ghc" "$SRCDIR/lib/sort.ghc" \
            "$SRCDIR/lib/map.ghc" "$SRCDIR/lib/list.ghc" \
            "$SRCDIR/lib/set.ghc" "$SRCDIR/lib/proc.ghc" \
            "$SRCDIR/lib/lex.ghc" "$SRCDIR/lib/parse.ghc" \
            "$SRCDIR/lib/mem.ghc" "$SRCDIR/lib/app.ghc" \
            "$SRCDIR/lib/io.ghc" "$SRCDIR/lib/scan.ghc" \
            "$SRCDIR/lib/match.ghc" -o LIBRARY1
    if ! [ -f LIBRARY ] || [ LIBRARY1 -nt LIBRARY ]; then
        echo "adding TOC"
        cat > LIBRARY <<EOF

FLENG LIBRARY REFERENCE
=======================


Table of contents:

EOF
        grep -E '^[0-9]+\. ' LIBRARY1 >> LIBRARY
        printf "\n" >> LIBRARY
        cat LIBRARY1 >> LIBRARY
        rm -f LIBRARY1
    fi
    ./cdo DOC : "$SRCDIR/SYNTAX" "$SRCDIR/DECLARATIONS" \
        "$SRCDIR/GUARDS" "$SRCDIR/PRIMITIVES" \
        "$SRCDIR/lib/rt.fl" "$SRCDIR/lib/lib.ghc" "$SRCDIR/lib/sys.ghc" \
        "$SRCDIR/lib/fmt.ghc" "$SRCDIR/lib/sort.ghc" "$SRCDIR/lib/map.ghc" \
        "$SRCDIR/lib/list.ghc" "$SRCDIR/lib/set.ghc" "$SRCDIR/lib/proc.ghc" \
        "$SRCDIR/lib/lex.ghc" "$SRCDIR/lib/parse.ghc" "$SRCDIR/lib/mem.ghc" \
        "$SRCDIR/lib/app.ghc" "$SRCDIR/lib/io.ghc" "$SRCDIR/lib/scan.ghc" \
        "$SRCDIR/lib/match.ghc" flengdoc : \
        ./flengdoc -c -db DOC -map rt '$rt' -q "$SRCDIR/SYNTAX" \
            "$SRCDIR/DECLARATIONS" \
            "$SRCDIR/GUARDS" "$SRCDIR/PRIMITIVES" \
            "$SRCDIR/lib/rt.fl" "$SRCDIR/lib/lib.ghc" "$SRCDIR/lib/sys.ghc" \
            "$SRCDIR/lib/fmt.ghc" "$SRCDIR/lib/sort.ghc" \
            "$SRCDIR/lib/map.ghc" "$SRCDIR/lib/list.ghc" \
            "$SRCDIR/lib/set.ghc" "$SRCDIR/lib/proc.ghc" \
            "$SRCDIR/lib/lex.ghc" "$SRCDIR/lib/parse.ghc" \
            "$SRCDIR/lib/mem.ghc" "$SRCDIR/lib/app.ghc" \
            "$SRCDIR/lib/io.ghc" "$SRCDIR/lib/scan.ghc" \
            "$SRCDIR/lib/match.ghc"
    man -l fleng.1.mdoc | col -b | ./flengdoc -db DOC -i fleng
    man -l flengdoc.1.mdoc | col -b | ./flengdoc -db DOC -i flengdoc
    man -l fleng.7.mdoc | col -b | ./flengdoc -db DOC -i options
    grep -hE '^%= .+ module:' lib/*.ghc lib/*.fl | sed 's/^%= //' | \
        ./flengdoc -db DOC -i modules
}

clean () {
    for x in $GHCLIBS $FCOBJS $ARCH fghc2fl compiler flengdoc; do
        v rm -f "$x.fl"
    done
    for x in $GHCLIBS $FCOBJS $ARCH fghc2fl compiler rt flengdoc; do
        v rm -f "$x.s"
    done
    v rm -f libfleng.a *.o fghc2fl fl2$ARCH flengdoc
    v rm -f fghc2fl-stage1 fl2$ARCH-stage1
    v rm -fr DOC
    if [ -d boot ]; then
        cd boot
        ./build clean
        cd ..
    fi
}

install=$(command -v install)

install () {
    v mkdir -p "$DESTDIR$PREFIX/bin"
    v "$install" fleng "$DESTDIR$PREFIX/bin/${TPREFIX}fleng"
    v "$install" fghc2fl "$DESTDIR$PREFIX/bin/${TPREFIX}fghc2fl"
    v "$install" fl2$ARCH "$DESTDIR$PREFIX/bin/${TPREFIX}fl2$ARCH"
    v mkdir -p "$DESTDIR$PREFIX/lib" "$DESTDIR$PREFIX/include"
    v cp libfleng.a "$DESTDIR$PREFIX/lib/lib${TPREFIX}fleng.a"
    v cp "$SRCDIR/fleng.h" "$SRCDIR/fleng-util.h" "$DESTDIR$PREFIX/include"
    v mkdir -p "$DESTDIR$PREFIX/share/man/man1"
    v mkdir -p "$DESTDIR$PREFIX/share/man/man7"
    v mkdir -p "$DESTDIR$PREFIX/share/doc/fleng"
    v cp "$SRCDIR/fleng.1.mdoc" "$DESTDIR$PREFIX/share/man/man1/fleng.1"
    v cp "$SRCDIR/flengdoc.1.mdoc" "$DESTDIR$PREFIX/share/man/man1/flengdoc.1"
    v cp "$SRCDIR/fleng.7.mdoc" "$DESTDIR$PREFIX/share/man/man7/fleng.7"
    v cp "$SRCDIR/flengdoc.7.mdoc" "$DESTDIR$PREFIX/share/man/man7/flengdoc.7"
    v cp "$SRCDIR/README" "$SRCDIR/MANUAL" "$SRCDIR/INTERNALS" \
        LIBRARY "$DESTDIR$PREFIX/share/doc/fleng"
    v cp -r DOC "$DESTDIR$PREFIX/share/doc/fleng"
}

check () {
    env FLENG_PREFIX=. sh "$SRCDIR/chk" "$SRCDIR"
}

bench () {
    env FLENG_PREFIX=. sh "$SRCDIR/bnch" "$SRCDIR"
}

bootstrap () {
    stage1
    stage2
    documentation
}

default () {
    if [ -f "fl2$ARCH" ]; then
        sources
        compiler_sources
        runtime
        base
    elif [ -x "fl2$ARCH-stage1" ]; then
        stage2
    elif [ -x "boot/fl2$ARCH" ]; then
        stage1
        stage2
    elif [ -d "$SRCDIR/asm/$ARCH" ]; then
        from_dist
    else
        bootstrap
    fi
    documentation
}

if [ -z "$1" ]; then
    default
    exit
fi

for x in "$@"; do
    $x
done
